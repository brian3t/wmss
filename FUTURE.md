#### 1 $POST conversion
Convert $POST from string values to integers when applicable. For example
$POST['uom_id'] = "3" -> should be converted to $POST['uom_id'] = 3

#### 2 prodUom 
todo ProdUom findWr: joinWith do not pull too many relations

#### 3 Diff
- Study codefile.php         $diff = new \Diff($lines1, $lines2);
  Find out why Diff is in global namespace.
- Make change to phpsec/Diff.php default 		'ignoreWhitespace' => true,

#### 4 Grids: comp_id hide
In tabulator grids; hide comp_id

#### 5 module/v1/api: 
Make v1/prod_uom/ work similar to v1/prod-uom

#### 6 table inv:
Column: prod_uom_id instead of uom_id

#### 7 text area empty strings

