<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VariantVal */

$this->title = 'Create Variant Val';
$this->params['breadcrumbs'][] = ['label' => 'Variant Val', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="variant-val-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
