<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\VariantVal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Variant Val', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="variant-val-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Variant Val'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'variant.name',
            'label' => 'Variant',
        ],
        'value_int',
        'value_str',
        'value_dtm',
        'value_float',
        'note',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerProdDetail->totalCount){
    $gridColumnProdDetail = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'prodVariant.name',
                'label' => 'Product Variant'
            ],
                        'note',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProdDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-detail']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Prod Detail'),
        ],
        'columns' => $gridColumnProdDetail
    ]);
}
?>
    </div>
</div>
