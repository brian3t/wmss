<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;

use kartik\grid\GridView;

//use yii\grid\GridView;
use soc\yiiuser\User\Model\User;

$this->title = 'Pickline';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
$this->registerJsFile(
  '@web/js/pickline.js',
  ['depends' => [\yii\web\JqueryAsset::class]]
);
//echo \yii\helpers\Html::script(null, ['type' => 'module', 'src' => 'js/pickline.js']);
?>
<div class="pickline-index">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a('Create Pickline', ['create'], ['class' => 'btn btn-success']) ?>
  </p>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
    [
      'class' => 'yii\grid\ActionColumn',
    ],
    ['label' => 'Pick Detail', 'format' => 'raw', 'value' => function ($model) {
      return '<a href="#" class="pick_this_line" data-pickline_id="' . $model->id . '">Pick this line</a>
';
/*      return '<a target="_blank" href="/pickdet?pick=' . $model->id . '">All Pick Details</a><br/>
<a href="#" class="pick_this_line" data-pickline_id="' . $model->id . '">Pick this line</a>
';*/
    }],
    ['attribute' => 'id', 'visible' => false],
    ['attribute' => 'pick_id', 'label' => 'Pick ID'],
    [
      'attribute' => 'pick_id',
      'label' => 'Pick',
      'value' => function ($model) {
        return $model->pick?->name;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Pick::find()->asArray()->all(), 'id', 'name'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Pick', 'id' => 'grid--pick_id']
    ],
    'seq',
    [
      'attribute' => 'assigned_to',
      'label' => 'Assigned To',
      'value' => function ($model) {
        return $model->assignedTo?->username;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid--assigned_to']
    ],
    [
      'attribute' => 'prod_var_id',
      'label' => 'Prod Var',
      'value' => function ($model) {
        return $model->prodVar?->name;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\ProdVariant::find()->asArray()->all(), 'id', 'name'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Product variant', 'id' => 'grid--prod_var_id']
    ],
    [
      'attribute' => 'soline_id',
      'label' => 'Soline',
      'value' => function ($model) {
        return $model->soline?->id;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Soline::find()->asArray()->all(), 'id', 'id'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Soline', 'id' => 'grid--soline_id']
    ],
    'description',
//        'wh_id',
    [
      'attribute' => 'bin_id',
      'label' => 'Bin',
      'value' => function ($model) {
        return $model->bin?->name;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Bin::find()->asArray()->all(), 'id', 'name'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Bin', 'id' => 'grid--bin_id']
    ],
//        'zone_id',
    'qty_to_pick',
    'orig_qty_to_pick',
    [
      'attribute' => 'prod_uom_id',
      'label' => 'Prod Uom',
      'value' => function ($model) {
        return $model->prodUom?->id;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\ProdUom::find()->asArray()->all(), 'id', 'id'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Prod uom', 'id' => 'grid--prod_uom_id']
    ],
    'min_qty_to_pick',
    'max_qty_to_pick',
    'decimals',
    'cust_id',
//        'cust_addr_id',
    'completed_at',
    'ship_addr_id',
    [
      'attribute' => 'pick_completed_by',
      'label' => 'Pick Completed By',
      'value' => function ($model) {
        return $model->pickCompletedBy?->username;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid--pick_completed_by']
    ],
//        'note',
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pickline']],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
//        'export' => false,
    /*// your toolbar can include the additional full export menu
    'toolbar' => [
        '{export}',
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'exportConfig' => [
                ExportMenu::FORMAT_PDF => false
            ]
        ]) ,
    ]*/
  ]); ?>

</div>
