<?php

use yii\helpers\Html;
use soc\yii2helper\yii2_widget_activeform\ActiveForm;
//use kartik\widgets\ActiveForm;
use soc\yiiuser\User\Model\User;

/* @var $this yii\web\View */
/* @var $model app\models\Pickline */
///* @var $form yii\widgets\ActiveForm */
///* @var $form \kartik\widgets\ActiveForm */
/* @var $form \soc\yii2helper\yii2_widget_activeform\ActiveForm */

/*if ($model->isNewRecord){
  $model->qty_to_pick = -88;
}*/

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Pickdet',
        'relID' => 'pickdet',
        'value' => \yii\helpers\Json::encode($model->pickdets),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="pickline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

<!--    --><?php //= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'pick_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Pick::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Pick'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

<!--    --><?php //= $form->field($model, 'seq')->textInput(['placeholder' => 'Seq']) ?>
    <?= $form->field($model, 'seq')->textInput(['placeholder' => 'Seq','has_def_chk' => true]) ?>

    <?= $form->field($model, 'assigned_to')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'prod_var_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdVariant::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Product variant'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'soline_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Soline::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Soline'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

<!--    --><?php //= $form->field($model, 'wh_id')->textInput(['placeholder' => 'Wh']) ?>

    <?= $form->field($model, 'bin_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Bin::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Bin'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'zone_id')->textInput(['placeholder' => 'Zone']) ?>

    <?= $form->field($model, 'qty_to_pick')->textInput(['maxlength' => true, 'placeholder' => 'Qty To Pick', 'has_def_chk' => true]) ?>

    <?= $form->field($model, 'orig_qty_to_pick')->textInput(['maxlength' => true, 'placeholder' => 'Orig Qty To Pick']) ?>

    <?= $form->field($model, 'prod_uom_id')->widget(\kartik\widgets\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdUom::findWr()->orderBy('id')->asArray()->all(), 'id', function($prod_uom) {
          return $prod_uom['prod_name'] . ' - ' . $prod_uom['uom_name'];
        }),
        'options' => ['placeholder' => 'Choose Prod uom', 'sctype' => 'integer'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'min_qty_to_pick')->textInput(['maxlength' => true, 'placeholder' => 'Min Qty To Pick']) ?>

    <?= $form->field($model, 'max_qty_to_pick')->textInput(['maxlength' => true, 'placeholder' => 'Max Qty To Pick']) ?>

    <?= $form->field($model, 'decimals')->textInput(['placeholder' => 'Decimals']) ?>

    <?= $form->field($model, 'cust_id')->textInput(['placeholder' => 'Cust']) ?>

    <?= $form->field($model, 'cust_addr_id')->textInput(['placeholder' => 'Cust Addr']) ?>

    <?= $form->field($model, 'completed_at')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Completed At',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'ship_addr_id')->textInput(['placeholder' => 'Ship Addr']) ?>

    <?= $form->field($model, 'pick_completed_by')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true, 'placeholder' => 'Note']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Pickdet'),
            'content' => $this->render('_formPickdet', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pickdets),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
