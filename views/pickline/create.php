<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pickline */

$this->title = 'Create Pickline';
$this->params['breadcrumbs'][] = ['label' => 'Pickline', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
try {
  $this->registerJsFile(
    '@web/js/pickline_create.js',
    ['depends' => [\yii\web\JqueryAsset::class]]
  );
} catch (\yii\base\InvalidConfigException $e) {
}
?>
<div class="pickline-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
