<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->pickdets,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'seq',
        'lot_id',
        'ser_id',
        'cont_id',
        [
                'attribute' => 'inv.id',
                'label' => 'Inv'
            ],
        'qty',
        [
                'attribute' => 'prodUom.id',
                'label' => 'Prod Uom'
            ],
        'confirmed',
        'confirmed_at',
        [
                'attribute' => 'confirmedBy.username',
                'label' => 'Confirmed By'
            ],
        'note',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'pickdet'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
