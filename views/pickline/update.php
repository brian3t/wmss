<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pickline */

$this->title = 'Update Pickline: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pickline', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pickline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
