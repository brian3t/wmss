<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pickline */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pickline', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickline-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= 'Pickline' . ' ' . Html::encode($this->title) ?></h2>
    </div>
    <div class="col-sm-3" style="margin-top: 15px">

      <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Are you sure you want to delete this item?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'pick.name',
        'label' => 'Pick',
      ],
      'seq',
      [
        'attribute' => 'assignedTo.username',
        'label' => 'Assigned To',
      ],
      [
        'attribute' => 'prodVar.name',
        'label' => 'Prod Variant',
      ],
      [
        'attribute' => 'soline.id',
        'label' => 'Soline',
      ],
      'description',
      'wh_id',
      [
        'attribute' => 'bin.name',
        'label' => 'Bin',
      ],
      'zone_id',
      'qty_to_pick',
      'orig_qty_to_pick',
      [
        'attribute' => 'prodUom.uom.name',
        'label' => 'Prod UOM',
      ],
      'min_qty_to_pick',
      'max_qty_to_pick',
      'decimals',
      'cust_id',
      'cust_addr_id',
      'completed_at',
      'ship_addr_id',
      [
        'attribute' => 'pickCompletedBy.username',
        'label' => 'Pick Completed By',
      ],
      'note',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerPickdet->totalCount) {
      $gridColumnPickdet = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'seq',
        [
          'attribute' => 'bin.name',
          'label' => 'Bin'
        ],
        'lot_id',
        'ser_id',
        'cont_id',
        [
          'attribute' => 'inv.id',
          'label' => 'Inv'
        ],
        'qty',
        [
          'attribute' => 'prodUom.id',
          'label' => 'Prod Uom'
        ],
        'confirmed',
        'confirmed_at',
        [
          'attribute' => 'confirmedBy.username',
          'label' => 'Confirmed By'
        ],
        'note',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerPickdet,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pickdet']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pickdet'),
        ],
        'columns' => $gridColumnPickdet
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if (true) {
      $gridColumnPickdet = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'produom.uom.name',
          'label' => 'UOM'
        ],
        'seq',
        'lot_id',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerPickdet,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-uom']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pick Details'),
        ],
        'columns' => $gridColumnPickdet
      ]);
    }
    ?>
    <div class="action">
      <button class="btn btn-success" id="pl_new_pickdet_btn">New Pick Detail</button>
      <button class="btn btn-success" id="pl_pd_fr_inv_btn">Select From Inventory</button>
    </div>
  </div>
</div>
