<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pickline */

?>
<div class="pickline-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'pick.name',
            'label' => 'Pick',
        ],
        'seq',
        [
            'attribute' => 'assignedTo.username',
            'label' => 'Assigned To',
        ],
        [
            'attribute' => 'prodVar.name',
            'label' => 'Prod Var',
        ],
        [
            'attribute' => 'soline.id',
            'label' => 'Soline',
        ],
        'description',
        'wh_id',
        [
            'attribute' => 'bin.name',
            'label' => 'Bin',
        ],
        'zone_id',
        'qty_to_pick',
        'orig_qty_to_pick',
        [
            'attribute' => 'prodUom.id',
            'label' => 'Prod Uom',
        ],
        'min_qty_to_pick',
        'max_qty_to_pick',
        'decimals',
        'cust_id',
        'cust_addr_id',
        'completed_at',
        'ship_addr_id',
        [
            'attribute' => 'pickCompletedBy.username',
            'label' => 'Pick Completed By',
        ],
        'note',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>