<?php

/** @var yii\web\View $this */

/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;

//use yii\bootstrap5\NavBar;
use soc\yii2helper\widgets\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100 dark">
<head>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
  <?php
  NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'brandImage' => '/img/logo.svg',
    'options' => ['class' => 'navbar navbar-light navbar-glass navbar-top navbar-expand-xl'],
    'innerContainerOptions' => ['class' => 'container-fluid']
  ]);
  echo Nav::widget([
    'encodeLabels' => false,
    'options' => ['class' => 'navbar-nav'],
    'items' => [
      ['label' => 'Home', 'url' => ['/site/index']],
      [
        'label' => 'Admin',
        'options' => ['class' => 'dropdown'],
        'template' => '<a href="{url}" class="href_class">{label}</a>',
        'items' => [
          ['label' => 'Company', 'url' => '/comp'],
          ['label' => 'Bins', 'url' => '/bin'],
          ['label' => 'Products', 'url' => '/prod'],
          ['label' => 'Variants', 'url' => '/variant'],
          ['label' => 'Variant Values', 'url' => '/variant-val'],
          ['label' => 'UOMs', 'url' => '/uom'],
        ]
      ],
      [
        'label' => 'Products',
        'options' => ['class' => 'dropdown'],
        'template' => '<a href="{url}" class="href_class">{label}</a>',
        'items' => [
          ['label' => 'Product', 'url' => '/prod'],
          ['label' => 'Product UOM', 'url' => '/prod-uom'],
          ['label' => 'Product Variants', 'url' => '/prod-variant'],
        ]
      ],
      ['label' => 'Inventory', 'url' => ['/inv']],
      [
        'label' => 'Sales Orders',
        'options' => ['class' => 'dropdown'],
        'template' => '<a href="{url}" class="href_class">{label}</a>',
        'items' => [
          ['label' => 'Sales Orders', 'url' => '/so'],
          ['label' => 'Sales Order Lines', 'url' => '/soline'],
        ]
      ],
      [
        'label' => 'Picking',
        'options' => ['class' => 'dropdown'],
        'template' => '<a href="{url}" class="href_class">{label}</a>',
        'items' => [
          ['label' => 'Picks', 'url' => '/pick'],
          ['label' => 'Pick Lines', 'url' => '/pickline'],
          ['label' => 'Pick Details', 'url' => '/pickdet'],
//          ['label' => 'Sales Order Lines', 'url' => '/soline'],
        ]
      ],
      Yii::$app->user->isGuest
        ? ['label' => 'Login', 'url' => ['/user/security/login']]
        : '<li class="nav-item abc">'
        . Html::beginForm(['/site/logout'])
        . Html::submitButton(
          'Logout (' . Yii::$app->user->identity->username . ')',
          ['class' => 'nav-link btn btn-sm btn-link logout']
        )
        . Html::endForm()
        . '</li>',
      Yii::$app->user->isGuest
        ? ['label' => 'Sign Up', 'url' => ['/user/registration/register']] : ['label' => ''],
    ]
  ]);
  NavBar::end();
  ?>
</header>

<main id="main" class="flex-shrink-0" role="main">
  <div class="container-fluid">
    <?php if (!empty($this->params['breadcrumbs'])): ?>
      <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
    <?php endif ?>
    <?= Alert::widget() ?>
    <?= $content ?>
  </div>
</main>
<div id="lkup_modal" class="modal fade" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="lkup_title"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div id="lkup_tabulator">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<footer id="footer" class="footer mb-0 text-600">
  <div class="container">
    <div class="row text-muted">
      <div class="col-md-6 text-center text-md-start">&copy; SOCAL WMS <?= date('Y') ?></div>
    </div>
  </div>
</footer>
<?php
try {
  $this->registerJsFile(
    '@web/js/lib/axios.min.js'
  );
  $this->registerJsFile(
    '@web/js/lib/store2.min.js'
  );
  $this->registerJsFile(
    '@web/js/apis.js',
    ['depends' => [\yii\web\JqueryAsset::class]]
  );
} catch (\yii\base\InvalidConfigException $e) {
}
?>

<?php
//echo \yii\helpers\Html::script(null, ['type' => 'module', 'src' => 'js/apis.js']);
$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
