<div class="form-group" id="add-inv">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Inv',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'prod_variant_id' => [
            'label' => 'Product variant',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdVariant::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Product variant'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'uom_id' => [
            'label' => 'Uom',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->orderBy('uom')->asArray()->all(), 'id', 'uom'),
                'options' => ['placeholder' => 'Choose Uom'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'qty' => ['type' => TabularForm::INPUT_TEXT],
        'note' => ['type' => TabularForm::INPUT_TEXT],
        'noted_by' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowInv(' . $key . '); return false;', 'id' => 'inv-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Inv', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowInv()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

