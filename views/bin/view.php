<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Bin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bin-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Bin'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        'name',
        'zone_id',
        'description',
        'active',
        'excl_fr_avail',
        'excl_fr_fcast',
        'allow_neg_inv',
        'seq',
        'erp_int',
        'excl_rec',
        'excl_ship',
        'erp_loc',
        'pallet_spaces',
        'primary_zone_id',
        'excl_from_recon',
        'reverse_recon',
        'reverse_recon_rsnkey',
        'excl_cont',
        'allow_ship_stage',
        'perm_move_in',
        'perm_move_out',
        'disallow_hold_inv',
        'height_level',
        'row',
        'col',
        'is_item_restricted',
        'loc_positions',
        'is_shipping_lane',
        'is_consolidation_area',
        'is_pack_area',
        'auto_unload_cont',
        'excl_count',
        'require_cont',
        'is_single_item_only',
        'is_pick_area',
        'pickgroup_id',
        'length',
        'width',
        'height',
        'flagged_for_count',
        'pickroute_id',
        'rank',
        'max_wt',
        'flagged_for_putaway',
        'is_putaway_hold',
        'putaway_hold_comment',
        'is_pick_hold',
        'pick_hold_comment',
        'latitude',
        'longitude',
        'last_count_date',
        'floor',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerInv->totalCount){
    $gridColumnInv = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'prodVariant.name',
                'label' => 'Product Variant'
            ],
            [
                'attribute' => 'uom.uom',
                'label' => 'Uom'
            ],
            'qty',
                        'note',
            'noted_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInv,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv'),
        ],
        'columns' => $gridColumnInv
    ]);
}
?>
    </div>
</div>
