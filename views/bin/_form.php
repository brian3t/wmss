<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bin */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
  $model->active = 1;
  $model->seq = 1;
  $model->floor = 0;
}

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Inv',
        'relID' => 'inv',
        'value' => \yii\helpers\Json::encode($model->invs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="bin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'zone_id')->textInput(['placeholder' => 'Zone']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'active')->checkbox() ?>
<!--
    <?/*= $form->field($model, 'excl_fr_avail')->checkbox() */?>

    <?/*= $form->field($model, 'excl_fr_fcast')->checkbox() */?>

    <?/*= $form->field($model, 'allow_neg_inv')->checkbox() */?>

    <?/*= $form->field($model, 'seq')->textInput(['placeholder' => 'Seq']) */?>

    <?/*= $form->field($model, 'erp_int')->textInput(['placeholder' => 'Erp Int']) */?>

    <?/*= $form->field($model, 'excl_rec')->checkbox() */?>

    <?/*= $form->field($model, 'excl_ship')->checkbox() */?>

    <?/*= $form->field($model, 'erp_loc')->textInput(['maxlength' => true, 'placeholder' => 'Erp Loc']) */?>

    <?/*= $form->field($model, 'pallet_spaces')->textInput(['maxlength' => true, 'placeholder' => 'Pallet Spaces']) */?>

    <?/*= $form->field($model, 'primary_zone_id')->textInput(['placeholder' => 'Primary Zone']) */?>

    <?/*= $form->field($model, 'excl_from_recon')->checkbox() */?>

    <?/*= $form->field($model, 'reverse_recon')->checkbox() */?>

    <?/*= $form->field($model, 'reverse_recon_rsnkey')->textInput(['placeholder' => 'Reverse Recon Rsnkey']) */?>

    <?/*= $form->field($model, 'excl_cont')->checkbox() */?>

    <?/*= $form->field($model, 'allow_ship_stage')->checkbox() */?>

    <?/*= $form->field($model, 'perm_move_in')->textInput(['maxlength' => true, 'placeholder' => 'Perm Move In']) */?>

    <?/*= $form->field($model, 'perm_move_out')->textInput(['maxlength' => true, 'placeholder' => 'Perm Move Out']) */?>

    <?/*= $form->field($model, 'disallow_hold_inv')->checkbox() */?>

    <?/*= $form->field($model, 'height_level')->textInput(['maxlength' => true, 'placeholder' => 'Height Level']) */?>

    <?/*= $form->field($model, 'row')->textInput(['placeholder' => 'Row']) */?>

    <?/*= $form->field($model, 'col')->textInput(['placeholder' => 'Col']) */?>

    <?/*= $form->field($model, 'is_item_restricted')->checkbox() */?>

    <?/*= $form->field($model, 'loc_positions')->textInput(['maxlength' => true, 'placeholder' => 'Loc Positions']) */?>

    <?/*= $form->field($model, 'is_shipping_lane')->checkbox() */?>

    <?/*= $form->field($model, 'is_consolidation_area')->checkbox() */?>

    <?/*= $form->field($model, 'is_pack_area')->checkbox() */?>

    <?/*= $form->field($model, 'auto_unload_cont')->checkbox() */?>

    <?/*= $form->field($model, 'excl_count')->checkbox() */?>

    <?/*= $form->field($model, 'require_cont')->checkbox() */?>

    <?/*= $form->field($model, 'is_single_item_only')->checkbox() */?>

    <?/*= $form->field($model, 'is_pick_area')->checkbox() */?>

    <?/*= $form->field($model, 'pickgroup_id')->textInput(['placeholder' => 'Pickgroup']) */?>

    <?/*= $form->field($model, 'length')->textInput(['maxlength' => true, 'placeholder' => 'Length']) */?>

    <?/*= $form->field($model, 'width')->textInput(['maxlength' => true, 'placeholder' => 'Width']) */?>

    <?/*= $form->field($model, 'height')->textInput(['maxlength' => true, 'placeholder' => 'Height']) */?>

    <?/*= $form->field($model, 'flagged_for_count')->checkbox() */?>

    <?/*= $form->field($model, 'pickroute_id')->textInput(['placeholder' => 'Pickroute']) */?>

    <?/*= $form->field($model, 'rank')->textInput(['maxlength' => true, 'placeholder' => 'Rank']) */?>

    <?/*= $form->field($model, 'max_wt')->textInput(['maxlength' => true, 'placeholder' => 'Max Wt']) */?>

    <?/*= $form->field($model, 'flagged_for_putaway')->checkbox() */?>

    <?/*= $form->field($model, 'is_putaway_hold')->checkbox() */?>

    <?/*= $form->field($model, 'putaway_hold_comment')->textInput(['maxlength' => true, 'placeholder' => 'Putaway Hold Comment']) */?>

    <?/*= $form->field($model, 'is_pick_hold')->checkbox() */?>

    <?/*= $form->field($model, 'pick_hold_comment')->textInput(['maxlength' => true, 'placeholder' => 'Pick Hold Comment']) */?>

    <?/*= $form->field($model, 'latitude')->textInput(['maxlength' => true, 'placeholder' => 'Latitude']) */?>

    --><?/*= $form->field($model, 'longitude')->textInput(['maxlength' => true, 'placeholder' => 'Longitude']) */?>

    <?= $form->field($model, 'last_count_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Last Count Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'floor')->textInput(['placeholder' => 'Floor']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Inv'),
            'content' => $this->render('_formInv', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invs),
            ]),
        ],
    ];
    /*echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);*/
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
