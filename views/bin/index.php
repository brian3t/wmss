<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Bin';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="bin-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bin', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        /*[
                'attribute' => 'comp_id',
                'label' => 'Company',
                'value' => function($model){
                    return $model->company->name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Comp', 'id' => 'grid--comp_id']
            ],*/
        'name',
        'zone_id',
        'description',
        'active',
        'excl_fr_avail',
        'excl_fr_fcast',
        'allow_neg_inv',
        'seq',
        'erp_int',
        'excl_rec',
        'excl_ship',
        'erp_loc',
        'pallet_spaces',
        'primary_zone_id',
        'excl_from_recon',
        'reverse_recon',
        'reverse_recon_rsnkey',
        'excl_cont',
        'allow_ship_stage',
        'perm_move_in',
        'perm_move_out',
        'disallow_hold_inv',
        'height_level',
        'row',
        'col',
        'is_item_restricted',
        'loc_positions',
        'is_shipping_lane',
        'is_consolidation_area',
        'is_pack_area',
        'auto_unload_cont',
        'excl_count',
        'require_cont',
        'is_single_item_only',
        'is_pick_area',
        'pickgroup_id',
        'length',
        'width',
        'height',
        'flagged_for_count',
        'pickroute_id',
        'rank',
        'max_wt',
        'flagged_for_putaway',
        'is_putaway_hold',
        'putaway_hold_comment',
        'is_pick_hold',
        'pick_hold_comment',
        'latitude',
        'longitude',
        'last_count_date',
        'floor',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-bin']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
