<div class="form-group" id="add-prod-uom">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ProdUom',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'prod_id' => [
            'label' => 'Product',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Prod::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Prod'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'comp_id' => [
            'label' => 'Comp',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Comp'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'cnv_to_stock' => ['type' => TabularForm::INPUT_TEXT],
        'decimals_ovrd' => ['type' => TabularForm::INPUT_TEXT],
        'weight' => ['type' => TabularForm::INPUT_TEXT],
        'volume' => ['type' => TabularForm::INPUT_TEXT],
        'length' => ['type' => TabularForm::INPUT_TEXT],
        'width' => ['type' => TabularForm::INPUT_TEXT],
        'height' => ['type' => TabularForm::INPUT_TEXT],
        'upc' => ['type' => TabularForm::INPUT_TEXT],
        'shippable_pack_series_id' => ['type' => TabularForm::INPUT_TEXT],
        'enabled' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'lot_specific_cnv' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowProdUom(' . $key . '); return false;', 'id' => 'prod-uom-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Product Uom', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProdUom()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

