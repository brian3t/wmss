<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Uom */
/* @var $form yii\widgets\ActiveForm */

/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Inv',
        'relID' => 'inv',
        'value' => \yii\helpers\Json::encode($model->invs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'ProdUom',
        'relID' => 'prod-uom',
        'value' => \yii\helpers\Json::encode($model->prodUoms),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);*/
?>

<div class="uom-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'desc')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'uom_type')->textInput(['placeholder' => 'Uom Type']) ?>

    <?= $form->field($model, 'is_base')->checkbox() ?>

    <?= $form->field($model, 'cnv_to_base')->textInput(['maxlength' => true, 'placeholder' => 'Convert To Base']) ?>

    <?= $form->field($model, 'erp_key')->textInput(['placeholder' => 'Erp Key']) ?>

    <?= $form->field($model, 'erp_uom')->textInput(['maxlength' => true, 'placeholder' => 'Erp Uom']) ?>

    <?php
    /*$forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Inv'),
            'content' => $this->render('_formInv', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->invs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ProdUom'),
            'content' => $this->render('_formProdUom', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->prodUoms),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);*/
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
