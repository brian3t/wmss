<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Uom */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Uom', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="uom-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Uom'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'desc',
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        'uom_type',
        'is_base',
        'cnv_to_base',
        'erp_key',
        'erp_uom',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
/*if($providerInv->totalCount){
    $gridColumnInv = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'prodVariant.name',
                'label' => 'Prod Variant'
            ],
                        'qty',
            [
                'attribute' => 'bin.name',
                'label' => 'Bin'
            ],
            'note',
            'noted_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInv,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv'),
        ],
        'columns' => $gridColumnInv
    ]);
}*/
?>
    </div>

    <div class="row">
<?php
/*if($providerProdUom->totalCount){
    $gridColumnProdUom = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'prod.name',
                'label' => 'Prod'
            ],
                        [
                'attribute' => 'company.name',
                'label' => 'Company'
            ],
            'cnv_to_stock',
            'decimals_ovrd',
            'weight',
            'volume',
            'length',
            'width',
            'height',
            'upc',
            'shippable_pack_series_id',
            'enabled',
            'lot_specific_cnv',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProdUom,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-uom']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Product Uom'),
        ],
        'columns' => $gridColumnProdUom
    ]);
}*/
?>
    </div>
</div>
