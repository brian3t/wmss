<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile(
  '@web/js/pick_form.js',
  ['depends' => [\yii\web\JqueryAsset::class]]
);

/* @var $this yii\web\View */
/* @var $model app\models\Pick */
/* @var $form yii\widgets\ActiveForm */
if ($model->isNewRecord) {
  $model->ok_to_post = 0;
}

?>

<div class="pick-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::class, [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Choose Comp'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <!--    --><?php //= $form->field($model, 'whse_id')->textInput(['placeholder' => 'Whse']) ?>

  <?= $form->field($model, 'so_id')->widget(\kartik\widgets\Select2::class, [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\So::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
    'options' => ['placeholder' => 'Choose Sales Order'],
    'pluginOptions' => [
      'allowClear' => true,
    ],
  ]); ?>

  <!--    --><?php //= $form->field($model, 'name')->textInput(['placeholder' => 'Pick No.']) ?>

  <!--  --><?php //= $form->field($model, 'tran_cat')->textInput(['maxlength' => true, 'placeholder' => 'Transaction Category']) ?>

  <?= $form->field($model, 'sub_cat')->textInput(['maxlength' => true, 'placeholder' => 'Choose Sub Category', 'class' => 'all_cap']) ?>

  <?= $form->field($model, 'ok_to_post')->checkbox() ?>

  <?= $form->field($model, 'is_complete')->checkbox() ?>

  <?= $form->field($model, 'is_released')->checkbox() ?>

  <!--    --><?php //= $form->field($model, 'rpt_group_id')->textInput(['placeholder' => 'Report Group']) ?>

  <!--    --><?php //= $form->field($model, 'pick_rule_id')->textInput(['placeholder' => 'Pick Rule']) ?>

  <?= $form->field($model, 'bol_notes')->textarea(['rows' => 6]) ?>

  <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

  <?= $form->field($model, 'is_bol_printed')->checkbox() ?>

  <!--    --><?php //= $form->field($model, 'shipmeth_id')->textInput(['placeholder' => 'Ship method']) ?>

  <?= $form->field($model, 'is_expedite')->checkbox() ?>

  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
