<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pick */

$this->title = 'Create Pick';
$this->params['breadcrumbs'][] = ['label' => 'Pick', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pick-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
