<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pick */

?>
<div class="pick-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'comp.name',
            'label' => 'Comp',
        ],
        'whse_id',
        'name',
        'tran_cat',
        'sub_cat',
        'ok_to_post',
        'is_complete',
        [
            'attribute' => 'so.name',
            'label' => 'So',
        ],
        'is_released',
        'rpt_group_id',
        'pick_rule_id',
        'bol_notes:ntext',
        'notes:ntext',
        'is_bol_printed',
        'shipmeth_id',
        'is_expedite',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>