<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pick */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pick', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pick-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Pick'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => true],
        [
            'attribute' => 'comp.name',
            'label' => 'Comp',
        ],
//        'whse_id',
        'name',
        'tran_cat',
        'sub_cat',
        'ok_to_post:boolean',
        'is_complete:boolean',
        [
            'attribute' => 'so.name',
            'label' => 'SO',
        ],
        'is_released:boolean',
        'rpt_group_id',
        'pick_rule_id',
        'bol_notes:ntext',
        'notes:ntext',
        'is_bol_printed:boolean',
        'shipmeth_id',
        'is_expedite:boolean',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

   <div class="row">
<?php
if($providerPickline->totalCount) {
    $gridColumnPickline = [
      ['class' => 'yii\grid\SerialColumn'],
      ['attribute' => 'id', 'visible' => false],
      'seq',
      [
        'attribute' => 'assignedTo.username',
        'label' => 'Assigned To'
      ],
      [
        'attribute' => 'prodVar.name',
        'label' => 'Prod Variant'
      ],
      [
        'attribute' => 'soline.id',
        'label' => 'SO Line ID'
      ],
      'description',
      'wh_id',
      [
        'attribute' => 'bin.name',
        'label' => 'Bin'
      ],
      'zone_id',
      'qty_to_pick',
      'orig_qty_to_pick',
      [
        'attribute' => 'prodUom.id',
        'label' => 'Prod Uom'
      ],
      'min_qty_to_pick',
      'max_qty_to_pick',
      'decimals',
      'cust_id',
      'cust_addr_id',
      'completed_at',
      'ship_addr_id',
      [
        'attribute' => 'pickCompletedBy.username',
        'label' => 'Pick Completed By'
      ],
      'note',
    ];
    echo Gridview::widget([
      'dataProvider' => $providerPickline,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pickline']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pickline'),
      ],
      'columns' => $gridColumnPickline,
    'condensed' => true,
    ]);
}
?>
   </div>
</div>
