<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use soc\yiiuser\User\Model\SessionHistory;
use soc\yiiuser\User\Search\SessionHistorySearch;
use soc\yiiuser\User\Widget\SessionStatusWidget;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var SessionHistorySearch $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var \soc\yiiuser\User\Model\User $user
 * @var \soc\yiiuser\User\Module $module
 */
?>

<?php $this->beginContent($module->viewPath. '/admin/update.php', ['user' => $user]) ?>
    <div class="row">
        <div class="col-xs-12">
            <?= Html::a(
                Yii::t('app', 'Terminate all sessions'),
                ['/user/admin/terminate-sessions', 'id' => $user->id],
                [
                    'class' => 'btn btn-danger btn-xs pull-right',
                    'data-method' => 'post'
                ]
            ) ?>
        </div>
    </div>
    <hr>

<?php Pjax::begin(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'user_agent',
        'ip',
        [
            'contentOptions' => [
                'class' => 'text-nowrap',
            ],
            'label' => Yii::t('app', 'Status'),
            'value' => function (SessionHistory $model) {
                return SessionStatusWidget::widget(['model' => $model]);
            },
        ],
        [
            'attribute' => 'updated_at',
            'format' => 'datetime'
        ],
    ],
]); ?>
<?php Pjax::end(); ?>

<?php $this->endContent() ?>
