<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

/**
 * @var String $code
 */
?>
<?= Yii::t('app', 'Hello') ?>,

<?= Yii::t('app', 'This is the code to insert to enable two factor authentication') ?>:

<?= $code ?>

<?= Yii::t('app', 'If you did not make this request you can ignore this email') ?>.
