<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InvJour */

$this->title = 'Create Inv Jour';
$this->params['breadcrumbs'][] = ['label' => 'Inv Jour', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-jour-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
