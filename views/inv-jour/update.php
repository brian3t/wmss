<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InvJour */

$this->title = 'Update Inv Jour: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inv Jour', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="inv-jour-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
