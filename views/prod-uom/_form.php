<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProdUom */
/* @var $form yii\widgets\ActiveForm */
if ($model->isNewRecord){
//    $model->cnv_to_base = null
}

?>

<div class="prod-uom-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'prod_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Prod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Prod'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'uom_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Uom'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'cnv_to_stock')->textInput(['maxlength' => true, 'placeholder' => 'Cnv To Stock']) ?>

    <?= $form->field($model, 'decimals_ovrd')->textInput(['placeholder' => 'Decimals Ovrd']) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true, 'placeholder' => 'Weight']) ?>

    <?= $form->field($model, 'volume')->textInput(['maxlength' => true, 'placeholder' => 'Volume']) ?>

    <?= $form->field($model, 'length')->textInput(['maxlength' => true, 'placeholder' => 'Length']) ?>

    <?= $form->field($model, 'width')->textInput(['maxlength' => true, 'placeholder' => 'Width']) ?>

    <?= $form->field($model, 'height')->textInput(['maxlength' => true, 'placeholder' => 'Height']) ?>

    <?= $form->field($model, 'upc')->textInput(['maxlength' => true, 'placeholder' => 'Upc']) ?>

    <?= $form->field($model, 'shippable_pack_series_id')->textInput(['placeholder' => 'Shippable Pack Series']) ?>

    <?= $form->field($model, 'enabled')->checkbox() ?>

    <?= $form->field($model, 'lot_specific_cnv')->textInput(['placeholder' => 'Lot Specific Conversion']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
