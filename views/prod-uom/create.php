<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProdUom */

$this->title = 'Create Product Uom';
$this->params['breadcrumbs'][] = ['label' => 'Product Uom', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-uom-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
