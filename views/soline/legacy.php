<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Soline';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="soline-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Soline', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'so',
        'company',
        'customer',
        'cust_id',
        'lineid',
        'seq',
        'void',
        'erp_id',
        'erp_soline',
        'wh',
        'description',
        'non_inv',
        'qty_ord',
        'qty_shipped',
        'erp_qty_open_to_ship',
        'ok_to_pick',
        'pickrule',
        'uom',
        'cnv_to_stk',
        'erp_so_id',
        'erp_so',
        'req_ship_date',
        'promise_date',
        'shipmeth',
        'ship_priority',
        'ship_to',
        'ship_contact',
        'ship_email:email',
        'addr1',
        'addr2',
        'addr3',
        'addr4',
        'city',
        'state',
        'postal',
        'country_code',
        'phone1',
        'phone2',
        'cancel_date',
        'additional_info',
        'extra_field_1',
        'extra_field_2',
        'extra_field_3',
        [
                'attribute' => 'uom_id',
                'label' => 'Uom',
                'value' => function($model){
                    return $model->uom0->name ?? 'UOM not mapped yet. Please map ERP UOM to WMSS UOM';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Uom', 'id' => 'grid--uom_id']
            ],
        [
                'attribute' => 'so_id',
                'label' => 'So',
                'value' => function($model){
                    return $model->so0->name ?? 'SO not mapped yet. Please map ERP SO with WMSS SO';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\So::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'So', 'id' => 'grid--so_id']
            ],
        'addr_hash',
        'wt_per',
        'comment',
        'decimals',
        'unit_price',
        'ext_price',
        'is_backorder',
        [
                'attribute' => 'comp_id',
                'label' => 'Comp',
                'value' => function($model){
                    return $model->comp->name ?? 'Company not mapped yet. Please map ERP Company with WMSS Company';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Comp', 'id' => 'grid--comp_id']
            ],
        'wh_id',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-soline']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
