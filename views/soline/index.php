<?php

/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Sales Order Lines';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
try {
  $this->registerJsFile(
    '@web/js/soline.js',
    ['depends' => [\yii\web\JqueryAsset::class]]
  );
} catch (\yii\base\InvalidConfigException $e) {
}
?>
<div class="so-index">

  <h2><?= Html::encode($this->title) ?></h2>

  <p>
    <?= Html::a('Create Sales Order Line', ['create'], ['class' => 'btn btn-primary btn-sm']) ?>
  </p>
  <!-- Col Select Modal -->
  <div class="modal fade" id="col_sel_mod" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="col_sel_mod_label">Choose Columns</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="col_selector">
            <div class="form-check"><label class="form-check-label" for="c_s_name">
                Order
              </label><input class="form-check-input" type="checkbox" data-col="name" checked id="c_s_name"></div>
            <div class="form-check"><label class="form-check-label" for="c_s_created_at">
                Created At
              </label><input class="form-check-input" type="checkbox" data-col="created_at" checked id="c_s_created_at">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_updated_at">
                Updated At
              </label><input class="form-check-input" type="checkbox" data-col="updated_at" checked id="c_s_updated_at">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_updated_by">
                Updated By
              </label><input class="form-check-input" type="checkbox" data-col="updated_by" checked id="c_s_updated_by">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_wh_id">
                Warehouse
              </label><input class="form-check-input" type="checkbox" data-col="wh_id" checked id="c_s_wh_id">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_void">
                Void
              </label><input class="form-check-input" type="checkbox" data-col="void" checked id="c_s_void">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_custid">
                Customer
              </label><input class="form-check-input" type="checkbox" data-col="custid" checked id="c_s_custid">
            </div>
            <div class="form-check"><label class="form-check-label" for="c_s_ord_type">
                Order Type
              </label><input class="form-check-input" type="checkbox" data-col="ord_type" checked id="c_s_ord_type">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Apply</button>
      </div>
    </div>
  </div>
</div>
  <div class="tabulator" id="so_tabulator"></div>
<button type="button" id="col_sel_mod_btn" class="hidden" data-bs-toggle="modal" data-bs-target="#col_sel_mod">
  Launch demo modal
</button>


</div>
