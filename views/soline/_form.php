<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Soline */
/* @var $form yii\widgets\ActiveForm */
if ($model->isNewRecord) {
  $model->ok_to_pick = 1;
  $model->cnv_to_stk = 1;
  $model->decimals = 4;
}
?>

<div class="soline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'so')->textInput(['maxlength' => true, 'placeholder' => 'So']) ?>

<?= $form->field($model, 'company')->widget(\kartik\widgets\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('name')->asArray()->all(), 'name', 'name'),
        'options' => ['placeholder' => 'Choose Company'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'customer')->textInput(['maxlength' => true, 'placeholder' => 'Customer']) ?>

<!--    --><?php //= $form->field($model, 'cust_id')->textInput(['placeholder' => 'Customer']) ?>

    <?= $form->field($model, 'lineid')->textInput(['maxlength' => true, 'placeholder' => 'Lineid']) ?>

    <?= $form->field($model, 'seq')->textInput(['placeholder' => 'Seq']) ?>

    <?= $form->field($model, 'void')->checkbox() ?>

    <?= $form->field($model, 'erp_id')->textInput(['placeholder' => 'Erp']) ?>

    <?= $form->field($model, 'erp_soline')->textInput(['maxlength' => true, 'placeholder' => 'Erp Soline']) ?>

<!--    --><?php //= $form->field($model, 'wh')->textInput(['maxlength' => true, 'placeholder' => 'Warehouse']) ?>

<?= $form->field($model, 'prod_var')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdVariant::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Product Variant'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'non_inv')->checkbox() ?>

    <?= $form->field($model, 'qty_ord')->textInput(['placeholder' => 'Qty Ord']) ?>

    <?= $form->field($model, 'qty_shipped')->textInput(['placeholder' => 'Qty Shipped']) ?>

    <?= $form->field($model, 'erp_qty_open_to_ship')->textInput(['placeholder' => 'ERP Qty Open To Ship']) ?>

    <?= $form->field($model, 'ok_to_pick')->checkbox() ?>

    <?= $form->field($model, 'pickrule')->textInput(['maxlength' => true, 'placeholder' => 'Pickrule']) ?>

<?= $form->field($model, 'uom')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose UOM'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model, 'cnv_to_stk')->textInput(['maxlength' => true, 'placeholder' => 'Convert To Stock']) ?>

    <?= $form->field($model, 'erp_so_id')->textInput(['placeholder' => 'Erp So']) ?>

    <?= $form->field($model, 'erp_so')->textInput(['maxlength' => true, 'placeholder' => 'Erp So']) ?>

    <?= $form->field($model, 'req_ship_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Req Ship Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'promise_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Promise Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'shipmeth')->textInput(['maxlength' => true, 'placeholder' => 'Shipmeth']) ?>

    <?= $form->field($model, 'ship_priority')->textInput(['placeholder' => 'Ship Priority']) ?>

    <?= $form->field($model, 'ship_to')->textInput(['maxlength' => true, 'placeholder' => 'Ship To']) ?>

    <?= $form->field($model, 'ship_contact')->textInput(['maxlength' => true, 'placeholder' => 'Ship Contact']) ?>

    <?= $form->field($model, 'ship_email')->textInput(['maxlength' => true, 'placeholder' => 'Ship Email']) ?>

    <?= $form->field($model, 'addr1')->textInput(['maxlength' => true, 'placeholder' => 'Addr1']) ?>

    <?= $form->field($model, 'addr2')->textInput(['maxlength' => true, 'placeholder' => 'Addr2']) ?>

    <?= $form->field($model, 'addr3')->textInput(['maxlength' => true, 'placeholder' => 'Addr3']) ?>

    <?= $form->field($model, 'addr4')->textInput(['maxlength' => true, 'placeholder' => 'Addr4']) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => 'City']) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true, 'placeholder' => 'State']) ?>

    <?= $form->field($model, 'postal')->textInput(['maxlength' => true, 'placeholder' => 'Postal']) ?>

    <?= $form->field($model, 'country_code')->textInput(['maxlength' => true, 'placeholder' => 'Country Code']) ?>

    <?= $form->field($model, 'phone1')->textInput(['maxlength' => true, 'placeholder' => 'Phone1']) ?>

    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true, 'placeholder' => 'Phone2']) ?>

    <?= $form->field($model, 'cancel_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Cancel Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'additional_info')->textInput(['maxlength' => true, 'placeholder' => 'Additional Info']) ?>

    <?= $form->field($model, 'extra_field_1')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 1']) ?>

    <?= $form->field($model, 'extra_field_2')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 2']) ?>

    <?= $form->field($model, 'extra_field_3')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 3']) ?>

<!--    --><?php /*= $form->field($model, 'uom_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Uom'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */?>

    <?php /*= $form->field($model, 'so_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\So::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose So'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */?>

    <?= $form->field($model, 'wt_per')->textInput(['maxlength' => true, 'placeholder' => 'Wt Per']) ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true, 'placeholder' => 'Comment']) ?>

    <?= $form->field($model, 'decimals')->textInput(['placeholder' => 'Decimals']) ?>

    <?= $form->field($model, 'unit_price')->textInput(['maxlength' => true, 'placeholder' => 'Unit Price']) ?>

    <?= $form->field($model, 'ext_price')->textInput(['maxlength' => true, 'placeholder' => 'Ext Price']) ?>

    <?= $form->field($model, 'is_backorder')->checkbox() ?>

    <?php /*= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */?>

    <?php /*= $form->field($model, 'prod_var_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdVariant::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Product Variant'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */?>

<!--    --><?php //= $form->field($model, 'wh_id')->textInput(['placeholder' => 'Warehouse']) ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
