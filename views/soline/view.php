<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//use yii\widgets\DetailView;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Soline */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Soline', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soline-view">

  <div class="row">
    <div class="col-sm-8">
      <h2><?= 'Soline' . ' ' . Html::encode($this->title) ?></h2>
    </div>
    <div class="col-sm-4" style="margin-top: 15px">
      <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
      <?= Html::a('Raw Data', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Are you sure you want to delete this item?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      'so',
      'company',
      'customer',
      'cust_id',
      'lineid',
      'seq',
      'void',
      'erp_id',
      'erp_soline',
      'wh',
      'description',
      'non_inv',
      'qty_ord',
      'qty_shipped',
      'erp_qty_open_to_ship',
      'ok_to_pick',
      'pickrule',
      'uom',
      'cnv_to_stk',
      'erp_so_id',
      'erp_so',
      'req_ship_date',
      'promise_date',
      'shipmeth',
      'ship_priority',
      'ship_to',
      'ship_contact',
      'ship_email:email',
      'addr1',
      'addr2',
      'addr3',
      'addr4',
      'city',
      'state',
      'postal',
      'country_code',
      'phone1',
      'phone2',
      'cancel_date',
      'additional_info',
      'extra_field_1',
      'extra_field_2',
      'extra_field_3',
      [
        'attribute' => 'uom0',
        'label' => 'Uom',
        'value' => 'UOM',
        'valueColOptions' => ['style' => 'width:30%']
      ],
      [
        'attribute' => 'so0',
        'label' => 'So',
        'value' => 'SO'
      ],
      'addr_hash',
      'wt_per',
      'comment',
      'decimals',
      'unit_price',
      'ext_price',
      'is_backorder',
      [
        'attribute' => 'comp',
        'label' => 'Comp',
        'value' => 'Company'
      ],
      'wh_id',
    ];
    /*echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);*/

    // DetailView Attributes Configuration
    $attributes = [
      [
        'columns' => [
          [
            'attribute' => 'id',
            'label' => 'ID',
            'displayOnly' => true,
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'so',
            'format' => 'raw',
            'value' => '<kbd>' . $model->so . '</kbd>',
            'valueColOptions' => ['style' => 'width:16%'],
            'displayOnly' => true
          ],
          [
            'attribute' => 'lineid',
            'valueColOptions' => ['style' => 'width:16%'],
          ],
        ],
      ],
      [
        'columns' => [
          [
            'attribute' => 'created_at',
            'format' => 'date',
            'type' => DetailView::INPUT_DATE,
            'widgetOptions' => [
              'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'ok_to_pick',
            'label' => 'OK To Pick',
            'format' => 'raw',
            'value' => $model->ok_to_pick ? '<span class="badge bg-success badge-success">Yes</span>' : '<span class="badge badge-danger bg-danger">No</span>',
            'type' => DetailView::INPUT_SWITCH,
            'widgetOptions' => [
              'pluginOptions' => [
                'onText' => 'Yes',
                'offText' => 'No',
              ]
            ],
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'void',
            'label' => 'Voided',
            'format' => 'raw',
            'value' => $model->void ? '<span class="badge badge-success">Yes</span>' : '<span class="badge badge-danger bg-danger">No</span>',
            'type' => DetailView::INPUT_SWITCH,
            'widgetOptions' => [
              'pluginOptions' => [
                'onText' => 'Yes',
                'offText' => 'No',
              ]
            ],
            'valueColOptions' => ['style' => 'width:16%']
          ]
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'customer',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'cust_id',
            'label' => 'Customer ID',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'seq',
            'label' => 'Sequence',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          ['attribute' => 'prod_var_id',
            'label' => 'Product Variation',
            'format' => 'raw',
            'value' => $model->getProdVar() ? $model->getProdVar()->one()->name : 'Please select product variant',
            'valueColOptions' => ['style' => 'width:50%']
          ],
          ['attribute' => 'uom_id',
            'label' => 'UOM',
            'format' => 'raw',
            'value' => $model->getUom0() ? $model->getUom0()->one()->name : 'Please select product variant'
          ]
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'description',
//            'valueColOptions' => ['style' => 'width:16%']
          ],/*
          [
            'attribute' => 'non_inv',
            'label' => 'Non-Inventory',
            'format' => 'raw',
            'value' => $model->non_inv ? '<span class="badge badge-success">Yes</span>' : '<span class="badge badge-danger bg-danger">No</span>',
            'type' => DetailView::INPUT_SWITCH,
            'widgetOptions' => [
              'pluginOptions' => [
                'onText' => 'Yes',
                'offText' => 'No',
              ]
            ],
            'valueColOptions' => ['style' => 'width:16%']
          ]*/
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'qty_ord',
            'label' => 'Qty Ordered',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'qty_shipped',
            'label' => 'Qty Shipped',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'pickrule',
            'label' => 'Pick Rule',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'uom',
            'label' => 'UOM',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'cnv_to_stk',
            'label' => 'Convert To Stock',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'req_ship_date',
            'label' => 'Requested Ship Date',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'promise_date',
            'label' => 'Promised Date',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'shipmeth',
            'label' => 'Ship Method',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'ship_priority',
            'label' => 'Ship Priority',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'ship_to',
            'label' => 'Ship To',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'ship_contact',
            'label' => 'Ship Contact',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'ship_email',
            'label' => 'Ship Email',
            'labelColOptions' => ['style' => 'width: 10%;text-align:right;'],
            'valueColOptions' => ['style' => 'width:23%']
          ],
          [
            'attribute' => 'addr1',
            'label' => 'Address 1',
            'labelColOptions' => ['style' => 'width: 10%;text-align:right;'],
            'valueColOptions' => ['style' => 'width:23%']
          ],
          [
            'attribute' => 'addr2',
            'label' => 'Address 2',
            'labelColOptions' => ['style' => 'width: 10%;text-align:right;'],
            'valueColOptions' => ['style' => 'width:23%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'addr3',
            'label' => 'Address 3',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'addr4',
            'label' => 'Address 4',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'city',
            'label' => 'City',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'state',
            'label' => 'State',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'postal',
            'label' => 'Postal',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'country_code',
            'label' => 'Country Code',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'phone1',
            'label' => 'Phone 1',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'phone2',
            'label' => 'Phone 2',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'cancel_date',
            'label' => 'Cancel Date',
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ]
      ],
      [
        'attribute' => 'additional_info',
        'type' => DetailView::INPUT_TEXTAREA,
        'options' => ['rows' => 4]
      ],
      [
        'attribute' => 'comment',
        'format' => 'raw',
        'value' => '<span class="text-justify"><em>' . $model->comment . '</em></span>',
        'type' => DetailView::INPUT_TEXTAREA,
        'options' => ['rows' => 4]
      ],
      [
        'columns' => [
          [
            'attribute' => 'wt_per',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'decimals',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'unit_price',
          ],
        ]
      ],
      [
        'group' => true,
        'label' => 'ERP Detail',
        'rowOptions' => ['class' => 'table-info']
      ],
      [
        'columns' => [[
          'attribute' => 'erp_id',
          'label' => 'ERP ID',
          'valueColOptions' => ['style' => 'width:16%']
        ],
          [
            'attribute' => 'erp_soline',
            'label' => 'ERP SO Line',
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'erp_qty_open_to_ship',
            'label' => 'ERP Qty Open to ship',
            'valueColOptions' => ['style' => 'width:16%']
          ]
        ]
      ],
      [
        'columns' => [
          [
            'attribute' => 'erp_so_id',
            'label' => 'ERP Sales Order ID',
            'displayOnly' => true,
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'erp_so',
            'label' => 'ERP Sales Order',
            'displayOnly' => true,
            'valueColOptions' => ['style' => 'width:16%']
          ],
          [
            'attribute' => 'erp_soline',
            'label' => 'ERP Sales Order Line',
            'displayOnly' => true,
            'valueColOptions' => ['style' => 'width:16%']
          ],
        ],
      ],
    ];

    // View file rendering the widget
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $attributes,
      'mode' => 'view',
      'bordered' => false,
      'striped' => false,
      'condensed' => true,
      'hAlign' => DetailView::ALIGN_RIGHT,
      'panel' => [
        'type' => DetailView::TYPE_PRIMARY,
        'heading' => '  ',
      ],
      'deleteOptions' => [ // your ajax delete parameters
        'params' => ['id' => 1000, 'kvdelete' => true],
      ],
      'container' => ['id' => 'kv-demo'],
      'formOptions' => ['action' => Url::current(['#' => 'kv-demo'])] // your action to delete
    ]);

    // Controller action
    /*public function actionDetailViewDemo()
    {
        $model = new Demo;
        $post = Yii::$app->request->post();
        // process ajax delete
        if (Yii::$app->request->isAjax && isset($post['kvdelete'])) {
            echo Json::encode([
                'success' => true,
                'messages' => [
                    'kv-detail-info' => 'The book # 1000 was successfully deleted. ' .
                        Html::a('<i class="fas fa-hand-point-right"></i>  Click here',
                            ['/site/detail-view-demo'], ['class' => 'btn btn-sm btn-info']) . ' to proceed.'
                ]
            ]);
            return;
        }
        // return messages on update of record
        if ($model->load($post) && $model->save()) {
            Yii::$app->session->setFlash('kv-detail-success', 'Success Message');
            Yii::$app->session->setFlash('kv-detail-warning', 'Warning Message');
        }
        return $this->render('detail-view', ['model'=>$model]);
    }*/
    ?>
  </div>
</div>
