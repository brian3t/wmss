<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Sales Order';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="so-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sales Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'comp_id',
                'label' => 'Company',
                'value' => function($model){
                    return $model->comp->name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Comp', 'id' => 'grid--comp_id']
            ],
        'name',
//        'wh_id',
        'void',
        'custid',
        'ord_type',
        'cust_desc',
        'description',
        'pick_instructions',
        'ok_to_pick',
        'currency',
//        'erp_id',
//        'erp_order',
        'ship_complete',
        'hold',
        'hold_rsn',
        'ship_meth',
        'more_attr:ntext',
        'cust_po',
        'close_on_first_ship',
        'instructions',
        'erp_key',
        'bill_to',
        'bill_contact',
        'bill_email:email',
        'bill_addr1',
        'bill_addr2',
        'bill_addr3',
        'bill_addr4',
        'bill_city',
        'bill_state',
        'bill_country',
        'bill_postal',
        'bill_phone',
        'request_date',
        'promise_date',
        'close_date',
        'ship_date',
        'carrier_id',
        'bol_notes',
        'sched_arrival',
        'is_expedite',
        'pickrule_id',
        'shipmeth_id',
        'ship_to',
        'ship_contact',
        'ship_email:email',
        'ship_phone',
        'ship_addr1',
        'ship_addr2',
        'ship_addr3',
        'ship_addr4',
        'ship_city',
        'ship_state',
        'ship_postal',
        'ship_country',
        'cust_addr_id',
        'total_amt',
        'extra_field_1',
        'extra_field_2',
        'extra_field_3',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-so']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
