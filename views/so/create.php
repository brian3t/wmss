<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\So */

$this->title = 'Create So';
$this->params['breadcrumbs'][] = ['label' => 'So', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="so-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
