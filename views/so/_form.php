<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\So */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
 $model->ok_to_pick = 1;
 $model->currency = 'USD';
}

?>

<div class="so-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

<!--    --><?php //= $form->field($model, 'wh_id')->textInput(['placeholder' => 'Wh']) ?>

    <?= $form->field($model, 'void')->checkbox() ?>

    <?= $form->field($model, 'custid')->textInput(['maxlength' => true, 'placeholder' => 'Custid']) ?>

    <?= $form->field($model, 'ord_type')->textInput(['maxlength' => true, 'placeholder' => 'Ord Type']) ?>

    <?= $form->field($model, 'cust_desc')->textInput(['maxlength' => true, 'placeholder' => 'Cust Desc']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'pick_instructions')->textInput(['maxlength' => true, 'placeholder' => 'Pick Instructions']) ?>

    <?= $form->field($model, 'ok_to_pick')->checkbox() ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true, 'placeholder' => 'Currency']) ?>

<!--    --><?php //= $form->field($model, 'erp_id')->textInput(['placeholder' => 'Erp']) ?>

    <?= $form->field($model, 'erp_order')->textInput(['maxlength' => true, 'placeholder' => 'Erp Order']) ?>

    <?= $form->field($model, 'ship_complete')->checkbox() ?>

    <?= $form->field($model, 'hold')->checkbox() ?>

    <?= $form->field($model, 'hold_rsn')->textInput(['maxlength' => true, 'placeholder' => 'Hold Rsn']) ?>

    <?= $form->field($model, 'ship_meth')->textInput(['maxlength' => true, 'placeholder' => 'Ship Meth']) ?>

<!--    --><?php //= $form->field($model, 'more_attr')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cust_po')->textInput(['maxlength' => true, 'placeholder' => 'Cust Po']) ?>

    <?= $form->field($model, 'close_on_first_ship')->checkbox() ?>

    <?= $form->field($model, 'instructions')->textInput(['maxlength' => true, 'placeholder' => 'Instructions']) ?>

<!--    --><?php //= $form->field($model, 'erp_key')->textInput(['placeholder' => 'Erp Key']) ?>

    <?= $form->field($model, 'bill_to')->textInput(['maxlength' => true, 'placeholder' => 'Bill To']) ?>

    <?= $form->field($model, 'bill_contact')->textInput(['maxlength' => true, 'placeholder' => 'Bill Contact']) ?>

    <?= $form->field($model, 'bill_email')->textInput(['maxlength' => true, 'placeholder' => 'Bill Email']) ?>

    <?= $form->field($model, 'bill_addr1')->textInput(['maxlength' => true, 'placeholder' => 'Bill Addr1']) ?>

    <?= $form->field($model, 'bill_addr2')->textInput(['maxlength' => true, 'placeholder' => 'Bill Addr2']) ?>

    <?= $form->field($model, 'bill_addr3')->textInput(['maxlength' => true, 'placeholder' => 'Bill Addr3']) ?>

    <?= $form->field($model, 'bill_addr4')->textInput(['maxlength' => true, 'placeholder' => 'Bill Addr4']) ?>

    <?= $form->field($model, 'bill_city')->textInput(['maxlength' => true, 'placeholder' => 'Bill City']) ?>

    <?= $form->field($model, 'bill_state')->textInput(['maxlength' => true, 'placeholder' => 'Bill State']) ?>

    <?= $form->field($model, 'bill_country')->textInput(['maxlength' => true, 'placeholder' => 'Bill Country']) ?>

    <?= $form->field($model, 'bill_postal')->textInput(['maxlength' => true, 'placeholder' => 'Bill Postal']) ?>

    <?= $form->field($model, 'bill_phone')->textInput(['maxlength' => true, 'placeholder' => 'Bill Phone']) ?>

    <?= $form->field($model, 'request_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Request Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'promise_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Promise Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'close_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Close Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'ship_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Ship Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'carrier_id')->textInput(['placeholder' => 'Carrier']) ?>

    <?= $form->field($model, 'bol_notes')->textInput(['maxlength' => true, 'placeholder' => 'Bol Notes']) ?>

    <?= $form->field($model, 'sched_arrival')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Sched Arrival',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'is_expedite')->checkbox() ?>

    <?= $form->field($model, 'pickrule_id')->textInput(['placeholder' => 'Pickrule']) ?>

    <?= $form->field($model, 'shipmeth_id')->textInput(['placeholder' => 'Shipmeth']) ?>

    <?= $form->field($model, 'ship_to')->textInput(['maxlength' => true, 'placeholder' => 'Ship To']) ?>

    <?= $form->field($model, 'ship_contact')->textInput(['maxlength' => true, 'placeholder' => 'Ship Contact']) ?>

    <?= $form->field($model, 'ship_email')->textInput(['maxlength' => true, 'placeholder' => 'Ship Email']) ?>

    <?= $form->field($model, 'ship_phone')->textInput(['maxlength' => true, 'placeholder' => 'Ship Phone']) ?>

    <?= $form->field($model, 'ship_addr1')->textInput(['maxlength' => true, 'placeholder' => 'Ship Addr1']) ?>

    <?= $form->field($model, 'ship_addr2')->textInput(['maxlength' => true, 'placeholder' => 'Ship Addr2']) ?>

    <?= $form->field($model, 'ship_addr3')->textInput(['maxlength' => true, 'placeholder' => 'Ship Addr3']) ?>

    <?= $form->field($model, 'ship_addr4')->textInput(['maxlength' => true, 'placeholder' => 'Ship Addr4']) ?>

    <?= $form->field($model, 'ship_city')->textInput(['maxlength' => true, 'placeholder' => 'Ship City']) ?>

    <?= $form->field($model, 'ship_state')->textInput(['maxlength' => true, 'placeholder' => 'Ship State']) ?>

    <?= $form->field($model, 'ship_postal')->textInput(['maxlength' => true, 'placeholder' => 'Ship Postal']) ?>

    <?= $form->field($model, 'ship_country')->textInput(['maxlength' => true, 'placeholder' => 'Ship Country']) ?>

    <?= $form->field($model, 'cust_addr_id')->textInput(['placeholder' => 'Cust Addr']) ?>

    <?= $form->field($model, 'total_amt')->textInput(['maxlength' => true, 'placeholder' => 'Total Amt']) ?>

    <?= $form->field($model, 'extra_field_1')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 1']) ?>

<!--    --><?php //= $form->field($model, 'extra_field_2')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 2']) ?>
<!---->
<!--    --><?php //= $form->field($model, 'extra_field_3')->textInput(['maxlength' => true, 'placeholder' => 'Extra Field 3']) ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
