<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\So */

?>
<div class="so-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'comp.name',
            'label' => 'Comp',
        ],
        'name',
        'wh_id',
        'void',
        'custid',
        'ord_type',
        'cust_desc',
        'description',
        'pick_instructions',
        'ok_to_pick',
        'currency',
        'erp_id',
        'erp_order',
        'ship_complete',
        'hold',
        'hold_rsn',
        'ship_meth',
        'more_attr:ntext',
        'cust_po',
        'close_on_first_ship',
        'instructions',
        'erp_key',
        'bill_to',
        'bill_contact',
        'bill_email:email',
        'bill_addr1',
        'bill_addr2',
        'bill_addr3',
        'bill_addr4',
        'bill_city',
        'bill_state',
        'bill_country',
        'bill_postal',
        'bill_phone',
        'request_date',
        'promise_date',
        'close_date',
        'ship_date',
        'carrier_id',
        'bol_notes',
        'sched_arrival',
        'is_expedite',
        'pickrule_id',
        'shipmeth_id',
        'ship_to',
        'ship_contact',
        'ship_email:email',
        'ship_phone',
        'ship_addr1',
        'ship_addr2',
        'ship_addr3',
        'ship_addr4',
        'ship_city',
        'ship_state',
        'ship_postal',
        'ship_country',
        'cust_addr_id',
        'total_amt',
        'extra_field_1',
        'extra_field_2',
        'extra_field_3',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>