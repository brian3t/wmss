<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Inv */

$this->title = 'Create Inv';
$this->params['breadcrumbs'][] = ['label' => 'Inv', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
