<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pickdet */

$this->title = 'Create Pickdet';
$this->params['breadcrumbs'][] = ['label' => 'Pickdet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickdet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
