<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pickdet */
/* @var $form yii\widgets\ActiveForm */

$pickline_id = Yii::$app->request->get('pickline_id');
if (empty($pickline_id)){
    echo 'Need Pick Line ID';
    return false;
}
$pickline = \app\models\Pickline::findOne(['id' => $pickline_id]);
if (!($pickline instanceof \app\models\Pickline)) return 'Invalid Pick Line; please try again';
$a = 1;
?>

<div class="pickdet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'pickline_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Pickline::find()->orderBy('id')->all(), 'id'
          ,function ($p){return $p->desc;}),
        'options' => ['placeholder' => 'Choose Pickline'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'seq')->textInput(['placeholder' => 'Seq']) ?>

    <?= $form->field($model, 'lot_id')->textInput(['placeholder' => 'Lot']) ?>

    <?= $form->field($model, 'ser_id')->textInput(['placeholder' => 'Ser']) ?>

    <?= $form->field($model, 'cont_id')->textInput(['placeholder' => 'Container']) ?>

    <?= $form->field($model, 'inv_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Inv::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Inv'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'qty')->textInput(['maxlength' => true, 'placeholder' => 'Qty']) ?>

    <?= $form->field($model, 'prod_uom_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ProdUom::find()->where(['prod_id'=>$pickline->soline->prod_id])->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Prod uom'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'confirmed')->checkbox() ?>

    <?= $form->field($model, 'confirmed_at')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Confirmed At',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'confirmed_by')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true, 'placeholder' => 'Note']) ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
