<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pickdet */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pickdet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pickdet-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Pickdet'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'pickline.id',
            'label' => 'Pickline',
        ],
        'seq',
        'lot_id',
        'ser_id',
        'cont_id',
        [
            'attribute' => 'inv.id',
            'label' => 'Inv',
        ],
        'qty',
        [
            'attribute' => 'prodUom.id',
            'label' => 'Prod Uom',
        ],
        'confirmed',
        'confirmed_at',
        [
            'attribute' => 'confirmedBy.username',
            'label' => 'Confirmed By',
        ],
        'note',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
