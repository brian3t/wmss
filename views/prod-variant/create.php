<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProdVariant */

$this->title = 'Create Product Variant';
$this->params['breadcrumbs'][] = ['label' => 'Prod Variant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-variant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
