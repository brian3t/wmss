<div class="form-group" id="add-prod-detail">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\depdrop\DepDrop;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ProdDetail',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'prod_variant_id' => [
            'label' => 'Variant',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::class,
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Variant::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Variant'],
            ],
            'columnOptions' => ['width' => '200px']
        ]
      , 'variant_val_id' => [
        'type' => TabularForm::INPUT_RAW,
        'label' => 'Variant val',
        'options' => [
          ['id' => 'variant_id']
        ],
        'columnOptions' => ['width' => '200px'],
        'value' => function ($model, $key, $index, $widget) {
          $default = [];
          //parent input id is proddetail-$key-prod_variant_id
//                if(isset($model['tbl_basis_asset_sub_type_id']))
//                {
          $default = \yii\helpers\ArrayHelper::map(
            \app\models\VariantVal::find()
//                                ->where(['id' => $model['variant_id']])
              ->asArray()
              ->all(),
            'id', 'value_str');
//                }
          return DepDrop::widget([
            'name' => 'ProdDetail[' . $key . '][variant_val_id]',
//                    'options' => ['id'=>'proddetail-'.$key.'-prod_variant_id'],
            'data' => $default,
            'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
//                       'initialize' => isset($model['tbl_basis_asset_sub_type_id']) ? false : true,
              'initialize' => true,
              'depends' => ['proddetail-' . $key . '-prod_variant_id'],
              'placeholder' => 'Select ...',
              'url' => Url::to(['/variant/dep-val']),
            ]
          ]);
        },
        ],
        'note' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowProdDetail(' . $key . '); return false;', 'id' => 'prod-detail-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Product Detail', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProdDetail()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

