<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProdVariant */

$this->title = 'Update Prod Variant: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Prod Variant', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prod-variant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
