<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProdVariant */

$this->title = 'Product Variant';
$this->params['breadcrumbs'][] = ['label' => 'Prod Variant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-variant-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" >

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'prod.name',
            'label' => 'Product',
        ],
        'sku',
        'price',
        'lot_id',
        'ser_id',
        'note',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerInv->totalCount){
    $gridColumnInv = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'uom_id',
            'qty',
            [
                'attribute' => 'bin.name',
                'label' => 'Bin'
            ],
            'note',
            'noted_by',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerInv,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-inv']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Inv'),
        ],
        'columns' => $gridColumnInv
    ]);
}
?>
    </div>

    <div class="row">
<?php
if($providerProdDetail->totalCount){
    $gridColumnProdDetail = [
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'variantVal.id',
                'label' => 'Variant Value',
                          'format'=>'raw',
                          'value'=> function($model){
      /** @var $model \app\models\ProdDetail */
      return $model->variantVal->value_str;
                          }
            ],
            'note',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProdDetail,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-detail']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Product Details'),
        ],
        'columns' => $gridColumnProdDetail
    ]);
}
?>
    </div>
</div>
