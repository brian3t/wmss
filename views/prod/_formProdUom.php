<div class="form-group" id="add-prod-uom">
    <?php

    use kartik\grid\GridView;

    //    use kartik\builder\TabularForm;
    use soc\yii2_builder\TabularForm;
    use yii\data\ArrayDataProvider;
    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use app\models\ProdUom;

    /** @var ProdUom[] $row */
    $dataProvider = new ArrayDataProvider([
        'allModels' => $row,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo TabularForm::widget([
        'dataProvider' => $dataProvider,
        'formName' => 'ProdUom',
        'checkboxColumn' => false,
        'actionColumn' => false,
        'attributeDefaults' => [
            'type' => TabularForm::INPUT_TEXT,
        ],
        'attributes' => [
            "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => true, 'columnOptions' => ['contentOptions' => ['class' => 'hidden w0'], 'headerOptions' => ['class' => 'hidden w0']]],
            'uom_id' => [
                'label' => 'Uom',
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Uom::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Uom'],
                ],
                'columnOptions' => ['width' => '200px', 'headerOptions' => [
//                    'class' => 'kv-align-middle'
                ]]
            ],
            'comp_id' => [
                'label' => 'Company',
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Company'],
                ],
                'columnOptions' => ['width' => '200px']
            ],
            'cnv_to_stock' => ['type' => TabularForm::INPUT_TEXT, 'columnOptions' => ['width' => '24%'], 'label' => 'Convert To Stk'],
            'decimals_ovrd' => ['type' => TabularForm::INPUT_TEXT, 'label' => 'Decimals Override'],
            'weight' => ['type' => TabularForm::INPUT_TEXT],
            'volume' => ['type' => TabularForm::INPUT_TEXT],
            'length' => ['type' => TabularForm::INPUT_TEXT],
            'width' => ['type' => TabularForm::INPUT_TEXT],
            'height' => ['type' => TabularForm::INPUT_TEXT],
            'upc' => ['type' => TabularForm::INPUT_TEXT],
            'shippable_pack_series_id' => ['type' => TabularForm::INPUT_TEXT],
            'enabled' => ['type' => TabularForm::INPUT_CHECKBOX,
                'default_val' => true
            ],
            'lot_specific_cnv' => ['type' => TabularForm::INPUT_TEXT, 'columnOptions' => ['width' => '11%', 'headerOptions' => ['class' => 'w11']], 'label' => 'Lot Specific Convert'],
            'del' => [
                'type' => 'raw',
                'label' => '',
                'value' => function ($model, $key) {
                    return Html::a('<i class="fa-regular fa-trash-can"></i>', '#', ['title' => 'Delete', 'onClick' => 'delRowProdUom(' . $key . '); return false;', 'id' => 'prod-uom-del-btn']);
                },
            ],
        ],
        'gridSettings' => [
            'panel' => [
                'heading' => false,
                'type' => GridView::TYPE_DEFAULT,
                'before' => false,
                'footer' => false,
                'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Product Uom', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProdUom()']),
            ],
            'condensed' => true,
            'options' => ['style' => 'table-layout:fixed;'],
        ]
    ]);
    echo "    </div>\n\n";
    ?>

