<div class="form-group" id="add-prod-variant">
  <?php

  use kartik\grid\GridView;

  //use kartik\builder\TabularForm;
  use soc\yii2_builder\TabularForm;
  use yii\data\ArrayDataProvider;
  use yii\helpers\Html;
  use yii\widgets\Pjax;

  $dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
      'pageSize' => -1
    ]
  ]);
  echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ProdVariant',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
      'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
      "id" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['contentOptions' =>['class'=>'hidden w0'],'headerOptions' => ['class' => 'hidden w0']],
      ],
      'name' => ['type' => TabularForm::INPUT_TEXT],
      'sku' => ['type' => TabularForm::INPUT_TEXT],
      'price' => ['type' => TabularForm::INPUT_TEXT],
      'lot_id' => ['type' => TabularForm::INPUT_TEXT],
      'ser_id' => ['type' => TabularForm::INPUT_TEXT],
      'note' => ['type' => TabularForm::INPUT_TEXT],
      'del' => [
        'type' => 'raw',
        'label' => '',
        'value' => function ($model, $key) {
          return Html::a('<i class="fa-regular fa-trash-can"></i>', '#', ['title' => 'Delete', 'onClick' => 'delRowProdVariant(' . $key . '); return false;', 'id' => 'prod-variant-del-btn']);
        },
      ],
    ],
    'gridSettings' => [
      'panel' => [
        'heading' => false,
        'type' => GridView::TYPE_DEFAULT,
        'before' => false,
        'footer' => false,
        'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Prod Variant', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProdVariant()']),
      ],
        'condensed' => true,
            'options' => [ 'style' => 'table-layout:fixed;' ],
    ]
  ]);
  echo "    </div>\n\n";
  ?>

