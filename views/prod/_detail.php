<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Prod */

?>
<div class="prod-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        'name',
        'active',
        'ext_code',
        'ext_id',
        'ext_upd_at',
        'description',
        'long_desc',
        'rank',
        'is_inv',
        'decimals',
        'stock_uom_id',
        'pallet_uom_id',
        'case_uom_id',
        'mc_uom_id',
        'std_qty_per_pall',
        'qty_per_case',
        'wave_max_qty',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>