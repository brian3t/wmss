<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Prod */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Product'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" >
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'company.name',
            'label' => 'Company',
        ],
        'code',
        'name',
        'active',
        'description',
        'long_desc',
        'rank',
      [ 'attribute' => 'is_inv', 'format' => 'raw',
        'value' => \kartik\helpers\Html::checkbox('is inv', $model->is_inv)
      ],
        'decimals',
        'stock_uom_id',
        'pallet_uom_id',
        'case_uom_id',
        'mc_uom_id',
        'std_qty_per_pall',
        'qty_per_case',
        'wave_max_qty',
        'ext_code',
        'ext_id',
        'ext_upd_at',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn,
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerProdVariant->totalCount){
    $gridColumnProdVariant = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
                        'sku',
            'price',
            'lot_id',
            'ser_id',
            'note',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProdVariant,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-variant']],
        'panel' => [
//        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Product Variant'),
        ],
        'columns' => $gridColumnProdVariant
    ]);
}
?>
    </div>
     <div class="row">
<?php
if($providerProdUom->totalCount){
   $gridColumnProdUom = [
       ['class' => 'yii\grid\SerialColumn'],
           ['attribute' => 'id', 'visible' => false],
                       [
               'attribute' => 'uom.name',
               'label' => 'Uom'
           ],
           [
               'attribute' => 'company.name',
               'label' => 'Company'
           ],
           'cnv_to_stock',
           'decimals_ovrd',
           'weight',
           'volume',
           'length',
           'width',
           'height',
           'upc',
           'shippable_pack_series_id',
           'enabled',
           'lot_specific_cnv',
   ];
   echo Gridview::widget([
       'dataProvider' => $providerProdUom,
       'pjax' => true,
       'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod-uom']],
       'panel' => [
       'type' => GridView::TYPE_PRIMARY,
       'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Product Uom'),
       ],
       'columns' => $gridColumnProdUom
   ]);
}
?>
   </div>
</div>
