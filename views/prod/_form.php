<?php

use kartik\widgets\SwitchInput;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use soc\yii2helper\yii2_widget_activeform\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Prod */
/* @var $form ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'ProdUom',
        'relID' => 'prod-uom',
        'value' => \yii\helpers\Json::encode($model->prodUoms),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'ProdVariant',
        'relID' => 'prod-variant',
        'value' => \yii\helpers\Json::encode($model->prodVariants),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
if ($model->isNewRecord) {
 $model->active = true;
 $model->is_inv = true;
 $model->decimals = 4;
}

?>

<div class="prod-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'comp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Comp::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Comp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder' => 'Code']) ?>

<!--    --><?php //= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

<!--    --><?php //= $form->field($model, 'active')->checkbox() ?><!--
<!--   --><?php //= $form->field($model, 'descr')->textInput(['maxlength' => true, 'placeholder' => 'Description']) ?>

    <?= $form->field($model, 'long_desc')->textInput(['maxlength' => true, 'placeholder' => 'Long Desc']) ?>

<!--    --><?php //= $form->field($model, 'rank')->textInput(['maxlength' => true, 'placeholder' => 'Rank']) ?>
  <?php
  echo $form->field($model, 'is_inv')->widget(SwitchInput::classname(), [
         'value' => (!$model->is_inv) ? false : true,
    ])->label('Is Inventory Product');
  ?>
<!--    --><?php //= $form->field($model, 'is_inv')->checkboxList(['1' => 'Yes', '0' => 'No']) ?>
    <?= $form->field($model, 'decimals')->textInput(['placeholder' => 'Decimals']) ?>
    <?= $form->field($model, 'stock_uom_id')->textInput(['placeholder' => 'Stock Uom']) ?>

    <?= $form->field($model, 'pallet_uom_id')->textInput(['placeholder' => 'Pallet Uom']) ?>

    <?= $form->field($model, 'case_uom_id')->textInput(['placeholder' => 'Case Uom']) ?>

    <?= $form->field($model, 'mc_uom_id')->textInput(['placeholder' => 'Mc Uom']) ?>

    <?= $form->field($model, 'std_qty_per_pall')->textInput(['placeholder' => 'Std Qty Per Pall']) ?>

    <?= $form->field($model, 'qty_per_case')->textInput(['placeholder' => 'Qty Per Case']) ?>

    <?= $form->field($model, 'wave_max_qty')->textInput(['placeholder' => 'Wave Max Qty']) ?>

    <?= $form->field($model, 'ext_code')->textInput(['maxlength' => true, 'placeholder' => 'Ext Code']) ?>

    <?= $form->field($model, 'ext_id')->textInput(['placeholder' => 'Ext']) ?>

    <?= $form->field($model, 'ext_upd_at')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Ext Upd At',
                'autoclose' => true,
            ]
        ],
    ]); ?>


    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Product UOMs'),
            'content' => $this->render('_formProdUom', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->prodUoms),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Product Variants'),
            'content' => $this->render('_formProdVariant', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->prodVariants),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
