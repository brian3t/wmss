<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Comp */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Comp', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comp-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Comp'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3">

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'descr',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>

    <div class="row">
<?php
if($providerProd->totalCount){
    $gridColumnProd = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'active',
            'ext_code',
            'ext_id',
            'ext_upd_at',
            'description',
            'long_desc',
            'rank',
            'is_inv',
            'decimals',
            'stock_uom_id',
            'pallet_uom_id',
            'case_uom_id',
            'mc_uom_id',
            'std_qty_per_pall',
            'qty_per_case',
            'wave_max_qty',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProd,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-prod']],
        'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Product'),
        ],
        'columns' => $gridColumnProd
    ]);
}
?>
    </div>
</div>
