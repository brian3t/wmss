<div class="form-group" id="add-prod">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Prod',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'name' => ['type' => TabularForm::INPUT_TEXT],
        'active' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'ext_code' => ['type' => TabularForm::INPUT_TEXT],
        'ext_id' => ['type' => TabularForm::INPUT_TEXT],
        'ext_upd_at' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Ext Upd At',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'description' => ['type' => TabularForm::INPUT_TEXT],
        'long_desc' => ['type' => TabularForm::INPUT_TEXT],
        'rank' => ['type' => TabularForm::INPUT_TEXT],
        'is_inv' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'decimals' => ['type' => TabularForm::INPUT_TEXT],
        'stock_uom_id' => ['type' => TabularForm::INPUT_TEXT],
        'pallet_uom_id' => ['type' => TabularForm::INPUT_TEXT],
        'case_uom_id' => ['type' => TabularForm::INPUT_TEXT],
        'mc_uom_id' => ['type' => TabularForm::INPUT_TEXT],
        'std_qty_per_pall' => ['type' => TabularForm::INPUT_TEXT],
        'qty_per_case' => ['type' => TabularForm::INPUT_TEXT],
        'wave_max_qty' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowProd(' . $key . '); return false;', 'id' => 'prod-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Prod', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProd()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

