<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProdDetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Prod Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-detail-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Prod Detail'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" >

            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'prodVariant.name',
            'label' => 'Product Variant',
        ],
        [
            'attribute' => 'variantVal.id',
            'label' => 'Variant Val',
        ],
        'note',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
