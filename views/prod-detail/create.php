<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProdDetail */

$this->title = 'Create Prod Detail';
$this->params['breadcrumbs'][] = ['label' => 'Prod Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prod-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
