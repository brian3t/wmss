<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProdDetail */

$this->title = 'Update Prod Detail: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Prod Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prod-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
