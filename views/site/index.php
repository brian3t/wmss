<?php

/** @var yii\web\View $this */

$this->title = 'Socal WMS Web';
?>
<div class="site-index">
  <br>
  <div class="container px-4 text-center menus">
    <div class="row gx-5">
      <div class="col">
        <div class="card h-md-100">
          <div class="card-header pb-0">
            <h5>Common Information</h5>
          </div>
          <div class="card-body p-1 border bg-light">
            <a href="/comp">Companies</a><br>
            <a href="/bin">Bins</a><br>
            <a href="/prod">Products</a><br>
            <a href="/variant">Variants</a><br>
            <a href="/variant-val">Variant Values</a><br>
            <a href="/uom">UOM</a><br>
<!--            <a href="/#">Warehouses</a>-->
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-md-100">
          <div class="card-header pb-0">
            <h5>Inventory</h5>
          </div>
          <div class="card-body p-1 border bg-light">
            <a href="/prod-variant">Products Variants</a><br>
            <a href="/prod-uom">Products UOM</a><br>
            <a href="/inv">Inventory</a><br>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-md-100">
          <div class="card-header pb-0">
            <h5>Sales Orders</h5>
          </div>
          <div class="card-body p-1 border bg-light">
            <a href="/so">Sales Orders</a><br>
            <a href="/soline">Sales Order Lines</a><br>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-md-100">
          <div class="card-header pb-0">
            <h5>Picking</h5>
          </div>
          <div class="card-body p-1 border bg-light">
            <a href="/pick">Picks</a><br>
            <a href="/pickline">Pick Lines</a><br>
            <a href="/pickdet">Pick Details</a><br>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card h-md-100">
          <div class="card-header pb-0">
            <h5>Reports</h5>
          </div>
          <div class="card-body p-1 border bg-light">
            <a href="/prod">Product Label</a><br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
