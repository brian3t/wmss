<?php

namespace app\commands;

use soc\yiiuser\User\Query\UserQuery;
use Throwable;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\db\StaleObjectException;
use yii\helpers\Console;

class DeleteController extends Controller
{
    protected $userQuery;

    public function __construct($id, Module $module, UserQuery $userQuery, array $config = [])
    {
        $this->userQuery = $userQuery;
        parent::__construct($id, $module, $config);
    }

    /**
     * This command deletes a user.
     *
     * @param string $usernameOrEmail Email or username of the user to delete
     *
     *
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionIndex($usernameOrEmail)
    {
        if ($this->confirm(Yii::t('app', 'Are you sure? Deleted user can not be restored'))) {
            $user = $this->userQuery->whereUsernameOrEmail($usernameOrEmail)->one();
            if ($user === null) {
                $this->stdout(Yii::t('app', 'User is not found') . "\n", Console::FG_RED);
            } else {
                if ($user->delete()) {
                    $this->stdout(Yii::t('app', 'User has been deleted') . "\n", Console::FG_GREEN);
                } else {
                    $this->stdout(Yii::t('app', 'Error occurred while deleting user') . "\n", Console::FG_RED);
                }
            }
        }
    }
}
