<?php

namespace app\commands;

use soc\yiiuser\User\Query\UserQuery;
use soc\yiiuser\User\Service\UserConfirmationService;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\Console;

class ConfirmController extends Controller
{
    use ContainerAwareTrait;

    protected $userQuery;

    public function __construct($id, Module $module, UserQuery $userQuery, array $config = [])
    {
        $this->userQuery = $userQuery;

        parent::__construct($id, $module, $config);
    }

    /**
     * Confirms a a user by setting its field `confirmed_at` to current time.
     *
     * @param string $usernameOrEmail Username or email of the user
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex($usernameOrEmail)
    {
        $user = $this->userQuery->whereUsernameOrEmail($usernameOrEmail)->one();
        if ($user === null) {
            $this->stdout('User is not found' . "\n", Console::FG_RED);
        } elseif ($this->make(UserConfirmationService::class, [$user])->run()) {
            $this->stdout('User has been confirmed' . "\n", Console::FG_GREEN);
        } else {
            $this->stdout('Error occurred while confirming user' . "\n", Console::FG_RED);
        }
    }
}
