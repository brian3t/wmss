<?php

namespace app\commands;

use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Query\UserQuery;
use soc\yiiuser\User\Service\ResetPasswordService;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\Console;

class PasswordController extends Controller
{
    use ContainerAwareTrait;

    protected $userQuery;

    public function __construct($id, Module $module, UserQuery $userQuery, array $config = [])
    {
        $this->userQuery = $userQuery;
        parent::__construct($id, $module, $config);
    }

    /**
     * This command updates the user's password.
     *
     * @param string $usernameOrEmail Username or email of the user who's password needs to be updated
     * @param string $password        The new password
     *
     * @throws InvalidConfigException
     */
    public function actionIndex($usernameOrEmail, $password)
    {
        $user_query = $this->userQuery;
        $user_q_filter = $user_query->whereUsernameOrEmail($usernameOrEmail);
        /** @var User $user */
        $user = $user_q_filter->one();

        if ($user === null) {
            $this->stdout(Yii::t('app', 'User is not found') . "\n", Console::FG_RED);
        } else {
            if ($this->make(ResetPasswordService::class, [$password, $user])->run()) {
                $this->stdout(Yii::t('app', 'Password has been changed') . "\n", Console::FG_GREEN);
            } else {
                $this->stdout(Yii::t('app', 'Error occurred while changing password') . "\n", Console::FG_RED);
            }
        }
    }
}
