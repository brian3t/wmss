<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';
    public $css = [
        'css/tabulator.css',
//        'css/tabulator_modern.css',
        'css/tabulator_bootstrap5.css',
        'css/fontawesome.min.css',
        'css/regular.min.css',
        'css/solid.min.css',
        'css/site.css',
        'css/gfont.css',
        'css/OverlayScrollbars.min.css',
        'css/theme.css',
        'js/lib/sweetalert2.min.css',
        'css/wmss.css',
        'css/yii_sc.css',
        'css/custom.css',
    ];
    public $js = [
        'js/bootstrap-notify.min.js',
        'js/fontawesome.min.js',
        'js/luxon.min.js',
        'js/tabulator.js',
//        'js/tabulator.min.js',
        'js/yii_sc.js',
        'js/config.js',
        'js/OverlayScrollbars.min.js',
        'js/popper.min.js',
        'js/anchor.min.js',
        'js/is.min.js',
        'js/echarts.min.js',
        'js/fa.min.js',
        'js/lodash.min.js',
        'js/polyfill.min.js',
        'js/list.min.js',
        'js/theme.min.js',
        'js/helper.js',
        'js/lib/sweetalert2.min.js',
        'js/wmss.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset'
    ];
}
