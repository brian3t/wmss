<?php

namespace app\controllers;

use Yii;
use app\models\Pickline;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PicklineController implements the CRUD actions for Pickline model.
 */
class PicklineController extends Controller
{
  public function behaviors() {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Pickline models.
   * @return mixed
   */
  public function actionIndex() {
    $dataProvider = new ActiveDataProvider([
      'query' => Pickline::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Pickline model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $model = $this->findModel($id);
    $providerPickdet = new \yii\data\ArrayDataProvider([
      'allModels' => $model->pickdets,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerPickdet' => $providerPickdet,
    ]);
  }

  /**
   * Creates a new Pickline model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Pickline();
    if (! Yii::$app->request->isPost) return $this->render('create', [
      'model' => $model,
    ]);
    $yii_post = Yii::$app->request->post();
    if ($model->loadAll($yii_post)) {
      $save_res = $model->save();
      if ($save_res) {
        return $this->redirect(['view', 'id' => $model->id]);
      } else {
        return $this->render('create', [
          'model' => $model,
        ]);
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing Pickline model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public
  function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Pickline model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public
  function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  /**
   * Finds the Pickline model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Pickline the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected
  function findModel($id) {
    if (($model = Pickline::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for Pickdet
   * @return mixed
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   */
  public
  function actionAddPickdet() {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('Pickdet');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formPickdet', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
