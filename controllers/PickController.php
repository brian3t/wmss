<?php

namespace app\controllers;

use Yii;
use app\models\Pick;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PickController implements the CRUD actions for Pick model.
 */
class PickController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'actions' => ['create', 'update', 'delete'],
            'roles' => ['@']
          ],
          [
            'allow' => true,
            'actions' => ['index', 'view'],
            'roles' => []
          ],
          [
            'allow' => false
          ]
        ]
      ]

    ];
  }

  /**
   * Lists all Pick models.
   * @return mixed
   */
  public function actionIndex()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Pick::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Pick model.
   * @param integer $id
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionView(int $id): string
  {
    $model = $this->findModel($id);
    $providerPickline = new \yii\data\ArrayDataProvider([
      'allModels' => $model->picklines,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerPickline' => $providerPickline,
    ]);
  }

  /**
   * Creates a new Pick model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Pick();

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Pick model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Pick model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  /**
   * Finds the Pick model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Pick the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Pick::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
