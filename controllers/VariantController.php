<?php

namespace app\controllers;

use Yii;
use app\models\Variant;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VariantController implements the CRUD actions for Variant model.
 */
class VariantController extends Controller
{
  public function behaviors() {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
   * Lists all Variant models.
   * @return mixed
   */
  public function actionIndex() {
    $dataProvider = new ActiveDataProvider([
      'query' => Variant::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Variant model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $model = $this->findModel($id);
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Variant model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Variant();

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Variant model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Variant model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  /**
   * Finds the Variant model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Variant the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Variant::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Generate dependant values for Kartik Depend Dropdown
   * @return array|string[]
   */
  public function actionDepVal() {
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
      $parents = $_POST['depdrop_parents'];
      if ($parents != null) {
        $variant_id = $parents[0];
        $out = self::getValList($variant_id);
        // the getSubCatList function will query the database based on the
        // cat_id and return an array like below:
        // [
        //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
        //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
        // ]
        return ['output' => $out, 'selected' => '1'];
      }
    }
    return ['output' => '', 'selected' => ''];
  }

  public function getValList($variant_id) {
    $all_variant_vals = \app\models\VariantVal::find()->select('id,value_str as name')->where(['variant_id' => $variant_id])->asArray()->all();

    return $all_variant_vals;
  }

}
