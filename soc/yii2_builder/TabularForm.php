<?php

/**
 * @package   yii2-builder
 * @author    ngxtri@gmail.com
 * @version   0.0.1
 */

namespace soc\yii2_builder;

use Closure;
use Exception;
use kartik\builder\TabularForm as BaseForm;
use kartik\builder\TabularFormAsset;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;
use kartik\helpers\Html;
use yii\i18n\Formatter;

/**
 * b3t Add default_val
 *
 * @author ngxtri@gmail.com
 */
class TabularForm extends BaseForm
{
  /**
   * Generates a cell value.
   *
   * @param string $attribute the model attribute.
   * @param mixed $settings the configuration for the attribute.
   *
   * @return Closure the parsed cell value.
   * @throws Exception
   */
  protected function getCellValue($attribute, $settings) {
    $formatter = ArrayHelper::getValue($this->gridSettings, 'formatter', Yii::$app->formatter);
    return function ($model, $key, $index, $widget) use ($attribute, $settings, $formatter) {
      $staticInput = '';
      foreach ($settings as $k => $v) {
        if ($v instanceof Closure) {
          $settings[$k] = call_user_func($v, $model, $key, $index, $widget);
        }
      }
      $type = ArrayHelper::getValue($settings, 'type', self::INPUT_RAW);
      if ($type === self::INPUT_STATIC || $this->staticOnly || $type === self::INPUT_HIDDEN_STATIC) {
        $staticInput = $this->getStaticInput($type, $model, $index, $settings, $attribute, $formatter);
        if ($type !== self::INPUT_HIDDEN_STATIC) {
          return $staticInput;
        }
      }
      $i = empty($key) ? $index : (is_array($key) ? implode($this->compositeKeySeparator, $key) : $key);
      $options = ArrayHelper::getValue($settings, 'options', []);
      if ($model instanceof Model) {
        if ($type === self::INPUT_HIDDEN || $type === self::INPUT_HIDDEN_STATIC) {
          return ($type === self::INPUT_HIDDEN ? '' : $staticInput) .
            Html::activeHiddenInput($model, "[{$i}]{$attribute}", $options);
        }
        return $this->renderActiveInput($this->form, $model, "[{$i}]{$attribute}", $settings);
      } else {
        $models = $this->dataProvider->getModels();
        //b3t
        if (! (static::isEmpty($models, $index, $attribute)))
          $value = $models[$index][$attribute];
        else
          $value = ArrayHelper::getValue($settings, 'default_val');
        $settings['value'] = $value;
//        $settings['value'] = static::isEmpty($models, $index, $attribute) ? null : $models[$index][$attribute];
        if ($type === self::INPUT_HIDDEN || $type === self::INPUT_HIDDEN_STATIC) {
          return ($type === self::INPUT_HIDDEN ? '' : $staticInput) .
            Html::hiddenInput("{$this->formName}[{$i}][{$attribute}]", $settings['value'], $options);
        }
        return $this->renderInput("{$this->formName}[{$i}][{$attribute}]", $settings);
      }
    };
  }

}
