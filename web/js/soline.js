jQuery(document).ready(() => {

//initialize table
  function all_cap(cell, formatterParams, onRendered){
    cell.getElement().classList.add('all_cap')
    return cell.getValue()
  }

  const table = new Tabulator(".tabulator", {
    // data: table_data,           //load row data from array
    ajaxURL: "//api.wmss/v1/soline",           //load row data from array
    // layout: "fitColumns",      //fit columns to width of table
    layout: "fitDataStretch",
    responsiveLayout: "hide",  //hide columns that dont fit on the table
    addRowPos: "top",          //when adding a new row, add it to the top of the table
    history: true,             //allow undo and redo actions on the table
    pagination: "local",       //paginate the data
    paginationSize: 7,         //allow 7 rows per page of data
    paginationCounter: "rows", //display count of paginated rows in footer
    movableColumns: true,      //allow column order to be changed
    initialSort: [             //set the initial sort order of the data
      {column: "so", dir: "asc"},
    ],
    columnDefaults: {
      tooltip: true,         //show tool tips on cells
    },
    autoColumns: true,
    autoColumnsDefinitions: function (definitions){
      //definitions - array of column definition objects

      definitions.forEach((column) => {
        column.headerFilter = true; // add header filter to every column
        if (column.title === 'name') {
          column.title = 'Order'
          column.formatter = all_cap
        }
        if (column.title === 'wh_id') column.title = 'Warehouse'
        // if (column.title === 'prod_var') column.title = 'Product Variant'
        if (column.title === 'custid') column.title = 'Customer'
        column.title = column.title.replaceAll('_', ' ')
      });

      return definitions;
    },
  })
  table.on("dataProcessed", function (data){
    console.log(`table dataProcessed`)
    table.deleteColumn('created_at')
    table.addColumn({
      title: "", field: "__", headerFilter: false, headerSort: false, headerMenu: [
        {
          label: "Select Columns",
          action: function (e, column){
            $('#col_sel_mod_btn').trigger('click')
          }
        },

      ]
    }, true, "__")
    table.addColumn({
      title: "__", field: "__", headerFilter: false, headerSort: false
      , cssClass: 'no_text_overflow'
      , cellClick: (e, cell) => {
        console.log(`e: `, e, ` cell: `, cell)
        const cell_data = cell.getData()
        if (!cell_data) return false
        const cell_id = cell_data.id
        const ajax_url = cell.getTable().getAjaxUrl()
        const db_table_name = ajax_url.split('/').pop()
        // window.open(`/` + db_table_name + `/update?id=${cell_id}`, '_blank');
        window.open(`/` + db_table_name + `/view?id=${cell_id}`, '_blank');
      },
      formatter: (value, data, cell, row, options) => {
        return "<i class='fa-regular fa-pen-to-square'></i>"
        // return "<i class='bi bi-pencil-square'></i>"
      }
    }, true, "__");
  });
  window.tab = table


  /**
   * Col Selector
   * BN 12/14/22
   */


  $('.col_selector :input').on('change', (e) => {
    e.preventDefault()
    e.stopPropagation()
    const $e = $(e.target)
    const is_selected = $e.is(':checked')
    const col = $e.data('col')
    if (! is_selected) table.hideColumn(col)
    else table.showColumn(col)
  })

  /**
   * Col Selector END
   */

})
