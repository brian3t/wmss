jQuery(function (){
 //define data array
  const table_data = [
    {id: 1, name: "Oli Bob", progress: 12, gender: "male", rating: 1, col: "red", dob: "19/02/1984", car: 1},
    {id: 2, name: "Mary May", progress: 1, gender: "female", rating: 2, col: "blue", dob: "14/05/1982", car: true},
    {id: 3, name: "Christine Lobowski", progress: 42, gender: "female", rating: 0, col: "green", dob: "22/05/1982", car: "true"},
    {id: 4, name: "Brendon Philips", progress: 100, gender: "male", rating: 1, col: "orange", dob: "01/08/1980"},
    {id: 5, name: "Margret Marmajuke", progress: 16, gender: "female", rating: 5, col: "yellow", dob: "31/01/1999"},
    {id: 6, name: "Frank Harbours", progress: 38, gender: "male", rating: 4, col: "red", dob: "12/05/1966", car: 1},
  ]

//initialize table
  /*
  const table = new Tabulator("#example-table", {
      data: table_data, //assign data to table
      autoColumns: true, //create columns from data field names
  })
  */
  function all_cap(cell, formatterParams, onRendered){
    cell.getElement().classList.add('all_cap')
    return cell.getValue()
  }

  const table = new Tabulator(".tabulator", {
    // data: table_data,           //load row data from array
    ajaxURL: "//api.wmss/v1/so",           //load row data from array
    // layout: "fitColumns",      //fit columns to width of table
    layout: "fitDataStretch",
    responsiveLayout: "hide",  //hide columns that dont fit on the table
    addRowPos: "top",          //when adding a new row, add it to the top of the table
    history: true,             //allow undo and redo actions on the table
    pagination: "local",       //paginate the data
    paginationSize: 7,         //allow 7 rows per page of data
    paginationCounter: "rows", //display count of paginated rows in footer
    movableColumns: true,      //allow column order to be changed
    initialSort: [             //set the initial sort order of the data
      {column: "name", dir: "asc"},
    ],
    columnDefaults: {
      tooltip: true,         //show tool tips on cells
    },
    autoColumns: true,
    autoColumnsDefinitions: function (definitions){
      //definitions - array of column definition objects

      definitions.forEach((column) => {
        column.headerFilter = true; // add header filter to every column
        if (column.title === 'name') {
          column.title = 'Order'
          column.formatter = all_cap
        }
        if (column.title === 'wh_id') column.title = 'Warehouse'
        if (column.title === 'custid') column.title = 'Customer'
        column.title = column.title.replaceAll('_', ' ')
      });

      return definitions;
    },
  })
  table.on("dataProcessed", function (data){
    console.log(`table dataProcessed`)
    table.deleteColumn('created_at')
    table.addColumn({
      title: "", field: "__", headerFilter: false, sort: false, headerSort: false, headerMenu: [
        {
          label: "Select Columns",
          action: function (e, column){
            $('#col_sel_mod_btn').trigger('click')
          }
        },

      ]
    }, true, "__");
  });
  window.tab = table


  /**
   * Col Selector
   * BN 12/14/22
   */


  $('.col_selector :input').on('change', (e) => {
    e.preventDefault()
    e.stopPropagation()
    const $e = $(e.target)
    const is_selected = $e.is(':checked')
    const col = $e.data('col')
    if (! is_selected) table.hideColumn(col)
    else table.showColumn(col)
  })

  /**
   * Col Selector END
   */

})
