jQuery(function (){
  $(':input[name$="company_id]"]').val(1)
  $(':input[name$="company_id]"]').trigger('change')
  $(':input[name$="[comp_id]"]').val(1)
  $(':input[name$="[comp_id]"]').trigger('change')

  /**
   * During form submit, if select has type=integer, convert it to integer instead of string
   * BN 1/29/23
   */
  /* During form submit, if select has type=integer, convert it to integer instead of string END   */
  $('form button[type="submit"]').on('click', async (e) => {
    console.log(`select int`)
    e.preventDefault()
    e.stopPropagation()

    const form = $(e.target).closest('form')
    if (! (form instanceof jQuery) || form.length !== 1) console.error(`Cannot find form`)
    const $form = $(form)
    let elements_type_int = []
    $form.find('select[sctype="integer"]').each((i, sel_int) => {
      elements_type_int.push(sel_int.name)
    })
    const form_data = $form.serializeArray()
    let form_data_obj = flat_array_to_assoc(form_data)
    /*elements_type_int.forEach(element_type_int => {
      form_data_obj[element_type_int].value = parseInt(form_data_obj[element_type_int].value)
    })*/
    // postResult = await $.post(postUrl, $.param(postData)).promise();

    const post_res = await $.post(form[0].action, form_data_obj).promise()
    /*const post_res = await $.ajax({
      url: form[0].action,
      type: 'POST',
      data: JSON.stringify(form_data_obj),
      contentType: 'application/json'
    }).promise()*/
    $('body').html(post_res)
    let a = 1


  })
})

//theme begin
let isFluid = JSON.parse(localStorage.getItem('isFluid'));
/*
if (isFluid) {
  var container = document.querySelector('[data-layout]');
  container.classList.remove('container');
  container.classList.add('container-fluid');
}
*/
var navbarStyle = localStorage.getItem("navbarStyle");
if (navbarStyle && navbarStyle !== 'transparent') {
  document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
}
var navbarPosition = localStorage.getItem('navbarPosition');
var navbarVertical = document.querySelector('.navbar-vertical');
var navbarTopVertical = document.querySelector('.navbar-top');
var navbarTop = document.querySelector('.navbar-top');
var navbarTopCombo = document.querySelector('[data-navbar-top="combo"]');
if (navbarPosition === 'top') {
  navbarTop.removeAttribute('style');
  // navbarTopVertical.remove(navbarTopVertical);
  if (navbarVertical) navbarVertical.remove(navbarVertical);
  if (navbarTopCombo) navbarTopCombo.remove(navbarTopCombo);
} else if (navbarPosition === 'combo') {
  navbarVertical.removeAttribute('style');
  navbarTopCombo.removeAttribute('style');
  navbarTop.remove(navbarTop);
  navbarTopVertical.remove(navbarTopVertical);
} else {
  navbarVertical.removeAttribute('style');
  navbarTopVertical.removeAttribute('style');
  navbarTop.remove(navbarTop);
  navbarTopCombo.remove(navbarTopCombo);
}

/**
 * Socal Toggle Default Attribute
 * Toggle current attribute, so that API tells DB to use default value
 * 2/3/23 Remove input from form submit. Future, instead of removal, can set attr value to -88.87654321 or 'use_def', to be more specific
 */
function sc_toggle_default(event){
  let $use_def_chkbox, use_def_checked
  console.log(`sc_toggle_default called`)
  if (!this || !this.event || !this.event.target) {
    //try grabbing from html event
    if (!event || !event.target) {
      console.error(`No element??`)
      return false
    }
    $use_def_chkbox = $(event.target)
    use_def_checked = event.target.checked
  } else {//if has this.event, use it
    $use_def_chkbox = $(this.event.target)
    use_def_checked = this.event.target.checked
  }
  const $active_field = $use_def_chkbox.closest('div[class^="field-"], div[class*=" field-"]')
  if (!($active_field instanceof jQuery) || $active_field.length !== 1){
    console.error(`Cannot find active field. Check html structure`)
    return false
  }
  const $af_input = $active_field.find(':input.form-control')
  $af_input.prop('disabled', use_def_checked) //if use_default checked; disable the :input

  //exclude [name] property of input; thus excluding it from form submit
  if (use_def_checked){
    $af_input.removeAttr('name')
  } else {
    const stored_input_name = $use_def_chkbox.data('input_name')
    $af_input.attr('name', stored_input_name)
  }
  let a = 1
}

//theme end
