$('.dropdown').on('mouseover', (e) => {
  console.log(`dropdown mouseover triggered`)
  const $e = $(e.currentTarget)
  const $dropdown_menu = $e.find('.dropdown-menu')
  if (false === $dropdown_menu.hasClass('show')) {
    $dropdown_menu.addClass('show').attr('aria-expanded', 'true');
    // $(this).find('.dropdown-menu').addClass('show');
  }
}).mouseout(function (){
  if ($('.navbar-toggler').is(':hidden')) {
    $(this).removeClass('show').attr('aria-expanded', 'false');
    $(this).find('.dropdown-menu').removeClass('show');
  }
});

function swal_sc(title, msg, duration = 3000) {
  return Swal.fire({
    title: title,
    html: msg,
    timer: duration,
    didOpen: () => {
      Swal.showLoading()
    },
    willClose: () => {
    }
  })
}
