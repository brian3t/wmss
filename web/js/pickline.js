// import {apis} from './apis.js'

$(function () {
  $('.pick_this_line').on('click', e => {
    cr_modal({title: 'Create Pick Detail', table: 'pickdet', id: null}, e)
  })

  /**
   * Create a modal
   * @param framework default Bootstrap5 for now
   * @param modal_conf Object. Must have:
   * table
   * id
   * fields []
   *  each field is [column_name,col_desc,column_type,is_nullable,min_length,max_length,foreign_name_col,is_editable,force_lkup_dis,cb_func]
   * Optional:
   * title
   * @param e Html event
   */
  async function cr_modal(modal_conf, e, framework = 'bs') {
    const $el = $(e.target)
    const pickline_id = $el.data('pickline_id')
    const modal_id = Math.floor(Math.random() * 65535);
    let meth = modal_conf.meth || 'create_update' //create read update delete
    const table = modal_conf.table
    if (!table) return false
    let foreign_values = {pickline_id}
    const id = modal_id.id
    if (id > 0) meth = 'update'
    let fields = [ //future; get fields from global fields config
      //column_name,col_desc,column_type,is_nullable,min_length,max_length,foreign_name_col,is_editable,force_lkup_dis,cb_func
      ['pickline_id', 'Pick Line', 'int', false, null, null, 'description', false, true, null],
      // ['seq', 'Sequence', 'int', false, 0, null, null,false],
      ['lot_id', 'Lot', 'int', true, 0, null, null, false, false, null],
      ['inv_id', 'Inventory', 'int', false, 0, null, 'desc', false, false, 'inv_id_cb'],
      ['qty', 'Qty', 'decimal(12,6)', false, 0, null, null, true, false, null],
      ['prod_uom_id', 'UOM', 'int', false, 0, null, null, false, false, null],
      ['note', 'Note', 'varchar', false, 0, 2000, null, true, false, null]
    ]
    let fields_html = await form_groups_gen(table, fields, foreign_values)
    let modal_tmpl = $('<div></div>')
    modal_tmpl.html = `
<div id="${modal_id}" class="modal fade" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalLabel">${modal_conf.title}</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form data-table="${modal_conf.table}">
`
      + fields_html +
      `</form></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="save_pick_det(event)">Save changes</button>
            </div>
          </div>
        </div></div>`
    // let modal_inst = new bootstrap.Modal(modal_tmpl[0], {})
    $('body').append(modal_tmpl.html)
    $(`#${modal_id}`).modal('show')
    await bind_elements()
  }
})

/**
 * Generate a bs5 form_group, e.g.
 <div class="form-check">
 <input type="checkbox" class="form-check-input" id="exampleCheck1" data-foreign_id=8>
 <label class="form-check-label" for="exampleCheck1">Check me out</label>
 </div>
 @param table String Table Name
 @param field Array [column_name,col_desc,column_type,is_nullable,min_length,max_length,foreign_name_col,is_editable,force_lkup_dis]
 @param foreign_values Object foreign_col => foreign_val, e.g. pickline_id => 8
 */
async function form_group_gen(table, field = [], foreign_values) {
  let [column_name, col_desc, column_type, is_nullable, min_length, max_length, foreign_name_col, is_editable, force_lkup_dis, cb_func] = field
  if (col_desc < ' ') col_desc = _.capitalize(column_name.replaceAll('_', ' '))
  let input_element_name = 'input', input_class = ''
  switch (column_type) {
    case 'checkbox':
      input_class = 'form-check-input'
      break
    case 'varchar':
    case 'nvarchar':
      input_element_name = 'textarea'
      break
    default:
      break
  }
  if (column_type.startsWith('decimal')) column_type = 'number'
  let foreign_entity, foreign_table = null
  const is_foreign = column_name.endsWith('_id')
  let toggle_lkup = 'hidden'
  if (is_foreign) {
    foreign_table = column_name.replace('_id', '').replace('_', '-')
    foreign_entity = {}
    if (foreign_values[column_name]) {
      foreign_entity = await (apis.g1(foreign_table, {id: foreign_values[column_name]}))
    }
    if (force_lkup_dis) {
      toggle_lkup = 'hidden'
    } else {
      toggle_lkup = '' //by default, foreign col allows lookup; unless flag force_lkup_dis is ON
    }
  }
  const input_id = Math.floor(Math.random() * 65535)
  return `
  <div class="mb-3">
    <label class="form-label" for="${column_name}${input_id}">${col_desc}</label>
    <span class="row">
      <span class="col-11">
        <${input_element_name} type="${column_type}" class="form-control ${input_class} " name="${column_name}" 
        id="${column_name}${input_id}" value="${is_foreign ? foreign_entity[foreign_name_col] || '' : ''}"
        ${(is_foreign && foreign_values[column_name]) ? `data-foreign_id="${foreign_values[column_name]}"` : ''}
        ${is_editable ? '' : 'readonly'} ${cb_func ? `data-cb="${cb_func}"` : ''}
        >
        </${input_element_name}>
      </span>
      <span class="col-1 lkup ${toggle_lkup}" data-foreign="${column_name}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
      </svg></span>
  </span>
  </div>`
}

async function form_groups_gen(table, fields = [], foreign_values) {
  let html_res = ''
  for (const field of fields) {
    html_res += await form_group_gen(table, field, foreign_values)
  }
  return html_res
}

/**
 * Bind all elements; such as lkup
 * @return {Promise<void>}
 */
async function bind_elements() {
  $('span.lkup').on('click', e => {
    console.log(`lkup clicked`)
    const $e = $($(e.target).closest('span'))
    const foreign = $e.data('foreign')
    const foreign_table = foreign.slice(0, -3).replace('_', '-')
    lkup(foreign_table, $e)
  })
}

/**
 * Calls a lookup modal
 * @param table The foreign table name
 * @param name_col The foreign table column that stores the name (description). Default to `name`.
 *                    If `name` doesn't have data; fallback to `desc`
 * @param {el} target_span The target el to write back selected data to
 * @return null Show modal. And when row is selected; populate into target_span's val and data-foreign_id
 */
async function lkup(table, target_span, name_col = 'name') {
  const modal_id = Math.floor(Math.random() * 65535);
  const table_data = await apis.g(table)

//initialize table

  // let modal_inst = new bootstrap.Modal(modal_tmpl[0], {})
  const tabulator = new Tabulator("#lkup_tabulator", {
    data: table_data, //assign data to table
    autoColumns: true, //create columns from data field names
    autoColumnsDefinitions: function (definitions) {
      //definitions - array of column definition objects
      definitions.forEach((column) => {
        if (column.field.endsWith('_id')) column.visible = false
        if (column.field == 'desc') column.title = 'Description'
        column.headerFilter = true // add header filter to every column
      })
      return definitions
    },

    /*columns: [
      {title: "UOM ID", field: "uom_id", visible: false},
      {title: "Description", field: "desc"}
    ],*/
  })
  tabulator.on('rowClick', (e, row) => {
    const target_input = target_span.closest('.row').find('input')
    target_input.data('foreign_id', row?._row?.data?.id)
    target_input.attr('data-foreign_id', row?._row?.data?.id)
    target_input.data('lkup_row_all_data', row?._row?.data)
    target_input.attr('data-lkup_row_all_data', JSON.stringify(row?._row?.data))
    target_input.val(row?._row?.data[name_col] || row?._row?.data['desc'])
    const cb_of_el = target_input.data('cb')
    const cb_of_el_func = window[cb_of_el]
    if (typeof cb_of_el_func == 'function') cb_of_el_func(target_input)
    $(`#lkup_modal`).modal('hide')
  })
  $(`#lkup_modal`).modal('show')
}

/**
 * Callback for inv_id field when its value changes
 * Populate qty and uom and bin id
 * @param el
 * @return {Promise<void>}
 */
async function inv_id_cb(el) {
  console.log(`inv_id_cb called`)
  const lkup_inv_full_data = el.data('lkup_row_all_data')
  if (!(lkup_inv_full_data instanceof Object))
    return console.error(`No lkup_inv_full_data. Double check lookup`)
  const qty = lkup_inv_full_data.qty
  $('.modal input[name="qty"]').val(qty)
  $('.modal input[name="prod_uom_id"]').val(lkup_inv_full_data.uom)
  // $('.modal input[name="prod_uom_id"]').data('foreign_id', lkup_inv_full_data.uom_id)
  $('.modal input[name="prod_uom_id"]').attr('data-foreign_id', lkup_inv_full_data.prod_uom_id)
}

/**
 * Save pick det
 * Submit the closest form
 * @return string
 */
async function save_pick_det(event) {
  const form = $(event.target).closest('.modal-content').find('form')
  if (form.length != 1) return 'No form found'
  //todob submit form; parsing data-foreign_id as foreign_id => value
  let [form_serialized_stat, form_serialized] = form_serialize(form)
  const table = form.data('table')
  if (table < ' ') return console.error(`Malformed data`)
  //here submit form to /v1/table
  let [pd_stat, pd_created] = await apis.c(table, form_serialized)
  if (!pd_stat) {
    swal_sc('Error', `Error saving ${table}`)
    return false
  }
  swal_sc('Saved successfully', '', 1200)
  $('.modal').modal('hide')
}

/**
 * Socal js api
 * Serialize a form to prepare for submit
 * input name=
 * @param form
 * @return Array [boolean, Object]
 * If success: [true, SerializedObject]
 * If failed: [false, Error]
 */
function form_serialize(form) {
  if (!(form instanceof jQuery)) return [false, 'Invalid form']
  let res = {}, $inp
  form.find(':input').each((i, inp) => {
    if (inp.name.endsWith('_id')) {
      $inp = $(inp)
      const foreign_data = $inp.data('foreign_id')
      if (foreign_data) {
        res[inp.name] = foreign_data
        return
      }
    }
    if (inp.type == 'number') return res[inp.name] = parseFloat(inp.value)
    res[inp.name] = inp.value
  })
  return [true, res]
}

//asdf
setTimeout(() => {
  // $('a.pick_this_line').trigger('click')
}, 2000)

//asdf
