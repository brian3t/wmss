create table if not exists auth_rule
(
    name       varchar(64) not null
        primary key,
    data       bytea,
    created_at integer,
    updated_at integer
);

alter table auth_rule
    owner to wmss;

create table if not exists auth_item
(
    name        varchar(64) not null
        primary key,
    type        smallint    not null,
    description text,
    rule_name   varchar(64),
    data        bytea,
    created_at  integer,
    updated_at  integer
);

alter table auth_item
    owner to wmss;

create table if not exists auth_assignment
(
    item_name  varchar(64) not null
        constraint auth_assignment_ibfk_1
            references auth_item
            on update cascade on delete cascade,
    user_id    varchar(64) not null,
    created_at integer,
    primary key (item_name, user_id)
);

alter table auth_assignment
    owner to wmss;

create table if not exists auth_item_child
(
    parent varchar(64) not null
        constraint auth_item_child_ibfk_1
            references auth_item
            on update cascade on delete cascade,
    child  varchar(64) not null
        constraint auth_item_child_ibfk_2
            references auth_item
            on update cascade on delete cascade
);

alter table auth_item_child
    owner to wmss;

create index if not exists child
    on auth_item_child (child);

create table if not exists inv_jour
(
    id         bigserial
        primary key,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    created_by integer,
    inv_old    varchar(2048)                       not null,
    inv_new    varchar(2048)                       not null,
    note       varchar(255)
);

alter table inv_jour
    owner to wmss;

create table if not exists migration
(
    version    varchar(180) not null
        primary key,
    apply_time timestamp
);

alter table migration
    owner to wmss;

create table if not exists "user"
(
    id                   serial
        primary key,
    username             varchar(255)       not null
        constraint idx_user_username
            unique,
    email                varchar(255)       not null
        constraint idx_user_email
            unique,
    password_hash        varchar(60)        not null,
    auth_key             varchar(32)        not null,
    unconfirmed_email    varchar(255),
    registration_ip      varchar(45),
    flags                integer  default 0 not null,
    confirmed_at         integer,
    blocked_at           integer,
    updated_at           integer            not null,
    created_at           integer            not null,
    last_login_at        integer,
    last_login_ip        varchar(45),
    auth_tf_key          varchar(16),
    auth_tf_enabled      smallint default 0,
    auth_tf_type         varchar(20),
    auth_tf_mobile_phone varchar(20),
    password_changed_at  integer,
    gdpr_consent         smallint default 0,
    gdpr_consent_date    integer,
    gdpr_deleted         smallint default 0
);

alter table "user"
    owner to wmss;

create table if not exists variant_val
(
    id          serial
        primary key,
    variant_id  integer                             not null,
    value_int   integer,
    value_str   varchar(512),
    value_dtm   timestamp,
    value_float numeric(12, 8),
    created_at  timestamp default CURRENT_TIMESTAMP not null,
    updated_at  timestamp,
    created_by  integer,
    updated_by  integer,
    note        varchar(512)
);

alter table variant_val
    owner to wmss;

create index if not exists variant_id
    on variant_val (variant_id);

create table if not exists profile
(
    user_id        serial
        primary key
        constraint fk_profile_user
            references "user"
            on delete cascade,
    name           varchar(255),
    public_email   varchar(255),
    gravatar_email varchar(255),
    gravatar_id    varchar(32),
    location       varchar(255),
    website        varchar(255),
    timezone       varchar(40),
    bio            text
);

alter table profile
    owner to wmss;

create table if not exists social_account
(
    id         serial
        primary key,
    user_id    integer
        constraint fk_social_account_user
            references "user"
            on delete cascade,
    provider   varchar(255) not null,
    client_id  varchar(255) not null,
    code       varchar(32)
        constraint idx_social_account_code
            unique,
    email      varchar(255),
    username   varchar(255),
    data       text,
    created_at integer,
    constraint idx_social_account_provider_client_id
        unique (provider, client_id)
);

alter table social_account
    owner to wmss;

create table if not exists token
(
    user_id    integer
        constraint fk_token_user
            references "user"
            on delete cascade,
    code       varchar(32) not null,
    type       smallint    not null,
    created_at integer     not null,
    constraint idx_token_user_id_code_type
        unique (user_id, code, type)
);

alter table token
    owner to wmss;

create table if not exists variant
(
    id         serial
        primary key,
    name       varchar(32)                                 not null,
    type       variant_type default 'String'::variant_type not null,
    created_at timestamp    default CURRENT_TIMESTAMP      not null,
    updated_at timestamp,
    created_by integer,
    updated_by integer,
    long_desc  varchar(512)
);

comment on column variant.name is 'Variant Name';

alter table variant
    owner to wmss;

create table if not exists comp
(
    id         smallserial
        primary key,
    name       varchar(12) not null,
    descr      varchar(128),
    updated_at timestamp,
    updated_by integer
);

comment on table comp is 'Company';

comment on column comp.descr is 'Description';

alter table comp
    owner to wmss;

create table if not exists bin
(
    id                    bigserial
        primary key,
    comp_id               integer                                   not null
        constraint bin_comp_id_fk
            references comp
            on delete cascade,
    name                  varchar(64)                               not null,
    zone_id               integer,
    description           varchar(255),
    active                boolean         default true              not null,
    excl_fr_avail         boolean         default false             not null,
    excl_fr_fcast         boolean         default false             not null,
    allow_neg_inv         boolean         default false,
    seq                   integer         default 1                 not null
        constraint bin_seq_check
            check (seq >= 0),
    erp_int               integer,
    excl_rec              boolean         default false,
    excl_ship             boolean         default false,
    erp_loc               varchar(64),
    pallet_spaces         numeric(12, 10) default 0.0000000000
        constraint bin_pallet_spaces_check
            check (pallet_spaces >= (0)::numeric),
    primary_zone_id       integer,
    excl_from_recon       boolean         default false,
    reverse_recon         boolean         default false,
    reverse_recon_rsnkey  integer,
    excl_cont             boolean         default false,
    allow_ship_stage      boolean         default true,
    perm_move_in          varchar(64),
    perm_move_out         varchar(64),
    disallow_hold_inv     boolean         default false,
    height_level          numeric(12, 10) default 0.0000000000,
    row                   smallint        default 0,
    col                   smallint        default 0,
    is_item_restricted    boolean         default false,
    loc_positions         numeric(12, 10) default 0.0000000000,
    is_shipping_lane      boolean         default false,
    is_consolidation_area boolean         default false,
    is_pack_area          boolean         default false,
    auto_unload_cont      boolean         default false,
    excl_count            boolean         default false,
    require_cont          boolean         default false,
    is_single_item_only   boolean         default false,
    is_pick_area          boolean         default true,
    pickgroup_id          integer,
    length                numeric(12, 10) default 0.0000000000,
    width                 numeric(12, 10) default 0.0000000000,
    height                numeric(12, 10) default 0.0000000000,
    flagged_for_count     boolean         default true,
    pickroute_id          integer,
    rank                  varchar(3),
    max_wt                numeric(12, 10),
    flagged_for_putaway   boolean         default true,
    is_putaway_hold       boolean         default false,
    putaway_hold_comment  varchar(255),
    is_pick_hold          boolean         default false,
    pick_hold_comment     varchar(255),
    latitude              numeric(12, 10),
    longitude             numeric(12, 10),
    last_count_date       timestamp,
    floor                 smallint        default 0                 not null
        constraint bin_floor_check
            check (floor >= 0),
    created_at            timestamp       default CURRENT_TIMESTAMP not null,
    updated_at            timestamp,
    created_by            integer,
    updated_by            integer
);

comment on constraint bin_comp_id_fk on bin is 'com';

comment on column bin.excl_fr_avail is 'Exclude From Available';

comment on column bin.excl_fr_fcast is 'Exclude From Forecast';

comment on column bin.excl_rec is 'Exclude From Receipt';

comment on column bin.excl_ship is 'Exclude From Ship';

comment on column bin.excl_from_recon is 'Exclude From Recon';

comment on column bin.reverse_recon is 'Reverse Reconcile From ERP';

comment on column bin.reverse_recon_rsnkey is 'Reverse Reconcile ERP Reason';

comment on column bin.excl_cont is 'Exclude Container';

comment on column bin.allow_ship_stage is 'Allow Shipment Staging';

comment on column bin.excl_count is 'Exclude From Count';

alter table bin
    owner to wmss;

create table if not exists inv
(
    id              bigserial
        primary key,
    prod_variant_id integer                             not null,
    uom_id          integer                             not null,
    qty             integer   default 0                 not null,
    bin_id          integer                             not null
        constraint inv_bin
            references bin
            on delete cascade,
    created_at      timestamp default CURRENT_TIMESTAMP not null,
    updated_at      timestamp,
    created_by      integer,
    updated_by      integer,
    note            varchar(255),
    noted_by        integer
);

alter table inv
    owner to wmss;

create table if not exists pick
(
    id             bigserial
        primary key,
    created_at     timestamp default CURRENT_TIMESTAMP not null,
    updated_at     timestamp,
    created_by     integer                             not null,
    updated_by     integer,
    comp_id        smallint                            not null
        constraint pick_comp_id_fk
            references comp
            on delete cascade,
    whse_id        smallint,
    name           integer,
    tran_cat       varchar(6),
    sub_cat        varchar(6),
    ok_to_post     boolean   default true              not null,
    is_complete    boolean   default false             not null,
    so_id          integer,
    is_released    boolean   default false             not null,
    rpt_group_id   smallint,
    pick_rule_id   smallint,
    bol_notes      text,
    notes          text,
    is_bol_printed boolean   default false             not null,
    shipmeth_id    integer,
    is_expedite    boolean   default false             not null
);

alter table pick
    owner to wmss;

create table if not exists prod
(
    id               serial
        primary key,
    comp_id          integer
        constraint prod_comp_id_fk
            references comp
            on delete cascade,
    code             varchar(32)                         not null,
    name             varchar(128)                        not null,
    created_at       timestamp default CURRENT_TIMESTAMP not null,
    updated_at       timestamp,
    created_by       integer,
    updated_by       integer,
    active           boolean   default true              not null,
    ext_code         varchar(80),
    ext_id           integer,
    ext_upd_at       timestamp,
    descr            varchar(255),
    long_desc        varchar(510),
    rank             varchar(2),
    is_inv           boolean   default true              not null,
    decimals         smallint  default 4                 not null,
    stock_uom_id     integer,
    pallet_uom_id    integer,
    case_uom_id      integer,
    mc_uom_id        integer,
    std_qty_per_pall integer,
    qty_per_case     smallint,
    wave_max_qty     smallint
);

comment on column prod.code is 'Product NO';

comment on column prod.name is 'Product ID';

comment on column prod.ext_code is 'Code in External System';

comment on column prod.ext_id is 'ID in External System';

comment on column prod.ext_upd_at is 'Updated At in External System';

comment on column prod.descr is 'Description';

comment on column prod.long_desc is 'Long Description';

comment on column prod.is_inv is 'Is Inventory Tracked';

comment on column prod.stock_uom_id is 'Stock UOM';

comment on column prod.pallet_uom_id is 'Pallet UOM';

comment on column prod.case_uom_id is 'Case UOM';

comment on column prod.mc_uom_id is 'Master Case UOM';

comment on column prod.std_qty_per_pall is 'Standard Qty Per Pallet';

comment on column prod.qty_per_case is 'Qty per case';

comment on column prod.wave_max_qty is 'Wave Max Qty';

alter table prod
    owner to wmss;

create index if not exists prod_code
    on prod (code);

create table if not exists prod_variant
(
    id         serial
        primary key,
    name       varchar(255)                        not null,
    prod_id    integer                             not null
        constraint prod_val_prod
            references prod
            on delete cascade,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp,
    created_by integer,
    updated_by integer,
    sku        varchar(64)                         not null,
    price      numeric(12, 10),
    lot_id     integer,
    ser_id     integer,
    note       varchar(512)
);

comment on column prod_variant.name is 'Friendly Name';

alter table prod_variant
    owner to wmss;

create table if not exists prod_detail
(
    id              serial
        primary key,
    prod_variant_id integer                             not null
        constraint prod_detail_prod_variant
            references prod_variant
            on update cascade on delete cascade,
    variant_val_id  integer                             not null
        constraint prod_detail_variant_val
            references variant_val
            on update cascade on delete cascade,
    created_at      timestamp default CURRENT_TIMESTAMP not null,
    updated_at      timestamp,
    created_by      integer,
    updated_by      integer,
    note            varchar(255)
);

comment on table prod_detail is 'A particular prod variant';

comment on column prod_detail.prod_variant_id is 'Product Variant ID';

comment on column prod_detail.variant_val_id is 'Variant Value ID';

alter table prod_detail
    owner to wmss;

create table if not exists so
(
    id                  bigserial
        primary key,
    created_at          timestamp    default CURRENT_TIMESTAMP not null,
    updated_at          timestamp,
    updated_by          integer,
    comp_id             integer                                not null
        constraint so_comp_id_fk
            references comp,
    name                varchar(64)                            not null,
    wh_id               integer,
    void                boolean      default false             not null,
    custid              varchar(64),
    ord_type            varchar(16),
    cust_desc           varchar(512),
    description         varchar(512),
    pick_instructions   varchar(2048),
    ok_to_pick          boolean      default true              not null,
    currency            varchar(32),
    erp_id              integer,
    erp_order           varchar(64),
    ship_complete       boolean      default false             not null,
    hold                boolean      default false             not null,
    hold_rsn            varchar(64),
    ship_meth           varchar(64),
    more_attr           text         default '{}'::text,
    cust_po             varchar(64),
    close_on_first_ship boolean      default false             not null,
    instructions        varchar(2047),
    erp_key             integer,
    bill_to             varchar(127),
    bill_contact        varchar(127),
    bill_email          varchar(127),
    bill_addr1          varchar(255),
    bill_addr2          varchar(255),
    bill_addr3          varchar(255) default NULL::character varying,
    bill_addr4          varchar(255) default NULL::character varying,
    bill_city           varchar(127),
    bill_state          varchar(63),
    bill_country        char(2),
    bill_postal         varchar(63),
    bill_phone          varchar(63),
    request_date        date,
    promise_date        date,
    close_date          date,
    ship_date           date,
    carrier_id          integer,
    bol_notes           varchar(2047),
    sched_arrival       timestamp,
    is_expedite         boolean      default false             not null,
    pickrule_id         integer,
    shipmeth_id         integer,
    ship_to             varchar(127),
    ship_contact        varchar(127),
    ship_email          varchar(127),
    ship_phone          varchar(63),
    ship_addr1          varchar(255),
    ship_addr2          varchar(255),
    ship_addr3          varchar(255) default NULL::character varying,
    ship_addr4          varchar(255) default NULL::character varying,
    ship_city           varchar(127),
    ship_state          varchar(63),
    ship_postal         varchar(63),
    ship_country        varchar(63),
    cust_addr_id        integer,
    total_amt           numeric(12, 4),
    extra_field_1       varchar(31)  default NULL::character varying,
    extra_field_2       varchar(31)  default NULL::character varying,
    extra_field_3       varchar(31)  default NULL::character varying,
    constraint comp_so_pk
        unique (comp_id, name)
);

comment on column so.name is 'Sales Order Number';

comment on column so.cust_addr_id is 'Customer Address ID';

alter table so
    owner to wmss;

create table if not exists uom
(
    id          serial
        primary key,
    name        varchar(64)                            not null,
    descr       varchar(255) default NULL::character varying,
    comp_id     integer                                not null
        constraint uom_comp_id_fk
            references comp
            on delete cascade,
    uom_type    smallint,
    is_base     boolean      default false             not null,
    created_at  timestamp    default CURRENT_TIMESTAMP not null,
    updated_at  timestamp,
    cnv_to_base numeric(12, 10),
    erp_key     integer,
    erp_uom     varchar(64)  default NULL::character varying
);

alter table uom
    owner to wmss;

create table if not exists soline
(
    id                   bigserial
        primary key,
    so                   varchar(64)                               not null,
    lineid               varchar(128),
    company              varchar(12)                               not null,
    created_at           timestamp       default CURRENT_TIMESTAMP not null,
    updated_at           timestamp,
    customer             varchar(64),
    cust_id              integer,
    seq                  smallint,
    void                 boolean         default false             not null,
    erp_id               integer,
    erp_soline           varchar(128),
    wh                   varchar(31)     default NULL::character varying,
    prod_var             varchar(128)                              not null,
    description          varchar(512),
    non_inv              boolean         default false             not null,
    qty_ord              integer                                   not null,
    qty_shipped          integer         default 0                 not null,
    erp_qty_open_to_ship integer,
    ok_to_pick           boolean         default true              not null,
    pickrule             varchar(64),
    uom                  varchar(64)                               not null,
    cnv_to_stk           numeric(12, 10) default 1.0000000000      not null,
    erp_so_id            integer,
    erp_so               varchar(128),
    req_ship_date        timestamp,
    promise_date         timestamp,
    shipmeth             varchar(64),
    ship_priority        smallint        default 255,
    ship_to              varchar(255),
    ship_contact         varchar(255),
    ship_email           varchar(255),
    addr1                varchar(511),
    addr2                varchar(511),
    addr3                varchar(511),
    addr4                varchar(511),
    city                 varchar(255),
    state                varchar(128),
    postal               varchar(64),
    country_code         char(2)         default 'us'::bpchar      not null,
    phone1               varchar(64),
    phone2               varchar(64),
    cancel_date          timestamp,
    additional_info      varchar(2047),
    extra_field_1        varchar(31),
    extra_field_2        varchar(31),
    extra_field_3        varchar(31),
    uom_id               integer
        constraint soline_uom_id_fk
            references uom,
    so_id                integer,
    addr_hash            varchar(2500),
    wt_per               numeric(12, 10),
    comment              varchar(2047),
    decimals             smallint        default 4                 not null,
    unit_price           numeric(12, 10),
    ext_price            numeric(12, 10),
    is_backorder         boolean         default false             not null,
    comp_id              integer
        constraint soline_comp_id_fk
            references comp,
    prod_var_id          integer
        constraint soline_prod_variant_id_fk
            references prod_variant
            on delete cascade,
    wh_id                integer,
    created_by           integer
        constraint soline_user_id_fk
            references "user"
            on delete set null,
    sync_stat            smallint        default 0,
    sync_msg             varchar(511),
    last_synced          timestamp
);

comment on column soline.sync_msg is 'Sync Message';

comment on column soline.last_synced is 'Last Synced At';

alter table soline
    owner to wmss;

create table if not exists prod_uom
(
    id                       serial
        primary key,
    prod_id                  integer                                   not null
        constraint prod_uom_prod_fk
            references prod,
    uom_id                   integer                                   not null
        constraint prod_uom_uom_fk
            references uom,
    comp_id                  integer                                   not null
        constraint prod_uom_comp_id_fk
            references comp
            on delete cascade,
    cnv_to_stock             numeric(12, 10) default 1.0000000000,
    decimals_ovrd            smallint        default 4,
    weight                   numeric(12, 10) default 0.0000000000,
    volume                   numeric(12, 10) default 0.0000000000,
    length                   numeric(12, 10) default 0.0000000000,
    width                    numeric(12, 10) default 0.0000000000,
    height                   numeric(12, 10) default 0.0000000000,
    upc                      varchar(64),
    shippable_pack_series_id integer,
    enabled                  boolean         default true              not null,
    lot_specific_cnv         smallint        default 1,
    created_at               timestamp       default CURRENT_TIMESTAMP not null,
    updated_at               timestamp
);

alter table prod_uom
    owner to wmss;

create table if not exists pickdet
(
    id           bigserial
        primary key,
    pickline_id  integer                                  not null,
    seq          smallint       default 0                 not null,
    lot_id       integer,
    ser_id       integer,
    cont_id      integer,
    inv_id       integer                                  not null
        constraint pickdet_inv_id_fk
            references inv,
    qty          numeric(12, 6) default 0.000000          not null,
    prod_uom_id  integer                                  not null
        constraint pickdet_prod_uom_id_fk
            references prod_uom,
    created_at   timestamp      default CURRENT_TIMESTAMP not null,
    updated_at   timestamp,
    created_by   integer
        constraint pickdet_user_id_fk
            references "user"
            on delete set null,
    updated_by   integer
        constraint pickdet_user_id_fk2
            references "user"
            on delete set null,
    confirmed    boolean        default false             not null,
    confirmed_at timestamp,
    confirmed_by integer
        constraint pickdet_user_id_fk3
            references "user"
            on delete set null,
    note         varchar(2000)
);

alter table pickdet
    owner to wmss;

create table if not exists pickline
(
    id                bigserial
        primary key,
    pick_id           integer                                  not null
        constraint pickline_pick_id_fk
            references pick
            on update cascade on delete cascade,
    created_at        timestamp      default CURRENT_TIMESTAMP not null,
    updated_at        timestamp,
    created_by        integer
        constraint pickline_user_id_fk
            references "user"
            on delete set null,
    updated_by        integer
        constraint pickline_user_id_fk2
            references "user"
            on delete set null,
    seq               smallint,
    assigned_to       integer
        constraint pickline_user_id_fk3
            references "user"
            on delete set null,
    prod_var_id       integer                                  not null
        constraint pickline_prod_variant_id_fk
            references prod_variant
            on delete cascade,
    soline_id         integer
        constraint pickline_soline_id_fk
            references soline
            on delete cascade,
    description       varchar(255)                             not null,
    wh_id             integer,
    bin_id            integer
        constraint pickline_bin_id_fk
            references bin
            on delete set null,
    zone_id           integer,
    qty_to_pick       numeric(12, 4) default 0.0000            not null,
    orig_qty_to_pick  numeric(12, 4) default 0.0000            not null,
    prod_uom_id       integer                                  not null
        constraint pickline_prod_uom_id_fk
            references prod_uom,
    min_qty_to_pick   numeric(12, 4) default 0.0000,
    max_qty_to_pick   numeric(12, 4),
    decimals          smallint       default '4'::smallint     not null,
    cust_id           integer,
    cust_addr_id      integer,
    completed_at      timestamp,
    ship_addr_id      integer,
    pick_completed_by integer
        constraint pickline_user_id_fk4
            references "user"
            on delete set null,
    note              varchar(1023)
);

alter table pickline
    owner to wmss;

create index if not exists pickid
    on pickline (pick_id);

create or replace view vpickdet
            (id, pickline_id, soline_id, sales_order, seq, qty, qty_stock, bin, product_variant, uom, created_at,
             updated_at, created_by, updated_by, confirmed, confirmed_at, confirmed_by, note, lot_id, ser_id, cont_id,
             inv_id, prod_uom_id)
as
SELECT pd.id,
       pd.pickline_id,
       sol.id                 AS soline_id,
       so.name                AS sales_order,
       pd.seq,
       pd.qty,
       pd.qty * u.cnv_to_base AS qty_stock,
       b.name                 AS bin,
       pv.name                AS product_variant,
       u.name                 AS uom,
       pd.created_at,
       pd.updated_at,
       pd.created_by,
       pd.updated_by,
       pd.confirmed,
       pd.confirmed_at,
       pd.confirmed_by,
       pd.note,
       pd.lot_id,
       pd.ser_id,
       pd.cont_id,
       pd.inv_id,
       pd.prod_uom_id
FROM pickdet pd
         JOIN pickline pl ON pl.id = pd.pickline_id
         LEFT JOIN soline sol ON pl.soline_id = sol.id
         LEFT JOIN so ON sol.so_id = so.id
         JOIN inv ON inv.id = pd.inv_id
         JOIN bin b ON inv.bin_id = b.id
         JOIN prod_uom pu ON pd.prod_uom_id = pu.id
         JOIN prod_variant pv ON pl.prod_var_id = pv.id
         JOIN uom u ON pu.uom_id = u.id;

comment on column vpickdet.sales_order is 'Sales Order Number';

comment on column vpickdet.product_variant is 'Friendly Name';

alter table vpickdet
    owner to wmss;

create or replace view vpickline_pickdet
            (id, pick_id, created_at, updated_at, created_by, updated_by, seq, assigned_to, prod_var_id, soline_id,
             product, uom, description, wh_id, bin_id, zone_id, qty_to_pick, qty_to_pick_stock, sum_qty_picked_stock,
             orig_qty_to_pick, min_qty_to_pick, max_qty_to_pick, decimals, cust_id, cust_addr_id, completed_at,
             ship_addr_id, pick_completed_by, note, prod_uom_id)
as
SELECT pl.id,
       pl.pick_id,
       pl.created_at,
       pl.updated_at,
       pl.created_by,
       pl.updated_by,
       pl.seq,
       pl.assigned_to,
       pl.prod_var_id,
       pl.soline_id,
       p.name                                               AS product,
       u.name                                               AS uom,
       pl.description,
       pl.wh_id,
       pl.bin_id,
       pl.zone_id,
       pl.qty_to_pick,
       pl.qty_to_pick * u.cnv_to_base                       AS qty_to_pick_stock,
       COALESCE(pd_per_pl.sum_qty_picked_stock, 0::numeric) AS sum_qty_picked_stock,
       pl.orig_qty_to_pick,
       pl.min_qty_to_pick,
       pl.max_qty_to_pick,
       pl.decimals,
       pl.cust_id,
       pl.cust_addr_id,
       pl.completed_at,
       pl.ship_addr_id,
       pl.pick_completed_by,
       pl.note,
       pl.prod_uom_id
FROM pickline pl
         JOIN prod_uom pu ON pl.prod_uom_id = pu.id
         JOIN prod p ON pu.prod_id = p.id
         JOIN uom u ON pu.uom_id = u.id
         JOIN (SELECT vpickdet.pickline_id,
                      sum(vpickdet.qty_stock) AS sum_qty_picked_stock
               FROM vpickdet
               GROUP BY vpickdet.pickline_id) pd_per_pl ON pd_per_pl.pickline_id = pl.id;

alter table vpickline_pickdet
    owner to wmss;

create or replace view vprod_uom
            (id, enabled, prod, prod_name, uom, name, cnv_to_stock, decimals_ovrd, weight, volume, length, width,
             height, upc, shippable_pack_series_id, lot_specific_cnv, created_at, updated_at, prod_id, uom_id, comp_id)
as
SELECT pu.id,
       pu.enabled,
       p.code AS prod,
       p.name AS prod_name,
       u.name AS uom,
       u.name,
       pu.cnv_to_stock,
       pu.decimals_ovrd,
       pu.weight,
       pu.volume,
       pu.length,
       pu.width,
       pu.height,
       pu.upc,
       pu.shippable_pack_series_id,
       pu.lot_specific_cnv,
       pu.created_at,
       pu.updated_at,
       pu.prod_id,
       pu.uom_id,
       pu.comp_id
FROM prod_uom pu
         JOIN prod p ON p.id = pu.prod_id
         JOIN uom u ON u.id = pu.uom_id;

comment on column vprod_uom.prod is 'Product Code';

alter table vprod_uom
    owner to wmss;

create or replace view vprod_variant
            (id, name, prod, prod_name, full_name, created_at, updated_at, created_by_user, updated_by_user, sku, price,
             note, prod_id, created_by, updated_by, lot_id, ser_id)
as
SELECT pv.id,
       pv.name,
       p.code                       AS prod,
       p.name                       AS prod_name,
       concat(p.name, ' ', pv.name) AS full_name,
       pv.created_at,
       pv.updated_at,
       u_cr.username                AS created_by_user,
       u_upd.username               AS updated_by_user,
       pv.sku,
       pv.price,
       pv.note,
       pv.prod_id,
       pv.created_by,
       pv.updated_by,
       pv.lot_id,
       pv.ser_id
FROM prod_variant pv
         JOIN prod p ON p.id = pv.prod_id
         LEFT JOIN "user" u_cr ON pv.created_by = u_cr.id
         LEFT JOIN "user" u_upd ON pv.updated_by = u_upd.id;

comment on column vprod_variant.name is 'Friendly Name';

comment on column vprod_variant.prod is 'Product Code';

alter table vprod_variant
    owner to wmss;

create or replace view vinv
            (id, prod, prod_name, prod_variant, qty, descr, uom, created_at, updated_at, created_by, updated_by, note,
             noted_by, prod_variant_id, prod_id, uom_id, prod_uom_id, bin_id)
as
SELECT inv.id,
       p.code                                                        AS prod,
       p.name                                                        AS prod_name,
       pv.name                                                       AS prod_variant,
       inv.qty,
       concat(inv.qty, ' ', uom.name, ' ', pv.name, ' @ ', bin.name) AS descr,
       uom.name                                                      AS uom,
       inv.created_at,
       inv.updated_at,
       inv.created_by,
       inv.updated_by,
       inv.note,
       inv.noted_by,
       inv.prod_variant_id,
       p.id                                                          AS prod_id,
       inv.uom_id,
       pu.id                                                         AS prod_uom_id,
       inv.bin_id
FROM inv
         JOIN prod_variant pv ON pv.id = inv.prod_variant_id
         JOIN bin ON bin.id = inv.bin_id
         JOIN prod p ON pv.prod_id = p.id
         JOIN uom ON uom.id = inv.uom_id
         JOIN prod_uom pu ON uom.id = pu.uom_id AND p.id = pu.prod_id;

alter table vinv
    owner to wmss;

create or replace view vpick
            (id, created_at, updated_at, created_by, updated_by, comp, name, tran_cat, sub_cat, ok_to_post, is_complete,
             so_id, is_released, rpt_group_id, pick_rule_id, bol_notes, notes, is_bol_printed, shipmeth_id, is_expedite,
             created_by_user_id, updated_by_user_id, comp_id, whse_id)
as
SELECT p.id,
       p.created_at,
       p.updated_at,
       u.username   AS created_by,
       uu.username  AS updated_by,
       c.name       AS comp,
       p.name,
       p.tran_cat,
       p.sub_cat,
       p.ok_to_post,
       p.is_complete,
       p.so_id,
       p.is_released,
       p.rpt_group_id,
       p.pick_rule_id,
       p.bol_notes,
       p.notes,
       p.is_bol_printed,
       p.shipmeth_id,
       p.is_expedite,
       p.created_by AS created_by_user_id,
       p.updated_by AS updated_by_user_id,
       p.comp_id,
       p.whse_id
FROM pick p
         JOIN comp c ON c.id = p.comp_id
         LEFT JOIN "user" u ON u.id = p.created_by
         LEFT JOIN "user" uu ON uu.id = p.updated_by;

alter table vpick
    owner to wmss;

create or replace view vpickline
            (id, pick_id, pick_name, created_at, updated_at, created_by, updated_by, prod, prod_name, prod_variant, uom,
             seq, assigned_to, soline_id, description, wh_id, bin_id, zone_id, qty_to_pick, orig_qty_to_pick,
             min_qty_to_pick, max_qty_to_pick, decimals, cust_id, cust_addr_id, completed_at, ship_addr_id,
             pick_completed_by, note, created_by_user_id, updated_by_user_id, prod_id, prod_var_id, prod_uom_id, uom_id)
as
SELECT pl.id,
       pl.pick_id,
       p.name        AS pick_name,
       pl.created_at,
       pl.updated_at,
       u.username    AS created_by,
       uu.username   AS updated_by,
       prod.code     AS prod,
       prod.name     AS prod_name,
       pv.name       AS prod_variant,
       uom.name      AS uom,
       pl.seq,
       pl.assigned_to,
       pl.soline_id,
       pl.description,
       pl.wh_id,
       pl.bin_id,
       pl.zone_id,
       pl.qty_to_pick,
       pl.orig_qty_to_pick,
       pl.min_qty_to_pick,
       pl.max_qty_to_pick,
       pl.decimals,
       pl.cust_id,
       pl.cust_addr_id,
       pl.completed_at,
       pl.ship_addr_id,
       pl.pick_completed_by,
       pl.note,
       pl.created_by AS created_by_user_id,
       pl.updated_by AS updated_by_user_id,
       prod.id       AS prod_id,
       pl.prod_var_id,
       pl.prod_uom_id,
       uom.id        AS uom_id
FROM pickline pl
         JOIN pick p ON p.id = pl.pick_id
         JOIN prod_variant pv ON pv.id = pl.prod_var_id
         JOIN prod ON prod.id = pv.prod_id
         JOIN prod_uom pu ON pl.prod_uom_id = pu.id
         JOIN uom uom ON pu.uom_id = uom.id
         LEFT JOIN "user" u ON u.id = pl.created_by
         LEFT JOIN "user" uu ON uu.id = pl.updated_by;

alter table vpickline
    owner to wmss;

