--
-- PostgreSQL database dump
--

-- Dumped from database version 14.16 (Ubuntu 14.16-1.pgdg24.04+1)
-- Dumped by pg_dump version 17.3 (Ubuntu 17.3-1.pgdg24.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET transaction_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: variant_type; Type: TYPE; Schema: public; Owner: wmss
--

CREATE TYPE public.variant_type AS ENUM (
    'Number',
    'String',
    'Date',
    'Date Time',
    'Numeric'
);


ALTER TYPE public.variant_type OWNER TO wmss;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_assignment; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.auth_assignment (
    item_name character varying(64) NOT NULL,
    user_id character varying(64) NOT NULL,
    created_at integer
);


ALTER TABLE public.auth_assignment OWNER TO wmss;

--
-- Name: auth_item; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.auth_item (
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    description text,
    rule_name character varying(64),
    data bytea,
    created_at integer,
    updated_at integer
);


ALTER TABLE public.auth_item OWNER TO wmss;

--
-- Name: auth_item_child; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.auth_item_child (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


ALTER TABLE public.auth_item_child OWNER TO wmss;

--
-- Name: auth_rule; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.auth_rule (
    name character varying(64) NOT NULL,
    data bytea,
    created_at integer,
    updated_at integer
);


ALTER TABLE public.auth_rule OWNER TO wmss;

--
-- Name: bin; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.bin (
    id bigint NOT NULL,
    comp_id integer NOT NULL,
    name character varying(64) NOT NULL,
    zone_id integer,
    description character varying(255),
    active boolean DEFAULT true NOT NULL,
    excl_fr_avail boolean DEFAULT false NOT NULL,
    excl_fr_fcast boolean DEFAULT false NOT NULL,
    allow_neg_inv boolean DEFAULT false,
    seq integer DEFAULT 1 NOT NULL,
    erp_int integer,
    excl_rec boolean DEFAULT false,
    excl_ship boolean DEFAULT false,
    erp_loc character varying(64),
    pallet_spaces numeric(12,10) DEFAULT 0.0000000000,
    primary_zone_id integer,
    excl_from_recon boolean DEFAULT false,
    reverse_recon boolean DEFAULT false,
    reverse_recon_rsnkey integer,
    excl_cont boolean DEFAULT false,
    allow_ship_stage boolean DEFAULT true,
    perm_move_in character varying(64),
    perm_move_out character varying(64),
    disallow_hold_inv boolean DEFAULT false,
    height_level numeric(12,10) DEFAULT 0.0000000000,
    "row" smallint DEFAULT 0,
    col smallint DEFAULT 0,
    is_item_restricted boolean DEFAULT false,
    loc_positions numeric(12,10) DEFAULT 0.0000000000,
    is_shipping_lane boolean DEFAULT false,
    is_consolidation_area boolean DEFAULT false,
    is_pack_area boolean DEFAULT false,
    auto_unload_cont boolean DEFAULT false,
    excl_count boolean DEFAULT false,
    require_cont boolean DEFAULT false,
    is_single_item_only boolean DEFAULT false,
    is_pick_area boolean DEFAULT true,
    pickgroup_id integer,
    length numeric(12,10) DEFAULT 0.0000000000,
    width numeric(12,10) DEFAULT 0.0000000000,
    height numeric(12,10) DEFAULT 0.0000000000,
    flagged_for_count boolean DEFAULT true,
    pickroute_id integer,
    rank character varying(3),
    max_wt numeric(12,10),
    flagged_for_putaway boolean DEFAULT true,
    is_putaway_hold boolean DEFAULT false,
    putaway_hold_comment character varying(255),
    is_pick_hold boolean DEFAULT false,
    pick_hold_comment character varying(255),
    latitude numeric(12,10),
    longitude numeric(12,10),
    last_count_date timestamp without time zone,
    floor smallint DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    CONSTRAINT bin_floor_check CHECK ((floor >= 0)),
    CONSTRAINT bin_pallet_spaces_check CHECK ((pallet_spaces >= (0)::numeric)),
    CONSTRAINT bin_seq_check CHECK ((seq >= 0))
);


ALTER TABLE public.bin OWNER TO wmss;

--
-- Name: COLUMN bin.excl_fr_avail; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_fr_avail IS 'Exclude From Available';


--
-- Name: COLUMN bin.excl_fr_fcast; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_fr_fcast IS 'Exclude From Forecast';


--
-- Name: COLUMN bin.excl_rec; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_rec IS 'Exclude From Receipt';


--
-- Name: COLUMN bin.excl_ship; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_ship IS 'Exclude From Ship';


--
-- Name: COLUMN bin.excl_from_recon; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_from_recon IS 'Exclude From Recon';


--
-- Name: COLUMN bin.reverse_recon; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.reverse_recon IS 'Reverse Reconcile From ERP';


--
-- Name: COLUMN bin.reverse_recon_rsnkey; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.reverse_recon_rsnkey IS 'Reverse Reconcile ERP Reason';


--
-- Name: COLUMN bin.excl_cont; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_cont IS 'Exclude Container';


--
-- Name: COLUMN bin.allow_ship_stage; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.allow_ship_stage IS 'Allow Shipment Staging';


--
-- Name: COLUMN bin.excl_count; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.bin.excl_count IS 'Exclude From Count';


--
-- Name: bin_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.bin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.bin_id_seq OWNER TO wmss;

--
-- Name: bin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.bin_id_seq OWNED BY public.bin.id;


--
-- Name: comp; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.comp (
    id smallint NOT NULL,
    name character varying(12) NOT NULL,
    descr character varying(128),
    updated_at timestamp without time zone,
    updated_by integer
);


ALTER TABLE public.comp OWNER TO wmss;

--
-- Name: TABLE comp; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON TABLE public.comp IS 'Company';


--
-- Name: COLUMN comp.descr; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.comp.descr IS 'Description';


--
-- Name: comp_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.comp_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.comp_id_seq OWNER TO wmss;

--
-- Name: comp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.comp_id_seq OWNED BY public.comp.id;


--
-- Name: inv; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.inv (
    id bigint NOT NULL,
    prod_variant_id integer NOT NULL,
    uom_id integer NOT NULL,
    qty integer DEFAULT 0 NOT NULL,
    bin_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    note character varying(255),
    noted_by integer
);


ALTER TABLE public.inv OWNER TO wmss;

--
-- Name: inv_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.inv_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.inv_id_seq OWNER TO wmss;

--
-- Name: inv_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.inv_id_seq OWNED BY public.inv.id;


--
-- Name: inv_jour; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.inv_jour (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by integer,
    inv_old character varying(2048) NOT NULL,
    inv_new character varying(2048) NOT NULL,
    note character varying(255)
);


ALTER TABLE public.inv_jour OWNER TO wmss;

--
-- Name: inv_jour_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.inv_jour_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.inv_jour_id_seq OWNER TO wmss;

--
-- Name: inv_jour_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.inv_jour_id_seq OWNED BY public.inv_jour.id;


--
-- Name: migration; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time timestamp without time zone
);


ALTER TABLE public.migration OWNER TO wmss;

--
-- Name: pick; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.pick (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer NOT NULL,
    updated_by integer,
    comp_id smallint NOT NULL,
    whse_id smallint,
    name integer,
    tran_cat character varying(6),
    sub_cat character varying(6),
    ok_to_post boolean DEFAULT true NOT NULL,
    is_complete boolean DEFAULT false NOT NULL,
    so_id integer,
    is_released boolean DEFAULT false NOT NULL,
    rpt_group_id smallint,
    pick_rule_id smallint,
    bol_notes text,
    notes text,
    is_bol_printed boolean DEFAULT false NOT NULL,
    shipmeth_id integer,
    is_expedite boolean DEFAULT false NOT NULL
);


ALTER TABLE public.pick OWNER TO wmss;

--
-- Name: pick_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.pick_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.pick_id_seq OWNER TO wmss;

--
-- Name: pick_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.pick_id_seq OWNED BY public.pick.id;


--
-- Name: pickdet; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.pickdet (
    id bigint NOT NULL,
    pickline_id integer NOT NULL,
    seq smallint DEFAULT 0 NOT NULL,
    lot_id integer,
    ser_id integer,
    cont_id integer,
    inv_id integer NOT NULL,
    qty numeric(12,6) DEFAULT 0.000000 NOT NULL,
    prod_uom_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    confirmed boolean DEFAULT false NOT NULL,
    confirmed_at timestamp without time zone,
    confirmed_by integer,
    note character varying(2000)
);


ALTER TABLE public.pickdet OWNER TO wmss;

--
-- Name: pickdet_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.pickdet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.pickdet_id_seq OWNER TO wmss;

--
-- Name: pickdet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.pickdet_id_seq OWNED BY public.pickdet.id;


--
-- Name: pickline; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.pickline (
    id bigint NOT NULL,
    pick_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    seq smallint,
    assigned_to integer,
    prod_var_id integer NOT NULL,
    soline_id integer,
    description character varying(255) NOT NULL,
    wh_id integer,
    bin_id integer,
    zone_id integer,
    qty_to_pick numeric(12,4) DEFAULT 0.0000 NOT NULL,
    orig_qty_to_pick numeric(12,4) DEFAULT 0.0000 NOT NULL,
    prod_uom_id integer NOT NULL,
    min_qty_to_pick numeric(12,4) DEFAULT 0.0000,
    max_qty_to_pick numeric(12,4),
    decimals smallint DEFAULT '4'::smallint NOT NULL,
    cust_id integer,
    cust_addr_id integer,
    completed_at timestamp without time zone,
    ship_addr_id integer,
    pick_completed_by integer,
    note character varying(1023)
);


ALTER TABLE public.pickline OWNER TO wmss;

--
-- Name: pickline_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.pickline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.pickline_id_seq OWNER TO wmss;

--
-- Name: pickline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.pickline_id_seq OWNED BY public.pickline.id;


--
-- Name: prod; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.prod (
    id integer NOT NULL,
    comp_id integer,
    code character varying(32) NOT NULL,
    name character varying(128) NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    active boolean DEFAULT true NOT NULL,
    ext_code character varying(80),
    ext_id integer,
    ext_upd_at timestamp without time zone,
    descr character varying(255),
    long_desc character varying(510),
    rank character varying(2),
    is_inv boolean DEFAULT true NOT NULL,
    decimals smallint DEFAULT 4 NOT NULL,
    stock_uom_id integer,
    pallet_uom_id integer,
    case_uom_id integer,
    mc_uom_id integer,
    std_qty_per_pall integer,
    qty_per_case smallint,
    wave_max_qty smallint
);


ALTER TABLE public.prod OWNER TO wmss;

--
-- Name: COLUMN prod.code; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.code IS 'Product NO';


--
-- Name: COLUMN prod.name; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.name IS 'Product ID';


--
-- Name: COLUMN prod.ext_code; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.ext_code IS 'Code in External System';


--
-- Name: COLUMN prod.ext_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.ext_id IS 'ID in External System';


--
-- Name: COLUMN prod.ext_upd_at; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.ext_upd_at IS 'Updated At in External System';


--
-- Name: COLUMN prod.descr; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.descr IS 'Description';


--
-- Name: COLUMN prod.long_desc; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.long_desc IS 'Long Description';


--
-- Name: COLUMN prod.is_inv; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.is_inv IS 'Is Inventory Tracked';


--
-- Name: COLUMN prod.stock_uom_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.stock_uom_id IS 'Stock UOM';


--
-- Name: COLUMN prod.pallet_uom_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.pallet_uom_id IS 'Pallet UOM';


--
-- Name: COLUMN prod.case_uom_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.case_uom_id IS 'Case UOM';


--
-- Name: COLUMN prod.mc_uom_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.mc_uom_id IS 'Master Case UOM';


--
-- Name: COLUMN prod.std_qty_per_pall; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.std_qty_per_pall IS 'Standard Qty Per Pallet';


--
-- Name: COLUMN prod.qty_per_case; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.qty_per_case IS 'Qty per case';


--
-- Name: COLUMN prod.wave_max_qty; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod.wave_max_qty IS 'Wave Max Qty';


--
-- Name: prod_detail; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.prod_detail (
    id integer NOT NULL,
    prod_variant_id integer NOT NULL,
    variant_val_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    note character varying(255)
);


ALTER TABLE public.prod_detail OWNER TO wmss;

--
-- Name: TABLE prod_detail; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON TABLE public.prod_detail IS 'A particular prod variant';


--
-- Name: COLUMN prod_detail.prod_variant_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod_detail.prod_variant_id IS 'Product Variant ID';


--
-- Name: COLUMN prod_detail.variant_val_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod_detail.variant_val_id IS 'Variant Value ID';


--
-- Name: prod_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.prod_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.prod_detail_id_seq OWNER TO wmss;

--
-- Name: prod_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.prod_detail_id_seq OWNED BY public.prod_detail.id;


--
-- Name: prod_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.prod_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.prod_id_seq OWNER TO wmss;

--
-- Name: prod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.prod_id_seq OWNED BY public.prod.id;


--
-- Name: prod_uom; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.prod_uom (
    id integer NOT NULL,
    prod_id integer NOT NULL,
    uom_id integer NOT NULL,
    comp_id integer NOT NULL,
    cnv_to_stock numeric(12,10) DEFAULT 1.0000000000,
    decimals_ovrd smallint DEFAULT 4,
    weight numeric(12,10) DEFAULT 0.0000000000,
    volume numeric(12,10) DEFAULT 0.0000000000,
    length numeric(12,10) DEFAULT 0.0000000000,
    width numeric(12,10) DEFAULT 0.0000000000,
    height numeric(12,10) DEFAULT 0.0000000000,
    upc character varying(64),
    shippable_pack_series_id integer,
    enabled boolean DEFAULT true NOT NULL,
    lot_specific_cnv smallint DEFAULT 1,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.prod_uom OWNER TO wmss;

--
-- Name: prod_uom_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.prod_uom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.prod_uom_id_seq OWNER TO wmss;

--
-- Name: prod_uom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.prod_uom_id_seq OWNED BY public.prod_uom.id;


--
-- Name: prod_variant; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.prod_variant (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    prod_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    sku character varying(64) NOT NULL,
    price numeric(12,10),
    lot_id integer,
    ser_id integer,
    note character varying(512)
);


ALTER TABLE public.prod_variant OWNER TO wmss;

--
-- Name: COLUMN prod_variant.name; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.prod_variant.name IS 'Friendly Name';


--
-- Name: prod_variant_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.prod_variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.prod_variant_id_seq OWNER TO wmss;

--
-- Name: prod_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.prod_variant_id_seq OWNED BY public.prod_variant.id;


--
-- Name: profile; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.profile (
    user_id integer NOT NULL,
    name character varying(255),
    public_email character varying(255),
    gravatar_email character varying(255),
    gravatar_id character varying(32),
    location character varying(255),
    website character varying(255),
    timezone character varying(40),
    bio text
);


ALTER TABLE public.profile OWNER TO wmss;

--
-- Name: profile_user_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.profile_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.profile_user_id_seq OWNER TO wmss;

--
-- Name: profile_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.profile_user_id_seq OWNED BY public.profile.user_id;


--
-- Name: so; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.so (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    updated_by integer,
    comp_id integer NOT NULL,
    name character varying(64) NOT NULL,
    wh_id integer,
    void boolean DEFAULT false NOT NULL,
    custid character varying(64),
    ord_type character varying(16),
    cust_desc character varying(512),
    description character varying(512),
    pick_instructions character varying(2048),
    ok_to_pick boolean DEFAULT true NOT NULL,
    currency character varying(32),
    erp_id integer,
    erp_order character varying(64),
    ship_complete boolean DEFAULT false NOT NULL,
    hold boolean DEFAULT false NOT NULL,
    hold_rsn character varying(64),
    ship_meth character varying(64),
    more_attr text DEFAULT '{}'::text,
    cust_po character varying(64),
    close_on_first_ship boolean DEFAULT false NOT NULL,
    instructions character varying(2047),
    erp_key integer,
    bill_to character varying(127),
    bill_contact character varying(127),
    bill_email character varying(127),
    bill_addr1 character varying(255),
    bill_addr2 character varying(255),
    bill_addr3 character varying(255) DEFAULT NULL::character varying,
    bill_addr4 character varying(255) DEFAULT NULL::character varying,
    bill_city character varying(127),
    bill_state character varying(63),
    bill_country character(2),
    bill_postal character varying(63),
    bill_phone character varying(63),
    request_date date,
    promise_date date,
    close_date date,
    ship_date date,
    carrier_id integer,
    bol_notes character varying(2047),
    sched_arrival timestamp without time zone,
    is_expedite boolean DEFAULT false NOT NULL,
    pickrule_id integer,
    shipmeth_id integer,
    ship_to character varying(127),
    ship_contact character varying(127),
    ship_email character varying(127),
    ship_phone character varying(63),
    ship_addr1 character varying(255),
    ship_addr2 character varying(255),
    ship_addr3 character varying(255) DEFAULT NULL::character varying,
    ship_addr4 character varying(255) DEFAULT NULL::character varying,
    ship_city character varying(127),
    ship_state character varying(63),
    ship_postal character varying(63),
    ship_country character varying(63),
    cust_addr_id integer,
    total_amt numeric(12,4),
    extra_field_1 character varying(31) DEFAULT NULL::character varying,
    extra_field_2 character varying(31) DEFAULT NULL::character varying,
    extra_field_3 character varying(31) DEFAULT NULL::character varying
);


ALTER TABLE public.so OWNER TO wmss;

--
-- Name: COLUMN so.name; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.so.name IS 'Sales Order Number';


--
-- Name: COLUMN so.cust_addr_id; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.so.cust_addr_id IS 'Customer Address ID';


--
-- Name: so_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.so_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.so_id_seq OWNER TO wmss;

--
-- Name: so_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.so_id_seq OWNED BY public.so.id;


--
-- Name: social_account; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.social_account (
    id integer NOT NULL,
    user_id integer,
    provider character varying(255) NOT NULL,
    client_id character varying(255) NOT NULL,
    code character varying(32),
    email character varying(255),
    username character varying(255),
    data text,
    created_at integer
);


ALTER TABLE public.social_account OWNER TO wmss;

--
-- Name: social_account_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.social_account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.social_account_id_seq OWNER TO wmss;

--
-- Name: social_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.social_account_id_seq OWNED BY public.social_account.id;


--
-- Name: soline; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.soline (
    id bigint NOT NULL,
    so character varying(64) NOT NULL,
    lineid character varying(128),
    company character varying(12) NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    customer character varying(64),
    cust_id integer,
    seq smallint,
    void boolean DEFAULT false NOT NULL,
    erp_id integer,
    erp_soline character varying(128),
    wh character varying(31) DEFAULT NULL::character varying,
    prod_var character varying(128) NOT NULL,
    description character varying(512),
    non_inv boolean DEFAULT false NOT NULL,
    qty_ord integer NOT NULL,
    qty_shipped integer DEFAULT 0 NOT NULL,
    erp_qty_open_to_ship integer,
    ok_to_pick boolean DEFAULT true NOT NULL,
    pickrule character varying(64),
    uom character varying(64) NOT NULL,
    cnv_to_stk numeric(12,10) DEFAULT 1.0000000000 NOT NULL,
    erp_so_id integer,
    erp_so character varying(128),
    req_ship_date timestamp without time zone,
    promise_date timestamp without time zone,
    shipmeth character varying(64),
    ship_priority smallint DEFAULT 255,
    ship_to character varying(255),
    ship_contact character varying(255),
    ship_email character varying(255),
    addr1 character varying(511),
    addr2 character varying(511),
    addr3 character varying(511),
    addr4 character varying(511),
    city character varying(255),
    state character varying(128),
    postal character varying(64),
    country_code character(2) DEFAULT 'us'::bpchar NOT NULL,
    phone1 character varying(64),
    phone2 character varying(64),
    cancel_date timestamp without time zone,
    additional_info character varying(2047),
    extra_field_1 character varying(31),
    extra_field_2 character varying(31),
    extra_field_3 character varying(31),
    uom_id integer,
    so_id integer,
    addr_hash character varying(2500),
    wt_per numeric(12,10),
    comment character varying(2047),
    decimals smallint DEFAULT 4 NOT NULL,
    unit_price numeric(12,10),
    ext_price numeric(12,10),
    is_backorder boolean DEFAULT false NOT NULL,
    comp_id integer,
    prod_var_id integer,
    wh_id integer,
    created_by integer,
    sync_stat smallint DEFAULT 0,
    sync_msg character varying(511),
    last_synced timestamp without time zone
);


ALTER TABLE public.soline OWNER TO wmss;

--
-- Name: COLUMN soline.sync_msg; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.soline.sync_msg IS 'Sync Message';


--
-- Name: COLUMN soline.last_synced; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.soline.last_synced IS 'Last Synced At';


--
-- Name: soline_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.soline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.soline_id_seq OWNER TO wmss;

--
-- Name: soline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.soline_id_seq OWNED BY public.soline.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.token (
    user_id integer,
    code character varying(32) NOT NULL,
    type smallint NOT NULL,
    created_at integer NOT NULL
);


ALTER TABLE public.token OWNER TO wmss;

--
-- Name: uom; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.uom (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    descr character varying(255) DEFAULT NULL::character varying,
    comp_id integer NOT NULL,
    uom_type smallint,
    is_base boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    cnv_to_base numeric(12,10),
    erp_key integer,
    erp_uom character varying(64) DEFAULT NULL::character varying
);


ALTER TABLE public.uom OWNER TO wmss;

--
-- Name: uom_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.uom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.uom_id_seq OWNER TO wmss;

--
-- Name: uom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.uom_id_seq OWNED BY public.uom.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password_hash character varying(60) NOT NULL,
    auth_key character varying(32) NOT NULL,
    unconfirmed_email character varying(255),
    registration_ip character varying(45),
    flags integer DEFAULT 0 NOT NULL,
    confirmed_at integer,
    blocked_at integer,
    updated_at integer NOT NULL,
    created_at integer NOT NULL,
    last_login_at integer,
    last_login_ip character varying(45),
    auth_tf_key character varying(16),
    auth_tf_enabled smallint DEFAULT 0,
    auth_tf_type character varying(20),
    auth_tf_mobile_phone character varying(20),
    password_changed_at integer,
    gdpr_consent smallint DEFAULT 0,
    gdpr_consent_date integer,
    gdpr_deleted smallint DEFAULT 0
);


ALTER TABLE public."user" OWNER TO wmss;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.user_id_seq OWNER TO wmss;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: variant; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.variant (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    type public.variant_type DEFAULT 'String'::public.variant_type NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    long_desc character varying(512)
);


ALTER TABLE public.variant OWNER TO wmss;

--
-- Name: COLUMN variant.name; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.variant.name IS 'Variant Name';


--
-- Name: variant_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.variant_id_seq OWNER TO wmss;

--
-- Name: variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.variant_id_seq OWNED BY public.variant.id;


--
-- Name: variant_val; Type: TABLE; Schema: public; Owner: wmss
--

CREATE TABLE public.variant_val (
    id integer NOT NULL,
    variant_id integer NOT NULL,
    value_int integer,
    value_str character varying(512),
    value_dtm timestamp without time zone,
    value_float numeric(12,8),
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    created_by integer,
    updated_by integer,
    note character varying(512)
);


ALTER TABLE public.variant_val OWNER TO wmss;

--
-- Name: variant_val_id_seq; Type: SEQUENCE; Schema: public; Owner: wmss
--

CREATE SEQUENCE public.variant_val_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.variant_val_id_seq OWNER TO wmss;

--
-- Name: variant_val_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: wmss
--

ALTER SEQUENCE public.variant_val_id_seq OWNED BY public.variant_val.id;


--
-- Name: vbin; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vbin AS
 SELECT b.id,
    c.name AS company,
    b.name,
    b.zone_id,
    b.description,
    b.active,
    b.excl_fr_avail,
    b.excl_fr_fcast,
    b.allow_neg_inv,
    b.seq,
    b.erp_int,
    b.excl_rec,
    b.excl_ship,
    b.erp_loc,
    b.pallet_spaces,
    b.primary_zone_id,
    b.excl_from_recon,
    b.reverse_recon,
    b.reverse_recon_rsnkey,
    b.excl_cont,
    b.allow_ship_stage,
    b.perm_move_in,
    b.perm_move_out,
    b.disallow_hold_inv,
    b.height_level,
    b."row",
    b.col,
    b.is_item_restricted,
    b.loc_positions,
    b.is_shipping_lane,
    b.is_consolidation_area,
    b.is_pack_area,
    b.auto_unload_cont,
    b.excl_count,
    b.require_cont,
    b.is_single_item_only,
    b.is_pick_area,
    b.pickgroup_id,
    b.length,
    b.width,
    b.height,
    b.flagged_for_count,
    b.pickroute_id,
    b.rank,
    b.max_wt,
    b.flagged_for_putaway,
    b.is_putaway_hold,
    b.putaway_hold_comment,
    b.is_pick_hold,
    b.pick_hold_comment,
    b.latitude,
    b.longitude,
    b.last_count_date,
    b.floor,
    b.created_at,
    b.updated_at,
    b.created_by,
    b.updated_by,
    b.comp_id
   FROM (public.bin b
     JOIN public.comp c ON ((c.id = b.comp_id)));


ALTER VIEW public.vbin OWNER TO wmss;

--
-- Name: vinv; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vinv AS
 SELECT inv.id,
    p.code AS prod,
    p.name AS prod_name,
    pv.name AS prod_variant,
    inv.qty,
    concat(inv.qty, ' ', uom.name, ' ', pv.name, ' @ ', bin.name) AS descr,
    uom.name AS uom,
    inv.created_at,
    inv.updated_at,
    inv.created_by,
    inv.updated_by,
    inv.note,
    inv.noted_by,
    inv.prod_variant_id,
    p.id AS prod_id,
    inv.uom_id,
    pu.id AS prod_uom_id,
    inv.bin_id
   FROM (((((public.inv
     JOIN public.prod_variant pv ON ((pv.id = inv.prod_variant_id)))
     JOIN public.bin ON ((bin.id = inv.bin_id)))
     JOIN public.prod p ON ((pv.prod_id = p.id)))
     JOIN public.uom ON ((uom.id = inv.uom_id)))
     JOIN public.prod_uom pu ON (((uom.id = pu.uom_id) AND (p.id = pu.prod_id))));


ALTER VIEW public.vinv OWNER TO wmss;

--
-- Name: vpick; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vpick AS
 SELECT p.id,
    p.created_at,
    p.updated_at,
    u.username AS created_by,
    uu.username AS updated_by,
    c.name AS comp,
    p.name,
    p.tran_cat,
    p.sub_cat,
    p.ok_to_post,
    p.is_complete,
    p.so_id,
    p.is_released,
    p.rpt_group_id,
    p.pick_rule_id,
    p.bol_notes,
    p.notes,
    p.is_bol_printed,
    p.shipmeth_id,
    p.is_expedite,
    p.created_by AS created_by_user_id,
    p.updated_by AS updated_by_user_id,
    p.comp_id,
    p.whse_id
   FROM (((public.pick p
     JOIN public.comp c ON ((c.id = p.comp_id)))
     LEFT JOIN public."user" u ON ((u.id = p.created_by)))
     LEFT JOIN public."user" uu ON ((uu.id = p.updated_by)));


ALTER VIEW public.vpick OWNER TO wmss;

--
-- Name: vpickdet; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vpickdet AS
 SELECT pd.id,
    pd.pickline_id,
    sol.id AS soline_id,
    so.name AS sales_order,
    pd.seq,
    pd.qty,
    (pd.qty * u.cnv_to_base) AS qty_stock,
    b.name AS bin,
    pv.name AS product_variant,
    u.name AS uom,
    pd.created_at,
    pd.updated_at,
    pd.created_by,
    pd.updated_by,
    pd.confirmed,
    pd.confirmed_at,
    pd.confirmed_by,
    pd.note,
    pd.lot_id,
    pd.ser_id,
    pd.cont_id,
    pd.inv_id,
    pd.prod_uom_id
   FROM ((((((((public.pickdet pd
     JOIN public.pickline pl ON ((pl.id = pd.pickline_id)))
     LEFT JOIN public.soline sol ON ((pl.soline_id = sol.id)))
     LEFT JOIN public.so ON ((sol.so_id = so.id)))
     JOIN public.inv ON ((inv.id = pd.inv_id)))
     JOIN public.bin b ON ((inv.bin_id = b.id)))
     JOIN public.prod_uom pu ON ((pd.prod_uom_id = pu.id)))
     JOIN public.prod_variant pv ON ((pl.prod_var_id = pv.id)))
     JOIN public.uom u ON ((pu.uom_id = u.id)));


ALTER VIEW public.vpickdet OWNER TO wmss;

--
-- Name: COLUMN vpickdet.sales_order; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.vpickdet.sales_order IS 'Sales Order Number';


--
-- Name: COLUMN vpickdet.product_variant; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.vpickdet.product_variant IS 'Friendly Name';


--
-- Name: vpickline; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vpickline AS
 SELECT pl.id,
    pl.pick_id,
    p.name AS pick_name,
    pl.created_at,
    pl.updated_at,
    u.username AS created_by,
    uu.username AS updated_by,
    prod.code AS prod,
    prod.name AS prod_name,
    pv.name AS prod_variant,
    uom.name AS uom,
    pl.seq,
    pl.assigned_to,
    pl.soline_id,
    pl.description,
    pl.wh_id,
    pl.bin_id,
    pl.zone_id,
    pl.qty_to_pick,
    pl.orig_qty_to_pick,
    pl.min_qty_to_pick,
    pl.max_qty_to_pick,
    pl.decimals,
    pl.cust_id,
    pl.cust_addr_id,
    pl.completed_at,
    pl.ship_addr_id,
    pl.pick_completed_by,
    pl.note,
    pl.created_by AS created_by_user_id,
    pl.updated_by AS updated_by_user_id,
    prod.id AS prod_id,
    pl.prod_var_id,
    pl.prod_uom_id,
    uom.id AS uom_id
   FROM (((((((public.pickline pl
     JOIN public.pick p ON ((p.id = pl.pick_id)))
     JOIN public.prod_variant pv ON ((pv.id = pl.prod_var_id)))
     JOIN public.prod ON ((prod.id = pv.prod_id)))
     JOIN public.prod_uom pu ON ((pl.prod_uom_id = pu.id)))
     JOIN public.uom uom ON ((pu.uom_id = uom.id)))
     LEFT JOIN public."user" u ON ((u.id = pl.created_by)))
     LEFT JOIN public."user" uu ON ((uu.id = pl.updated_by)));


ALTER VIEW public.vpickline OWNER TO wmss;

--
-- Name: vpickline_pickdet; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vpickline_pickdet AS
 SELECT pl.id,
    pl.pick_id,
    pl.created_at,
    pl.updated_at,
    pl.created_by,
    pl.updated_by,
    pl.seq,
    pl.assigned_to,
    pl.prod_var_id,
    pl.soline_id,
    p.name AS product,
    u.name AS uom,
    pl.description,
    pl.wh_id,
    pl.bin_id,
    pl.zone_id,
    pl.qty_to_pick,
    (pl.qty_to_pick * u.cnv_to_base) AS qty_to_pick_stock,
    COALESCE(pd_per_pl.sum_qty_picked_stock, (0)::numeric) AS sum_qty_picked_stock,
    pl.orig_qty_to_pick,
    pl.min_qty_to_pick,
    pl.max_qty_to_pick,
    pl.decimals,
    pl.cust_id,
    pl.cust_addr_id,
    pl.completed_at,
    pl.ship_addr_id,
    pl.pick_completed_by,
    pl.note,
    pl.prod_uom_id
   FROM ((((public.pickline pl
     JOIN public.prod_uom pu ON ((pl.prod_uom_id = pu.id)))
     JOIN public.prod p ON ((pu.prod_id = p.id)))
     JOIN public.uom u ON ((pu.uom_id = u.id)))
     JOIN ( SELECT vpickdet.pickline_id,
            sum(vpickdet.qty_stock) AS sum_qty_picked_stock
           FROM public.vpickdet
          GROUP BY vpickdet.pickline_id) pd_per_pl ON ((pd_per_pl.pickline_id = pl.id)));


ALTER VIEW public.vpickline_pickdet OWNER TO wmss;

--
-- Name: vprod_uom; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vprod_uom AS
 SELECT pu.id,
    pu.enabled,
    p.code AS prod,
    p.name AS prod_name,
    u.name AS uom,
    u.name,
    pu.cnv_to_stock,
    pu.decimals_ovrd,
    pu.weight,
    pu.volume,
    pu.length,
    pu.width,
    pu.height,
    pu.upc,
    pu.shippable_pack_series_id,
    pu.lot_specific_cnv,
    pu.created_at,
    pu.updated_at,
    pu.prod_id,
    pu.uom_id,
    pu.comp_id
   FROM ((public.prod_uom pu
     JOIN public.prod p ON ((p.id = pu.prod_id)))
     JOIN public.uom u ON ((u.id = pu.uom_id)));


ALTER VIEW public.vprod_uom OWNER TO wmss;

--
-- Name: COLUMN vprod_uom.prod; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.vprod_uom.prod IS 'Product Code';


--
-- Name: vprod_variant; Type: VIEW; Schema: public; Owner: wmss
--

CREATE VIEW public.vprod_variant AS
 SELECT pv.id,
    pv.name,
    p.code AS prod,
    p.name AS prod_name,
    concat(p.name, ' ', pv.name) AS full_name,
    pv.created_at,
    pv.updated_at,
    u_cr.username AS created_by_user,
    u_upd.username AS updated_by_user,
    pv.sku,
    pv.price,
    pv.note,
    pv.prod_id,
    pv.created_by,
    pv.updated_by,
    pv.lot_id,
    pv.ser_id
   FROM (((public.prod_variant pv
     JOIN public.prod p ON ((p.id = pv.prod_id)))
     LEFT JOIN public."user" u_cr ON ((pv.created_by = u_cr.id)))
     LEFT JOIN public."user" u_upd ON ((pv.updated_by = u_upd.id)));


ALTER VIEW public.vprod_variant OWNER TO wmss;

--
-- Name: COLUMN vprod_variant.name; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.vprod_variant.name IS 'Friendly Name';


--
-- Name: COLUMN vprod_variant.prod; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON COLUMN public.vprod_variant.prod IS 'Product Code';


--
-- Name: bin id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.bin ALTER COLUMN id SET DEFAULT nextval('public.bin_id_seq'::regclass);


--
-- Name: comp id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.comp ALTER COLUMN id SET DEFAULT nextval('public.comp_id_seq'::regclass);


--
-- Name: inv id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.inv ALTER COLUMN id SET DEFAULT nextval('public.inv_id_seq'::regclass);


--
-- Name: inv_jour id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.inv_jour ALTER COLUMN id SET DEFAULT nextval('public.inv_jour_id_seq'::regclass);


--
-- Name: pick id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pick ALTER COLUMN id SET DEFAULT nextval('public.pick_id_seq'::regclass);


--
-- Name: pickdet id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet ALTER COLUMN id SET DEFAULT nextval('public.pickdet_id_seq'::regclass);


--
-- Name: pickline id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline ALTER COLUMN id SET DEFAULT nextval('public.pickline_id_seq'::regclass);


--
-- Name: prod id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod ALTER COLUMN id SET DEFAULT nextval('public.prod_id_seq'::regclass);


--
-- Name: prod_detail id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_detail ALTER COLUMN id SET DEFAULT nextval('public.prod_detail_id_seq'::regclass);


--
-- Name: prod_uom id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_uom ALTER COLUMN id SET DEFAULT nextval('public.prod_uom_id_seq'::regclass);


--
-- Name: prod_variant id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_variant ALTER COLUMN id SET DEFAULT nextval('public.prod_variant_id_seq'::regclass);


--
-- Name: profile user_id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.profile ALTER COLUMN user_id SET DEFAULT nextval('public.profile_user_id_seq'::regclass);


--
-- Name: so id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.so ALTER COLUMN id SET DEFAULT nextval('public.so_id_seq'::regclass);


--
-- Name: social_account id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.social_account ALTER COLUMN id SET DEFAULT nextval('public.social_account_id_seq'::regclass);


--
-- Name: soline id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline ALTER COLUMN id SET DEFAULT nextval('public.soline_id_seq'::regclass);


--
-- Name: uom id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.uom ALTER COLUMN id SET DEFAULT nextval('public.uom_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: variant id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.variant ALTER COLUMN id SET DEFAULT nextval('public.variant_id_seq'::regclass);


--
-- Name: variant_val id; Type: DEFAULT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.variant_val ALTER COLUMN id SET DEFAULT nextval('public.variant_val_id_seq'::regclass);


--
-- Data for Name: auth_assignment; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.auth_assignment (item_name, user_id, created_at) FROM stdin;
\.


--
-- Data for Name: auth_item; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.auth_item (name, type, description, rule_name, data, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: auth_item_child; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.auth_item_child (parent, child) FROM stdin;
\.


--
-- Data for Name: auth_rule; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.auth_rule (name, data, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: bin; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.bin (id, comp_id, name, zone_id, description, active, excl_fr_avail, excl_fr_fcast, allow_neg_inv, seq, erp_int, excl_rec, excl_ship, erp_loc, pallet_spaces, primary_zone_id, excl_from_recon, reverse_recon, reverse_recon_rsnkey, excl_cont, allow_ship_stage, perm_move_in, perm_move_out, disallow_hold_inv, height_level, "row", col, is_item_restricted, loc_positions, is_shipping_lane, is_consolidation_area, is_pack_area, auto_unload_cont, excl_count, require_cont, is_single_item_only, is_pick_area, pickgroup_id, length, width, height, flagged_for_count, pickroute_id, rank, max_wt, flagged_for_putaway, is_putaway_hold, putaway_hold_comment, is_pick_hold, pick_hold_comment, latitude, longitude, last_count_date, floor, created_at, updated_at, created_by, updated_by) FROM stdin;
2	1	Lower Bin	\N	Lower Bin	t	f	f	f	1	\N	f	f	\N	0.0000000000	\N	f	f	\N	f	t	\N	\N	f	0.0000000000	0	0	f	0.0000000000	f	f	f	f	f	f	f	t	\N	0.0000000000	0.0000000000	0.0000000000	t	\N	\N	\N	t	f	\N	f	\N	\N	\N	\N	0	2022-11-27 18:30:52	\N	\N	\N
1	1	Upper Bin	\N	Upper Bin	t	f	f	f	1	\N	f	f		\N	\N	f	f	\N	f	f			f	\N	\N	\N	f	\N	f	f	f	f	f	f	f	f	\N	\N	\N	\N	f	\N		\N	f	f		f		\N	\N	\N	0	2022-11-27 18:20:23	\N	\N	\N
\.


--
-- Data for Name: comp; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.comp (id, name, descr, updated_at, updated_by) FROM stdin;
1	SOCAL	Socal Test Company	2023-04-15 19:43:46	\N
\.


--
-- Data for Name: inv; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.inv (id, prod_variant_id, uom_id, qty, bin_id, created_at, updated_at, created_by, updated_by, note, noted_by) FROM stdin;
\.


--
-- Data for Name: inv_jour; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.inv_jour (id, created_at, created_by, inv_old, inv_new, note) FROM stdin;
\.


--
-- Data for Name: migration; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.migration (version, apply_time) FROM stdin;
\.


--
-- Data for Name: pick; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.pick (id, created_at, updated_at, created_by, updated_by, comp_id, whse_id, name, tran_cat, sub_cat, ok_to_post, is_complete, so_id, is_released, rpt_group_id, pick_rule_id, bol_notes, notes, is_bol_printed, shipmeth_id, is_expedite) FROM stdin;
\.


--
-- Data for Name: pickdet; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.pickdet (id, pickline_id, seq, lot_id, ser_id, cont_id, inv_id, qty, prod_uom_id, created_at, updated_at, created_by, updated_by, confirmed, confirmed_at, confirmed_by, note) FROM stdin;
\.


--
-- Data for Name: pickline; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.pickline (id, pick_id, created_at, updated_at, created_by, updated_by, seq, assigned_to, prod_var_id, soline_id, description, wh_id, bin_id, zone_id, qty_to_pick, orig_qty_to_pick, prod_uom_id, min_qty_to_pick, max_qty_to_pick, decimals, cust_id, cust_addr_id, completed_at, ship_addr_id, pick_completed_by, note) FROM stdin;
\.


--
-- Data for Name: prod; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.prod (id, comp_id, code, name, created_at, updated_at, created_by, updated_by, active, ext_code, ext_id, ext_upd_at, descr, long_desc, rank, is_inv, decimals, stock_uom_id, pallet_uom_id, case_uom_id, mc_uom_id, std_qty_per_pall, qty_per_case, wave_max_qty) FROM stdin;
\.


--
-- Data for Name: prod_detail; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.prod_detail (id, prod_variant_id, variant_val_id, created_at, updated_at, created_by, updated_by, note) FROM stdin;
\.


--
-- Data for Name: prod_uom; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.prod_uom (id, prod_id, uom_id, comp_id, cnv_to_stock, decimals_ovrd, weight, volume, length, width, height, upc, shippable_pack_series_id, enabled, lot_specific_cnv, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: prod_variant; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.prod_variant (id, name, prod_id, created_at, updated_at, created_by, updated_by, sku, price, lot_id, ser_id, note) FROM stdin;
\.


--
-- Data for Name: profile; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.profile (user_id, name, public_email, gravatar_email, gravatar_id, location, website, timezone, bio) FROM stdin;
\.


--
-- Data for Name: so; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.so (id, created_at, updated_at, updated_by, comp_id, name, wh_id, void, custid, ord_type, cust_desc, description, pick_instructions, ok_to_pick, currency, erp_id, erp_order, ship_complete, hold, hold_rsn, ship_meth, more_attr, cust_po, close_on_first_ship, instructions, erp_key, bill_to, bill_contact, bill_email, bill_addr1, bill_addr2, bill_addr3, bill_addr4, bill_city, bill_state, bill_country, bill_postal, bill_phone, request_date, promise_date, close_date, ship_date, carrier_id, bol_notes, sched_arrival, is_expedite, pickrule_id, shipmeth_id, ship_to, ship_contact, ship_email, ship_phone, ship_addr1, ship_addr2, ship_addr3, ship_addr4, ship_city, ship_state, ship_postal, ship_country, cust_addr_id, total_amt, extra_field_1, extra_field_2, extra_field_3) FROM stdin;
\.


--
-- Data for Name: social_account; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.social_account (id, user_id, provider, client_id, code, email, username, data, created_at) FROM stdin;
\.


--
-- Data for Name: soline; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.soline (id, so, lineid, company, created_at, updated_at, customer, cust_id, seq, void, erp_id, erp_soline, wh, prod_var, description, non_inv, qty_ord, qty_shipped, erp_qty_open_to_ship, ok_to_pick, pickrule, uom, cnv_to_stk, erp_so_id, erp_so, req_ship_date, promise_date, shipmeth, ship_priority, ship_to, ship_contact, ship_email, addr1, addr2, addr3, addr4, city, state, postal, country_code, phone1, phone2, cancel_date, additional_info, extra_field_1, extra_field_2, extra_field_3, uom_id, so_id, addr_hash, wt_per, comment, decimals, unit_price, ext_price, is_backorder, comp_id, prod_var_id, wh_id, created_by, sync_stat, sync_msg, last_synced) FROM stdin;
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.token (user_id, code, type, created_at) FROM stdin;
\.


--
-- Data for Name: uom; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.uom (id, name, descr, comp_id, uom_type, is_base, created_at, updated_at, cnv_to_base, erp_key, erp_uom) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public."user" (id, username, email, password_hash, auth_key, unconfirmed_email, registration_ip, flags, confirmed_at, blocked_at, updated_at, created_at, last_login_at, last_login_ip, auth_tf_key, auth_tf_enabled, auth_tf_type, auth_tf_mobile_phone, password_changed_at, gdpr_consent, gdpr_consent_date, gdpr_deleted) FROM stdin;
56	johndoe	someids@gmail.com	$2y$10$i05hX2k33DmovbXb8tc1MeKw3hA6DHHUwgiwOtkVjTF7YZVhg8mq6	NpwWA4GjbCZCYCy8tl8FesHo5BsJDkUn	\N	127.0.0.1	0	1670391144	\N	1670649250	1670649250	1682215840	127.0.0.1		0	\N	\N	1670649250	0	\N	0
27	brian3t	ngxtri@gmail.com	$2y$10$i05hX2k33DmovbXb8tc1MeKw3hA6DHHUwgiwOtkVjTF7YZVhg8mq6	NpwWA4GjbCZCYCy8tl8FesHo5BsJDkUn	\N	\N	0	1670391144	\N	1670391144	1670391144	1739728313	127.0.0.1		0	\N	\N	1672537779	0	\N	0
\.


--
-- Data for Name: variant; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.variant (id, name, type, created_at, updated_at, created_by, updated_by, long_desc) FROM stdin;
\.


--
-- Data for Name: variant_val; Type: TABLE DATA; Schema: public; Owner: wmss
--

COPY public.variant_val (id, variant_id, value_int, value_str, value_dtm, value_float, created_at, updated_at, created_by, updated_by, note) FROM stdin;
\.


--
-- Name: bin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.bin_id_seq', 1, false);


--
-- Name: comp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.comp_id_seq', 1, false);


--
-- Name: inv_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.inv_id_seq', 1, false);


--
-- Name: inv_jour_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.inv_jour_id_seq', 1, false);


--
-- Name: pick_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.pick_id_seq', 1, false);


--
-- Name: pickdet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.pickdet_id_seq', 1, false);


--
-- Name: pickline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.pickline_id_seq', 1, false);


--
-- Name: prod_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.prod_detail_id_seq', 1, false);


--
-- Name: prod_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.prod_id_seq', 1, false);


--
-- Name: prod_uom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.prod_uom_id_seq', 1, false);


--
-- Name: prod_variant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.prod_variant_id_seq', 1, false);


--
-- Name: profile_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.profile_user_id_seq', 1, false);


--
-- Name: so_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.so_id_seq', 1, false);


--
-- Name: social_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.social_account_id_seq', 1, false);


--
-- Name: soline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.soline_id_seq', 1, false);


--
-- Name: uom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.uom_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.user_id_seq', 1, false);


--
-- Name: variant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.variant_id_seq', 1, false);


--
-- Name: variant_val_id_seq; Type: SEQUENCE SET; Schema: public; Owner: wmss
--

SELECT pg_catalog.setval('public.variant_val_id_seq', 1, false);


--
-- Name: auth_assignment auth_assignment_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_assignment
    ADD CONSTRAINT auth_assignment_pkey PRIMARY KEY (item_name, user_id);


--
-- Name: auth_item auth_item_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_item
    ADD CONSTRAINT auth_item_pkey PRIMARY KEY (name);


--
-- Name: auth_rule auth_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_rule
    ADD CONSTRAINT auth_rule_pkey PRIMARY KEY (name);


--
-- Name: bin bin_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.bin
    ADD CONSTRAINT bin_pkey PRIMARY KEY (id);


--
-- Name: comp comp_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.comp
    ADD CONSTRAINT comp_pkey PRIMARY KEY (id);


--
-- Name: so comp_so_pk; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.so
    ADD CONSTRAINT comp_so_pk UNIQUE (comp_id, name);


--
-- Name: social_account idx_social_account_code; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.social_account
    ADD CONSTRAINT idx_social_account_code UNIQUE (code);


--
-- Name: social_account idx_social_account_provider_client_id; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.social_account
    ADD CONSTRAINT idx_social_account_provider_client_id UNIQUE (provider, client_id);


--
-- Name: token idx_token_user_id_code_type; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT idx_token_user_id_code_type UNIQUE (user_id, code, type);


--
-- Name: user idx_user_email; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT idx_user_email UNIQUE (email);


--
-- Name: user idx_user_username; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT idx_user_username UNIQUE (username);


--
-- Name: inv_jour inv_jour_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.inv_jour
    ADD CONSTRAINT inv_jour_pkey PRIMARY KEY (id);


--
-- Name: inv inv_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.inv
    ADD CONSTRAINT inv_pkey PRIMARY KEY (id);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: pick pick_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pick
    ADD CONSTRAINT pick_pkey PRIMARY KEY (id);


--
-- Name: pickdet pickdet_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_pkey PRIMARY KEY (id);


--
-- Name: pickline pickline_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_pkey PRIMARY KEY (id);


--
-- Name: prod_detail prod_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_detail
    ADD CONSTRAINT prod_detail_pkey PRIMARY KEY (id);


--
-- Name: prod prod_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod
    ADD CONSTRAINT prod_pkey PRIMARY KEY (id);


--
-- Name: prod_uom prod_uom_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_uom
    ADD CONSTRAINT prod_uom_pkey PRIMARY KEY (id);


--
-- Name: prod_variant prod_variant_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_variant
    ADD CONSTRAINT prod_variant_pkey PRIMARY KEY (id);


--
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (user_id);


--
-- Name: so so_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.so
    ADD CONSTRAINT so_pkey PRIMARY KEY (id);


--
-- Name: social_account social_account_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.social_account
    ADD CONSTRAINT social_account_pkey PRIMARY KEY (id);


--
-- Name: soline soline_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline
    ADD CONSTRAINT soline_pkey PRIMARY KEY (id);


--
-- Name: uom uom_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.uom
    ADD CONSTRAINT uom_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: variant variant_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.variant
    ADD CONSTRAINT variant_pkey PRIMARY KEY (id);


--
-- Name: variant_val variant_val_pkey; Type: CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.variant_val
    ADD CONSTRAINT variant_val_pkey PRIMARY KEY (id);


--
-- Name: child; Type: INDEX; Schema: public; Owner: wmss
--

CREATE INDEX child ON public.auth_item_child USING btree (child);


--
-- Name: pickid; Type: INDEX; Schema: public; Owner: wmss
--

CREATE INDEX pickid ON public.pickline USING btree (pick_id);


--
-- Name: prod_code; Type: INDEX; Schema: public; Owner: wmss
--

CREATE INDEX prod_code ON public.prod USING btree (code);


--
-- Name: variant_id; Type: INDEX; Schema: public; Owner: wmss
--

CREATE INDEX variant_id ON public.variant_val USING btree (variant_id);


--
-- Name: auth_assignment auth_assignment_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_assignment
    ADD CONSTRAINT auth_assignment_ibfk_1 FOREIGN KEY (item_name) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_item_child auth_item_child_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_item_child
    ADD CONSTRAINT auth_item_child_ibfk_1 FOREIGN KEY (parent) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_item_child auth_item_child_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.auth_item_child
    ADD CONSTRAINT auth_item_child_ibfk_2 FOREIGN KEY (child) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bin bin_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.bin
    ADD CONSTRAINT bin_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id) ON DELETE CASCADE;


--
-- Name: CONSTRAINT bin_comp_id_fk ON bin; Type: COMMENT; Schema: public; Owner: wmss
--

COMMENT ON CONSTRAINT bin_comp_id_fk ON public.bin IS 'com';


--
-- Name: profile fk_profile_user; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.profile
    ADD CONSTRAINT fk_profile_user FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: social_account fk_social_account_user; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.social_account
    ADD CONSTRAINT fk_social_account_user FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: token fk_token_user; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT fk_token_user FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;


--
-- Name: inv inv_bin; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.inv
    ADD CONSTRAINT inv_bin FOREIGN KEY (bin_id) REFERENCES public.bin(id) ON DELETE CASCADE;


--
-- Name: pick pick_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pick
    ADD CONSTRAINT pick_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id) ON DELETE CASCADE;


--
-- Name: pickdet pickdet_inv_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_inv_id_fk FOREIGN KEY (inv_id) REFERENCES public.inv(id);


--
-- Name: pickdet pickdet_prod_uom_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_prod_uom_id_fk FOREIGN KEY (prod_uom_id) REFERENCES public.prod_uom(id);


--
-- Name: pickdet pickdet_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_user_id_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickdet pickdet_user_id_fk2; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_user_id_fk2 FOREIGN KEY (updated_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickdet pickdet_user_id_fk3; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickdet
    ADD CONSTRAINT pickdet_user_id_fk3 FOREIGN KEY (confirmed_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickline pickline_bin_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_bin_id_fk FOREIGN KEY (bin_id) REFERENCES public.bin(id) ON DELETE SET NULL;


--
-- Name: pickline pickline_pick_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_pick_id_fk FOREIGN KEY (pick_id) REFERENCES public.pick(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pickline pickline_prod_uom_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_prod_uom_id_fk FOREIGN KEY (prod_uom_id) REFERENCES public.prod_uom(id);


--
-- Name: pickline pickline_prod_variant_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_prod_variant_id_fk FOREIGN KEY (prod_var_id) REFERENCES public.prod_variant(id) ON DELETE CASCADE;


--
-- Name: pickline pickline_soline_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_soline_id_fk FOREIGN KEY (soline_id) REFERENCES public.soline(id) ON DELETE CASCADE;


--
-- Name: pickline pickline_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_user_id_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickline pickline_user_id_fk2; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_user_id_fk2 FOREIGN KEY (updated_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickline pickline_user_id_fk3; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_user_id_fk3 FOREIGN KEY (assigned_to) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: pickline pickline_user_id_fk4; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.pickline
    ADD CONSTRAINT pickline_user_id_fk4 FOREIGN KEY (pick_completed_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: prod prod_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod
    ADD CONSTRAINT prod_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id) ON DELETE CASCADE;


--
-- Name: prod_detail prod_detail_prod_variant; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_detail
    ADD CONSTRAINT prod_detail_prod_variant FOREIGN KEY (prod_variant_id) REFERENCES public.prod_variant(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: prod_detail prod_detail_variant_val; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_detail
    ADD CONSTRAINT prod_detail_variant_val FOREIGN KEY (variant_val_id) REFERENCES public.variant_val(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: prod_uom prod_uom_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_uom
    ADD CONSTRAINT prod_uom_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id) ON DELETE CASCADE;


--
-- Name: prod_uom prod_uom_prod_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_uom
    ADD CONSTRAINT prod_uom_prod_fk FOREIGN KEY (prod_id) REFERENCES public.prod(id);


--
-- Name: prod_uom prod_uom_uom_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_uom
    ADD CONSTRAINT prod_uom_uom_fk FOREIGN KEY (uom_id) REFERENCES public.uom(id);


--
-- Name: prod_variant prod_val_prod; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.prod_variant
    ADD CONSTRAINT prod_val_prod FOREIGN KEY (prod_id) REFERENCES public.prod(id) ON DELETE CASCADE;


--
-- Name: so so_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.so
    ADD CONSTRAINT so_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id);


--
-- Name: soline soline_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline
    ADD CONSTRAINT soline_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id);


--
-- Name: soline soline_prod_variant_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline
    ADD CONSTRAINT soline_prod_variant_id_fk FOREIGN KEY (prod_var_id) REFERENCES public.prod_variant(id) ON DELETE CASCADE;


--
-- Name: soline soline_uom_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline
    ADD CONSTRAINT soline_uom_id_fk FOREIGN KEY (uom_id) REFERENCES public.uom(id);


--
-- Name: soline soline_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.soline
    ADD CONSTRAINT soline_user_id_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON DELETE SET NULL;


--
-- Name: uom uom_comp_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: wmss
--

ALTER TABLE ONLY public.uom
    ADD CONSTRAINT uom_comp_id_fk FOREIGN KEY (comp_id) REFERENCES public.comp(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

