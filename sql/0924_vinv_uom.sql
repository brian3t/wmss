alter table uom
    add `desc` varchar(64) default null null;

create or replace definer = wmss@`%` view wmss.vinv as
select `wmss`.`inv`.`id`                                                         AS `id`,
       `wmss`.`inv`.`prod_variant_id`                                            AS `prod_variant_id`,
       `wmss`.`inv`.`uom_id`                                                     AS `uom_id`,
       `wmss`.`inv`.`qty`                                                        AS `qty`,
       `wmss`.`inv`.`bin_id`                                                     AS `bin_id`,
       `wmss`.`inv`.`created_at`                                                 AS `created_at`,
       `wmss`.`inv`.`updated_at`                                                 AS `updated_at`,
       `wmss`.`inv`.`created_by`                                                 AS `created_by`,
       `wmss`.`inv`.`updated_by`                                                 AS `updated_by`,
       `wmss`.`inv`.`note`                                                       AS `note`,
       `wmss`.`inv`.`noted_by`                                                   AS `noted_by`,
       concat(`wmss`.`uom`.`name`, ' ', `pv`.`name`, ' @ ', `wmss`.`bin`.`name`) AS `desc`
from (((`wmss`.`inv` join `wmss`.`prod_variant` `pv`
        on ((`pv`.`id` = `wmss`.`inv`.`prod_variant_id`))) join `wmss`.`bin`
       on ((`wmss`.`bin`.`id` = `wmss`.`inv`.`bin_id`))) join `wmss`.`uom`
      on ((`wmss`.`uom`.`id` = `wmss`.`inv`.`uom_id`)));

