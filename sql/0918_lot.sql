create table if not exists wmss.lot
(
    id            int auto_increment
        primary key,
    prod_id       int                                not null,
    num           varchar(60)                        not null,
    comp_id       int                                not null,
    exp_date      datetime                           null,
    mfg_date      datetime                           null,
    hold          smallint default 0                 null,
    active        smallint default 1                 null,
    rotation_date datetime                           null,
    created_at    datetime default CURRENT_TIMESTAMP null,
    updated_at    datetime                           null on update CURRENT_TIMESTAMP,
    created_by    int                                null,
    updated_by    int                                null,
    constraint lot_comp_id_fk
        foreign key (comp_id) references wmss.comp (id),
    constraint lot_prod_id_fk
        foreign key (prod_id) references wmss.prod (id),
    constraint lot_user_id_fk
        foreign key (created_by) references wmss.user (id),
    constraint lot_user_id_fk2
        foreign key (updated_by) references wmss.user (id)
);

