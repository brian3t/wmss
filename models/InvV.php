<?php

namespace app\models;

use \app\models\base\Inv as BaseInv;
use \soc\yii2helper\models\ModelB3tTrait;

/**
 * This is the model class for table "inv".
 */
class InvV extends BaseInv
{
  use ModelB3tTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'vinv';
  }
}
