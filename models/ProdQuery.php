<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Prod]].
 *
 * @see Prod
 */
class ProdQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Prod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Prod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}