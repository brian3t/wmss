<?php

namespace app\models;

use \app\models\base\VariantVal as BaseVariantVal;

/**
 * This is the model class for table "variant_val".
 */
class VariantVal extends BaseVariantVal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['variant_id'], 'required'],
            [['variant_id', 'value_int', 'created_by', 'updated_by'], 'integer'],
            [['value_dtm', 'created_at', 'updated_at'], 'safe'],
            [['value_float'], 'number'],
            [['value_str', 'note'], 'string', 'max' => 512],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
