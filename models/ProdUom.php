<?php

namespace app\models;

use \app\models\base\ProdUom as BaseProdUom;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "prod_uom".
 */
class ProdUom extends BaseProdUom
{
  /**
   * @inheritdoc
   * @throws \yii\base\InvalidConfigException
   */
    static public function findWr(): ActiveQuery{
         $query = Yii::createObject(ActiveQuery::class, [get_called_class()]);
         $query->innerJoin('uom','prod_uom.uom_id = uom.id');
         $query->innerJoin('prod','prod_uom.prod_id = prod.id');
         $query->select(['prod_uom.*','uom.name as uom_name','prod.name as prod_name']);
         return $query;
    }

}
