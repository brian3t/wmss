<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "prod_uom".
 *
 * @property integer $id
 * @property integer $prod_id
 * @property integer $uom_id
 * @property integer $comp_id
 * @property string $cnv_to_stock
 * @property integer $decimals_ovrd
 * @property string $weight
 * @property string $volume
 * @property string $length
 * @property string $width
 * @property string $height
 * @property string $upc
 * @property integer $shippable_pack_series_id
 * @property integer $enabled
 * @property integer $lot_specific_cnv
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\Comp $company
 * @property \app\models\Prod $prod
 * @property \app\models\Uom $uom
 */
class ProdUom extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['prod_id', 'uom_id', 'comp_id'], 'required'],
      [['prod_id', 'uom_id', 'comp_id', 'shippable_pack_series_id', 'lot_specific_cnv'], 'integer'],
      [['cnv_to_stock', 'weight', 'volume', 'length', 'width', 'height'], 'number'],
      [['decimals_ovrd'], 'string'],
      [['created_at', 'updated_at'], 'safe'],
      [['upc'], 'string', 'max' => 64],
      [['enabled'], 'string', 'max' => 1]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'prod_uom';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'prod_id' => 'Product ID',
      'uom_id' => 'Uom ID',
      'comp_id' => 'Company ID',
      'cnv_to_stock' => 'Cnv To Stock',
      'decimals_ovrd' => 'Decimals Ovrd',
      'weight' => 'Weight',
      'volume' => 'Volume',
      'length' => 'Length',
      'width' => 'Width',
      'height' => 'Height',
      'upc' => 'Upc',
      'shippable_pack_series_id' => 'Shippable Pack Series ID',
      'enabled' => 'Enabled',
      'lot_specific_cnv' => 'Lot Specific Cnv',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompany() {
    return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id'])->inverseOf('prodUoms');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProd() {
    return $this->hasOne(\app\models\Prod::className(), ['id' => 'prod_id'])->inverseOf('prodUoms');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUom() {
    return $this->hasOne(\app\models\Uom::className(), ['id' => 'uom_id'])->inverseOf('prodUoms');
  }

}
