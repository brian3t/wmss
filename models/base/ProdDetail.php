<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "prod_detail".
 *
 * @property integer $id
 * @property integer $prod_variant_id
 * @property integer $variant_val_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $note
 *
 * @property \app\models\ProdVariant $prodVariant
 * @property \app\models\VariantVal $variantVal
 */
class ProdDetail extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_variant_id', 'variant_val_id'], 'required'],
            [['prod_variant_id', 'variant_val_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prod_detail';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_variant_id' => 'Product Variant ID',
            'variant_val_id' => 'Variant Val ID',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdVariant()
    {
        return $this->hasOne(\app\models\ProdVariant::className(), ['id' => 'prod_variant_id'])->inverseOf('prodDetails');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariantVal()
    {
        return $this->hasOne(\app\models\VariantVal::className(), ['id' => 'variant_val_id'])->inverseOf('prodDetails');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
