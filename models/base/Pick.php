<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use \soc\yiiuser\User\Model\User;

/**
 * This is the base model class for table "pick".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $comp_id
 * @property integer $whse_id
 * @property integer $name
 * @property string $tran_cat
 * @property string $sub_cat
 * @property integer $ok_to_post
 * @property integer $is_complete
 * @property integer $so_id
 * @property integer $is_released
 * @property integer $rpt_group_id
 * @property integer $pick_rule_id
 * @property string $bol_notes
 * @property string $notes
 * @property integer $is_bol_printed
 * @property integer $shipmeth_id
 * @property integer $is_expedite
 *
 * @property \app\models\Comp $comp
 * @property \app\models\So $so
 * @property \soc\yiiuser\User\Model\User $createdBy
 * @property \soc\yiiuser\User\Model\User $updatedBy
 */
class Pick extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'comp_id'], 'required'],
            [['created_by', 'updated_by', 'comp_id', 'whse_id', 'name', 'so_id', 'rpt_group_id', 'pick_rule_id', 'shipmeth_id'], 'integer'],
            [['bol_notes', 'notes'], 'string'],
            [['tran_cat', 'sub_cat'], 'string', 'max' => 6],
            [['ok_to_post', 'is_complete', 'is_released', 'is_bol_printed', 'is_expedite'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pick';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comp_id' => 'Company',
            'whse_id' => 'Warehouse',
            'name' => 'Pick No.',
            'tran_cat' => 'Transaction Category',
            'sub_cat' => 'Sub Category',
            'ok_to_post' => 'OK To Post',
            'is_complete' => 'Completed',
            'so_id' => 'Sales Order ID',
            'is_released' => 'Released',
            'rpt_group_id' => 'Report Group ID',
            'pick_rule_id' => 'Pick Rule ID',
            'bol_notes' => 'BOL Notes',
            'notes' => 'Notes',
            'is_bol_printed' => 'BOL Printed',
            'shipmeth_id' => 'Ship method ID',
            'is_expedite' => 'Expedite',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id']);
    }


   /**
    * @return \yii\db\ActiveQuery
    */
   public function getPicklines()
   {
       return $this->hasMany(\app\models\Pickline::className(), ['pick_id' => 'id']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSo()
    {
        return $this->hasOne(\app\models\So::class, ['id' => 'so_id'])->inverseOf('picks');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->inverseOf('picks');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by'])->inverseOf('picks');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
