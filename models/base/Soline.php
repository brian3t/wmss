<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "soline".
 *
 * @property integer $id
 * @property string $so
 * @property string $company
 * @property string $created_at
 * @property string $updated_at
 * @property string $customer
 * @property integer $cust_id
 * @property string $lineid
 * @property integer $seq
 * @property integer $void
 * @property integer $erp_id
 * @property string $erp_soline
 * @property string $wh
 * @property string $prod
 * @property string $description
 * @property integer $non_inv
 * @property integer $qty_ord
 * @property integer $qty_shipped
 * @property integer $ok_to_pick
 * @property string $pickrule
 * @property string $uom
 * @property string $cnv_to_stk
 * @property integer $erp_so_id
 * @property string $erp_so
 * @property string $req_ship_date
 * @property string $promise_date
 * @property string $shipmeth
 * @property integer $ship_priority
 * @property string $ship_to
 * @property string $ship_contact
 * @property string $ship_email
 * @property string $addr1
 * @property string $addr2
 * @property string $addr3
 * @property string $addr4
 * @property string $city
 * @property string $state
 * @property string $postal
 * @property string $country_code
 * @property string $phone1
 * @property string $phone2
 * @property string $cancel_date
 * @property string $additional_info
 * @property string $extra_field_1
 * @property string $extra_field_2
 * @property string $extra_field_3
 * @property integer $uom_id
 * @property integer $so_id
 * @property string $addr_hash
 * @property string $wt_per
 * @property string $comment
 * @property integer $decimals
 * @property string $unit_price
 * @property string $ext_price
 * @property integer $is_backorder
 * @property integer $comp_id
 * @property integer $wh_id
 * @property integer $created_by
 *
 * @property \app\models\Comp $comp
 * @property \app\models\Prod $prod0
 * @property \app\models\So $so0
 * @property \app\models\Uom $uom0
 * @property \app\models\User $createdBy
 * @property string $prod_var [varchar(128)]
 * @property int $prod_var_id [int]
 * @property int $erp_qty_open_to_ship [mediumint]
 * @property bool $sync_stat [tinyint]
 * @property string $sync_msg [varchar(511)]  Sync Message
 * @property string $last_synced [datetime]  Last Synced At
 */
class Soline extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['so', 'company', 'prod_var', 'qty_ord', 'uom'], 'required'],
            [['created_at', 'updated_at', 'req_ship_date', 'promise_date', 'cancel_date'], 'safe'],
            [['cust_id', 'seq', 'erp_id', 'qty_ord', 'qty_shipped', 'erp_qty_open_to_ship', 'erp_so_id', 'uom_id', 'so_id', 'comp_id', 'wh_id', 'created_by'], 'integer'],
            [['cnv_to_stk', 'wt_per', 'unit_price', 'ext_price'], 'number'],
            [['ship_priority', 'decimals'], 'string'],
            [['so', 'customer', 'pickrule', 'uom', 'shipmeth', 'postal', 'phone1', 'phone2'], 'string', 'max' => 64],
            [['company'], 'string', 'max' => 12],
            [['lineid', 'erp_soline', 'erp_so', 'state'], 'string', 'max' => 128],
            [['void', 'non_inv', 'ok_to_pick', 'is_backorder'], 'string', 'max' => 1],
            [['wh', 'extra_field_1', 'extra_field_2', 'extra_field_3'], 'string', 'max' => 31],
            [['description'], 'string', 'max' => 512],
            [['ship_to', 'ship_contact', 'ship_email', 'city'], 'string', 'max' => 255],
            [['addr1', 'addr2', 'addr3', 'addr4'], 'string', 'max' => 511],
            [['country_code'], 'string', 'max' => 2],
            [['additional_info', 'comment'], 'string', 'max' => 2047],
            [['addr_hash'], 'string', 'max' => 2500]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vsoline';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'so' => 'So',
            'company' => 'Company',
            'customer' => 'Customer',
            'cust_id' => 'Customer ID',
            'lineid' => 'Line ID',
            'seq' => 'Sequence',
            'void' => 'Void',
            'erp_id' => 'Erp ID',
            'erp_soline' => 'Erp SO Line',
            'wh' => 'Warehouse',
            'prod_var' => 'Product Variant',
            'description' => 'Description',
            'non_inv' => 'Non Inventory',
            'qty_ord' => 'Qty Ordered',
            'qty_shipped' => 'Qty Shipped',
            'erp_qty_open_to_ship' => 'ERP Qty Open To Ship',
            'ok_to_pick' => 'Ok To Pick',
            'pickrule' => 'Pick Rule',
            'uom' => 'Uom',
            'cnv_to_stk' => 'Cnv To Stk',
            'erp_so_id' => 'ERP SO ID',
            'erp_so' => 'ERP SO',
            'req_ship_date' => 'Requested Ship Date',
            'promise_date' => 'Promise Date',
            'shipmeth' => 'Ship Method',
            'ship_priority' => 'Ship Priority',
            'ship_to' => 'Ship To',
            'ship_contact' => 'Ship Contact',
            'ship_email' => 'Ship Email',
            'addr1' => 'Addr1',
            'addr2' => 'Addr2',
            'addr3' => 'Addr3',
            'addr4' => 'Addr4',
            'city' => 'City',
            'state' => 'State',
            'postal' => 'Postal',
            'country_code' => 'Country Code',
            'phone1' => 'Phone1',
            'phone2' => 'Phone2',
            'cancel_date' => 'Cancel Date',
            'additional_info' => 'Additional Info',
            'extra_field_1' => 'Extra Field 1',
            'extra_field_2' => 'Extra Field 2',
            'extra_field_3' => 'Extra Field 3',
            'uom_id' => 'Uom ID',
            'so_id' => 'Sales Order ID',
            'addr_hash' => 'Addr Hash',
            'wt_per' => 'Weight Per',
            'comment' => 'Comment',
            'decimals' => 'Decimals',
            'unit_price' => 'Unit Price',
            'ext_price' => 'Ext Price',
            'is_backorder' => 'Is Backorder',
            'comp_id' => 'Company ID',
            'prod_var_id' => 'Product Variant ID',
            'wh_id' => 'Warehouse ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdVar()
    {
        return $this->hasOne(\app\models\ProdVariant::class, ['id' => 'prod_var_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSo0()
    {
        return $this->hasOne(\app\models\So::className(), ['id' => 'so_id'])->inverseOf('solines');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUom0()
    {
        return $this->hasOne(\app\models\Uom::className(), ['id' => 'uom_id']);
    }

   /**
   * @return \yii\db\ActiveQuery
   */
  public function getVSolinePickline(): \yii\db\ActiveQuery {
    return $this->findBySql('SELECT * FROM vsoline_pickline WHERE id = :id', [':id' => $this->id]);
  }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\soc\yiiuser\User\Model\User::class, ['id' => 'created_by']);
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    /*public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }*/
}
