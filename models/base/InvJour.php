<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv_jour".
 *
 * @property integer $id
 * @property string $created_at
 * @property integer $created_by
 * @property string $inv_old
 * @property string $inv_new
 * @property string $note
 */
class InvJour extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['created_by'], 'integer'],
            [['inv_old', 'inv_new'], 'required'],
            [['inv_old', 'inv_new'], 'string', 'max' => 2048],
            [['note'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_jour';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inv_old' => 'Inv Old',
            'inv_new' => 'Inv New',
            'note' => 'Note',
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
            ],
        ];
    }
}
