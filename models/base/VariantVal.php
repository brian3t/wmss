<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "variant_val".
 *
 * @property integer $id
 * @property integer $variant_id
 * @property integer $value_int
 * @property string $value_str
 * @property string $value_dtm
 * @property string $value_float
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $note
 *
 * @property \app\models\ProdDetail[] $prodDetails
 * @property \app\models\Variant $variant
 */
class VariantVal extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['variant_id'], 'required'],
            [['variant_id', 'value_int', 'created_by', 'updated_by'], 'integer'],
            [['value_dtm', 'created_at', 'updated_at'], 'safe'],
            [['value_float'], 'number'],
            [['value_str', 'note'], 'string', 'max' => 512],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variant_val';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'variant_id' => 'Variant ID',
            'value_int' => 'Value Int',
            'value_str' => 'Value Str',
            'value_dtm' => 'Value Dtm',
            'value_float' => 'Value Float',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdDetails()
    {
        return $this->hasMany(\app\models\ProdDetail::className(), ['variant_val_id' => 'id']);//->inverseOf('variantVal');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariant()
    {
        return $this->hasOne(\app\models\Variant::className(), ['id' => 'variant_id']);//->inverseOf('variantVals');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }
}
