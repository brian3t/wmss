<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "so".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $comp_id
 * @property string $name Sales Order Number
 * @property integer $wh_id
 * @property integer $void
 * @property string $custid
 * @property string $ord_type
 * @property string $cust_desc
 * @property string $description
 * @property string $pick_instructions
 * @property integer $ok_to_pick
 * @property string $currency
 * @property integer $erp_id
 * @property string $erp_order
 * @property integer $ship_complete
 * @property integer $hold
 * @property string $hold_rsn
 * @property string $ship_meth
 * @property string $more_attr
 * @property string $cust_po
 * @property integer $close_on_first_ship
 * @property string $instructions
 * @property integer $erp_key
 * @property string $bill_to
 * @property string $bill_contact
 * @property string $bill_email
 * @property string $bill_addr1
 * @property string $bill_addr2
 * @property string $bill_addr3
 * @property string $bill_addr4
 * @property string $bill_city
 * @property string $bill_state
 * @property string $bill_country
 * @property string $bill_postal
 * @property string $bill_phone
 * @property string $request_date
 * @property string $promise_date
 * @property string $close_date
 * @property string $ship_date
 * @property integer $carrier_id
 * @property string $bol_notes
 * @property string $sched_arrival
 * @property integer $is_expedite
 * @property integer $pickrule_id
 * @property integer $shipmeth_id
 * @property string $ship_to
 * @property string $ship_contact
 * @property string $ship_email
 * @property string $ship_phone
 * @property string $ship_addr1
 * @property string $ship_addr2
 * @property string $ship_addr3
 * @property string $ship_addr4
 * @property string $ship_city
 * @property string $ship_state
 * @property string $ship_postal
 * @property string $ship_country
 * @property integer $cust_addr_id
 * @property string $total_amt
 * @property string $extra_field_1
 * @property string $extra_field_2
 * @property string $extra_field_3
 *
 * @property \app\models\Comp $comp
 * @property \app\models\Pick[] $picks
 * @property \app\models\Soline[] $solines
 * @property \yii\db\ActiveQuery $solinesQ
 */
class So extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'request_date', 'promise_date', 'close_date', 'ship_date', 'sched_arrival'], 'safe'],
            [['updated_by', 'comp_id', 'wh_id', 'erp_id', 'erp_key', 'carrier_id', 'pickrule_id', 'shipmeth_id', 'cust_addr_id'], 'integer'],
            [['comp_id', 'name'], 'required'],
            [['more_attr'], 'string'],
            [['total_amt'], 'number'],
            [['name', 'custid', 'erp_order', 'hold_rsn', 'ship_meth', 'cust_po'], 'string', 'max' => 64],
            [['void', 'hold', 'close_on_first_ship', 'is_expedite'], 'number', 'max' => 1],
            [['ord_type'], 'string', 'max' => 16],
            [['cust_desc', 'description'], 'string', 'max' => 512],
            [['pick_instructions'], 'string', 'max' => 2048],
            [['currency'], 'string', 'max' => 32],
            [['instructions', 'bol_notes'], 'string', 'max' => 2047],
            [['bill_to', 'bill_contact', 'bill_email', 'bill_city', 'ship_to', 'ship_contact', 'ship_email', 'ship_city'], 'string', 'max' => 127],
            [['bill_addr1', 'bill_addr2', 'bill_addr3', 'bill_addr4', 'ship_addr1', 'ship_addr2', 'ship_addr3', 'ship_addr4'], 'string', 'max' => 255],
            [['bill_state', 'bill_postal', 'bill_phone', 'ship_phone', 'ship_state', 'ship_postal', 'ship_country'], 'string', 'max' => 63],
            [['bill_country'], 'string', 'max' => 2],
            [['extra_field_1', 'extra_field_2', 'extra_field_3'], 'string', 'max' => 31],
            [['comp_id', 'name'], 'unique', 'targetAttribute' => ['comp_id', 'name'], 'message' => 'The combination of Comp ID and Name has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'so';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comp_id' => 'Comp ID',
            'name' => 'SO Number',
            'wh_id' => 'Wh ID',
            'void' => 'Void',
            'custid' => 'Custid',
            'ord_type' => 'Ord Type',
            'cust_desc' => 'Cust Desc',
            'description' => 'Description',
            'pick_instructions' => 'Pick Instructions',
            'ok_to_pick' => 'OK To Pick',
            'currency' => 'Currency',
            'erp_id' => 'Erp ID',
            'erp_order' => 'Erp Order',
            'ship_complete' => 'Ship Complete',
            'hold' => 'Hold',
            'hold_rsn' => 'Hold Reason',
            'ship_meth' => 'Ship Method',
            'more_attr' => 'More Attr',
            'cust_po' => 'Cust Po',
            'close_on_first_ship' => 'Close On First Ship',
            'instructions' => 'Instructions',
            'erp_key' => 'Erp Key',
            'bill_to' => 'Bill To',
            'bill_contact' => 'Bill Contact',
            'bill_email' => 'Bill Email',
            'bill_addr1' => 'Bill Addr1',
            'bill_addr2' => 'Bill Addr2',
            'bill_addr3' => 'Bill Addr3',
            'bill_addr4' => 'Bill Addr4',
            'bill_city' => 'Bill City',
            'bill_state' => 'Bill State',
            'bill_country' => 'Bill Country',
            'bill_postal' => 'Bill Postal',
            'bill_phone' => 'Bill Phone',
            'request_date' => 'Request Date',
            'promise_date' => 'Promise Date',
            'close_date' => 'Close Date',
            'ship_date' => 'Ship Date',
            'carrier_id' => 'Carrier ID',
            'bol_notes' => 'Bol Notes',
            'sched_arrival' => 'Sched Arrival',
            'is_expedite' => 'Is Expedite',
            'pickrule_id' => 'Pickrule ID',
            'shipmeth_id' => 'Shipmeth ID',
            'ship_to' => 'Ship To',
            'ship_contact' => 'Ship Contact',
            'ship_email' => 'Ship Email',
            'ship_phone' => 'Ship Phone',
            'ship_addr1' => 'Ship Addr1',
            'ship_addr2' => 'Ship Addr2',
            'ship_addr3' => 'Ship Addr3',
            'ship_addr4' => 'Ship Addr4',
            'ship_city' => 'Ship City',
            'ship_state' => 'Ship State',
            'ship_postal' => 'Ship Postal',
            'ship_country' => 'Ship Country',
            'cust_addr_id' => 'Cust Addr ID',
            'total_amt' => 'Total Amt',
            'extra_field_1' => 'Extra Field 1',
            'extra_field_2' => 'Extra Field 2',
            'extra_field_3' => 'Extra Field 3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id']);
    }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getPicks()
   {
       return $this->hasMany(\app\models\Pick::class, ['so_id' => 'id'])->inverseOf('so');
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getSolines()
   {
       return $this->hasMany(\app\models\Soline::class, ['so_id' => 'id'])->inverseOf('so0');
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getSolinesQ()
   {
       return $this->hasMany(\app\models\Soline::class, ['so_id' => 'id']);
   }

   /**
   * @return \yii\db\ActiveQuery
   */
  public function getVSolinePickline(): \yii\db\ActiveQuery {
    return $this->findBySql('SELECT * FROM vsoline_pickline WHERE so_id = :so_id', [':so_id' => $this->id]);
  }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
