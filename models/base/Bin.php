<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "bin".
 *
 * @property integer $id
 * @property integer $comp_id
 * @property string $name
 * @property integer $zone_id
 * @property string $description
 * @property integer $active
 * @property integer $excl_fr_avail
 * @property integer $excl_fr_fcast
 * @property integer $allow_neg_inv
 * @property integer $seq
 * @property integer $erp_int
 * @property integer $excl_rec
 * @property integer $excl_ship
 * @property string $erp_loc
 * @property string $pallet_spaces
 * @property integer $primary_zone_id
 * @property integer $excl_from_recon
 * @property integer $reverse_recon
 * @property integer $reverse_recon_rsnkey
 * @property integer $excl_cont
 * @property integer $allow_ship_stage
 * @property string $perm_move_in
 * @property string $perm_move_out
 * @property integer $disallow_hold_inv
 * @property string $height_level
 * @property integer $row
 * @property integer $col
 * @property integer $is_item_restricted
 * @property string $loc_positions
 * @property integer $is_shipping_lane
 * @property integer $is_consolidation_area
 * @property integer $is_pack_area
 * @property integer $auto_unload_cont
 * @property integer $excl_count
 * @property integer $require_cont
 * @property integer $is_single_item_only
 * @property integer $is_pick_area
 * @property integer $pickgroup_id
 * @property string $length
 * @property string $width
 * @property string $height
 * @property integer $flagged_for_count
 * @property integer $pickroute_id
 * @property string $rank
 * @property string $max_wt
 * @property integer $flagged_for_putaway
 * @property integer $is_putaway_hold
 * @property string $putaway_hold_comment
 * @property integer $is_pick_hold
 * @property string $pick_hold_comment
 * @property string $latitude
 * @property string $longitude
 * @property string $last_count_date
 * @property integer $floor
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Comp $company
 * @property \app\models\Inv[] $invs
 */
class Bin extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id', 'name'], 'required'],
            [['comp_id', 'zone_id', 'seq', 'erp_int', 'primary_zone_id', 'reverse_recon_rsnkey', 'row', 'col', 'pickgroup_id', 'pickroute_id', 'created_by', 'updated_by'], 'integer'],
            [['pallet_spaces', 'height_level', 'loc_positions', 'length', 'width', 'height', 'max_wt', 'latitude', 'longitude'], 'number'],
            [['last_count_date', 'created_at', 'updated_at'], 'safe'],
            [['floor'], 'string'],
            [['name', 'erp_loc', 'perm_move_in', 'perm_move_out'], 'string', 'max' => 64],
            [['description', 'putaway_hold_comment', 'pick_hold_comment'], 'string', 'max' => 255],
            [['active', 'excl_fr_avail', 'excl_fr_fcast', 'allow_neg_inv', 'excl_rec', 'excl_ship', 'excl_from_recon', 'reverse_recon', 'excl_cont', 'allow_ship_stage', 'disallow_hold_inv', 'is_item_restricted', 'is_shipping_lane', 'is_consolidation_area', 'is_pack_area', 'auto_unload_cont', 'excl_count', 'require_cont', 'is_single_item_only', 'is_pick_area', 'flagged_for_count', 'flagged_for_putaway', 'is_putaway_hold', 'is_pick_hold'], 'string', 'max' => 1],
            [['rank'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bin';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comp_id' => 'Company ID',
            'name' => 'Name',
            'zone_id' => 'Zone ID',
            'description' => 'Description',
            'active' => 'Active',
            'excl_fr_avail' => 'Exclude From Avail',
            'excl_fr_fcast' => 'Exclude From Forecast',
            'allow_neg_inv' => 'Allow Neg Inv',
            'seq' => 'Seq',
            'erp_int' => 'Erp Int',
            'excl_rec' => 'Exclude Rec',
            'excl_ship' => 'Exclude Ship',
            'erp_loc' => 'Erp Loc',
            'pallet_spaces' => 'Pallet Spaces',
            'primary_zone_id' => 'Primary Zone ID',
            'excl_from_recon' => 'Exclude From Recon',
            'reverse_recon' => 'Reverse Recon',
            'reverse_recon_rsnkey' => 'Reverse Recon Rsnkey',
            'excl_cont' => 'Exclude Cont',
            'allow_ship_stage' => 'Allow Ship Stage',
            'perm_move_in' => 'Perm Move In',
            'perm_move_out' => 'Perm Move Out',
            'disallow_hold_inv' => 'Disallow Hold Inv',
            'height_level' => 'Height Level',
            'row' => 'Row',
            'col' => 'Col',
            'is_item_restricted' => 'Is Item Restricted',
            'loc_positions' => 'Loc Positions',
            'is_shipping_lane' => 'Is Shipping Lane',
            'is_consolidation_area' => 'Is Consolidation Area',
            'is_pack_area' => 'Is Pack Area',
            'auto_unload_cont' => 'Auto Unload Cont',
            'excl_count' => 'Exclude Count',
            'require_cont' => 'Require Cont',
            'is_single_item_only' => 'Is Single Item Only',
            'is_pick_area' => 'Is Pick Area',
            'pickgroup_id' => 'Pickgroup ID',
            'length' => 'Length',
            'width' => 'Width',
            'height' => 'Height',
            'flagged_for_count' => 'Flagged For Count',
            'pickroute_id' => 'Pickroute ID',
            'rank' => 'Rank',
            'max_wt' => 'Max Wt',
            'flagged_for_putaway' => 'Flagged For Putaway',
            'is_putaway_hold' => 'Is Putaway Hold',
            'putaway_hold_comment' => 'Putaway Hold Comment',
            'is_pick_hold' => 'Is Pick Hold',
            'pick_hold_comment' => 'Pick Hold Comment',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'last_count_date' => 'Last Count Date',
            'floor' => 'Floor',
        ];
    }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getCompany()
   {
       return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id'])->inverseOf('bins');
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvs()
    {
        return $this->hasMany(\app\models\Inv::className(), ['bin_id' => 'id'])->inverseOf('bin');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
