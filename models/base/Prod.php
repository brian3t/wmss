<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "prod".
 *
 * @property integer $id
 * @property integer $comp_id
 * @property string $code
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $active
 * @property string $ext_code
 * @property integer $ext_id
 * @property string $ext_upd_at
 * @property string $description
 * @property string $long_desc
 * @property string $rank
 * @property bool $is_inv
 * @property integer $decimals
 * @property integer $stock_uom_id
 * @property integer $pallet_uom_id
 * @property integer $case_uom_id
 * @property integer $mc_uom_id
 * @property integer $std_qty_per_pall
 * @property integer $qty_per_case
 * @property integer $wave_max_qty
 *
 * @property \app\models\Comp $company
 * @property \app\models\ProdUom[] $prodUoms
 * @property \app\models\ProdVariant[] $prodVariants
 */
class Prod extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['comp_id', 'created_by', 'updated_by', 'ext_id', 'stock_uom_id', 'pallet_uom_id', 'case_uom_id', 'mc_uom_id', 'std_qty_per_pall', 'qty_per_case', 'wave_max_qty'], 'integer'],
      [['code', 'name'], 'required'],
      [['created_at', 'updated_at', 'ext_upd_at'], 'safe'],
      [['decimals'], 'string'],
      [['code'], 'string', 'max' => 32],
      [['name'], 'string', 'max' => 128],
      [['active'], 'string', 'max' => 1],
      [['ext_code'], 'string', 'max' => 80],
      [['description'], 'string', 'max' => 255],
      [['long_desc'], 'string', 'max' => 510],
      [['rank'], 'string', 'max' => 2],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'prod';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'comp_id' => 'Company',
      'code' => 'Product Code',
      'name' => 'Product Name',
      'active' => 'Active',
      'ext_code' => 'Ext Code',
      'ext_id' => 'Ext ID',
      'ext_upd_at' => 'Ext Upd At',
      'description' => 'Description',
      'long_desc' => 'Long Desc',
      'rank' => 'Rank',
      'is_inv' => 'Is Inventory',
      'decimals' => 'Decimals',
      'stock_uom_id' => 'Stock Uom ID',
      'pallet_uom_id' => 'Pallet Uom ID',
      'case_uom_id' => 'Case Uom ID',
      'mc_uom_id' => 'Mc Uom ID',
      'std_qty_per_pall' => 'Std Qty Per Pall',
      'qty_per_case' => 'Qty Per Case',
      'wave_max_qty' => 'Wave Max Qty',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompany() {
    return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id'])->inverseOf('prods');
  }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getProdUoms()
   {
       return $this->hasMany(\app\models\ProdUom::className(), ['prod_id' => 'id'])->inverseOf('prod');
   }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProdVariants() {
    return $this->hasMany(\app\models\ProdVariant::className(), ['prod_id' => 'id'])->inverseOf('prod');
  }

  /**
   * @inheritdoc
   * @return array mixed
   */
  public function behaviors() {
    return [
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
   * @inheritdoc
   * @return \app\models\ProdQuery the active query used by this AR class.
   */
  public static function find() {
    return new \app\models\ProdQuery(get_called_class());
  }
}
