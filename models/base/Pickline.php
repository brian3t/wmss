<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use soc\yiiuser\User\Model\User;

/**
 * This is the base model class for table "pickline".
 *
 * @property integer $id
 * @property integer $pick_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $seq
 * @property integer $assigned_to
 * @property integer $prod_var_id
 * @property integer $soline_id
 * @property string $description
 * @property integer $wh_id
 * @property integer $bin_id
 * @property integer $zone_id
 * @property string $qty_to_pick
 * @property string $orig_qty_to_pick
 * @property integer $prod_uom_id
 * @property string $min_qty_to_pick
 * @property string $max_qty_to_pick
 * @property integer $decimals
 * @property integer $cust_id
 * @property integer $cust_addr_id
 * @property string $completed_at
 * @property integer $ship_addr_id
 * @property integer $pick_completed_by
 * @property string $note
 *
 * @property \app\models\Pickdet[] $pickdets
 * @property \app\models\Bin $bin
 * @property \app\models\Pick $pick
 * @property \app\models\ProdUom $prodUom
 * @property \app\models\ProdVariant $prodVar
 * @property \app\models\Soline $soline
 * @property \app\models\User $createdBy
 * @property \app\models\User $updatedBy
 * @property \app\models\User $assignedTo
 * @property \app\models\User $pickCompletedBy
 * @property \yii\db\ActiveQuery $vSolinePickline
 */
class Pickline extends \soc\yii2helper\db\ActiveRecordSc
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['pick_id', 'prod_var_id', 'prod_uom_id'], 'required'],
      [['pick_id', 'created_by', 'updated_by', 'seq', 'assigned_to', 'prod_var_id', 'wh_id', 'bin_id', 'zone_id', 'prod_uom_id', 'cust_id', 'cust_addr_id', 'ship_addr_id', 'pick_completed_by'], 'integer'],
      [['created_at', 'updated_at', 'completed_at', 'soline_id'], 'safe'],
      [['qty_to_pick', 'orig_qty_to_pick', 'min_qty_to_pick', 'max_qty_to_pick'], 'number'],
      [['decimals'], 'string'],
      [['description'], 'string', 'max' => 255],
      [['note'], 'string', 'max' => 1023]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'pickline';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'pick_id' => 'Pick',
      'seq' => 'Seq',
      'assigned_to' => 'Assigned To',
      'prod_var_id' => 'Product Variant ID',
     'soline_id' => 'SO line ID',
      'description' => 'Description',
      'wh_id' => 'Warehouse ID',
      'bin_id' => 'Bin ID',
      'zone_id' => 'Zone ID',
      'qty_to_pick' => 'Qty To Pick',
      'orig_qty_to_pick' => 'Orig Qty To Pick',
      'prod_uom_id' => 'Product UOM ID',
      'min_qty_to_pick' => 'Min Qty To Pick',
      'max_qty_to_pick' => 'Max Qty To Pick',
      'decimals' => 'Decimals',
      'cust_id' => 'Customer ID',
      'cust_addr_id' => 'Customer Addr ID',
      'completed_at' => 'Completed At',
      'ship_addr_id' => 'Ship Address ID',
      'pick_completed_by' => 'Pick Completed By',
      'note' => 'Note',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBin() {
    return $this->hasOne(\app\models\Bin::className(), ['id' => 'bin_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPick() {
    return $this->hasOne(\app\models\Pick::className(), ['id' => 'pick_id'])->inverseOf('picklines');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
   public function getPickdets() {
       return $this->hasMany(\app\models\Pickdet::class, ['pickline_id' => 'id'])->inverseOf('pickline');
   }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProdVar() {
    return $this->hasOne(\app\models\ProdVariant::className(), ['id' => 'prod_var_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProdUom() {
    return $this->hasOne(\app\models\ProdUom::className(), ['id' => 'prod_uom_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedBy() {
    return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedBy() {
    return $this->hasOne(\app\models\User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAssignedTo() {
    return $this->hasOne(\app\models\User::className(), ['id' => 'assigned_to']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPickCompletedBy() {
    return $this->hasOne(\app\models\User::className(), ['id' => 'pick_completed_by']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSoline() {
    return $this->hasOne(\app\models\Soline::class, ['id' => 'soline_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getVSolinePickline(): \yii\db\ActiveQuery {
    return $this->findBySql('SELECT * FROM vsoline_pickline WHERE id = :id', [':id' => $this->soline_id]);
  }

  /**
   * @inheritdoc
   * @return array mixed
   */
  public function behaviors() {
    return [
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

/*    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        // TODO: Implement toArray() method.
    }

    public static function instance($refresh = false)
    {
        // TODO: Implement instance() method.
    }*/
}
