<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "comp".
 *
 * @property integer $id
 * @property string $name
 * @property string $descr
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \app\models\Bin[] $bins
 * @property \app\models\Prod[] $prods
 * @property \app\models\ProdUom[] $prodUoms
 * @property \app\models\Uom[] $uoms
 */
class Comp extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['name'], 'string', 'max' => 12],
            [['descr'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comp';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'descr' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBins()
    {
        return $this->hasMany(\app\models\Bin::className(), ['comp_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProds()
    {
        return $this->hasMany(\app\models\Prod::className(), ['comp_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdUoms()
    {
        return $this->hasMany(\app\models\ProdUom::className(), ['comp_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUoms()
    {
        return $this->hasMany(\app\models\Uom::className(), ['comp_id' => 'id'])->inverseOf('company');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => false,
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
