<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "prod_variant".
 *
 * @property integer $id
 * @property string $name
 * @property integer $prod_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $sku
 * @property string $price
 * @property integer $lot_id
 * @property integer $ser_id
 * @property string $note
 *
 * @property \app\models\Inv[] $invs
 * @property \app\models\ProdDetail[] $prodDetails
 * @property \app\models\Prod $prod
 */
class ProdVariant extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'prod_id', 'sku'], 'required'],
            [['prod_id', 'created_by', 'updated_by', 'lot_id', 'ser_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['sku'], 'string', 'max' => 64],
            [['note'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prod_variant';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'prod_id' => 'Product ID',
            'sku' => 'SKU',
            'price' => 'Price',
            'lot_id' => 'Lot ID',
            'ser_id' => 'Ser ID',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvs()
    {
        return $this->hasMany(\app\models\Inv::className(), ['prod_variant_id' => 'id'])->inverseOf('prodVariant');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdDetails()
    {
        return $this->hasMany(\app\models\ProdDetail::className(), ['prod_variant_id' => 'id'])->inverseOf('prodVariant');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(\app\models\Prod::className(), ['id' => 'prod_id'])->inverseOf('prodVariants');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
