<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;
use soc\yiiuser\User\Model\User;
use soc\yii2helper\db\ActiveRecordSc;

/**
 * This is the base model class for table "pickdet".
 *
 * @property integer $id
 * @property integer $pickline_id
 * @property integer $seq
 * @property integer $lot_id
 * @property integer $ser_id
 * @property integer $cont_id
 * @property integer $inv_id
 * @property string $qty
 * @property integer $prod_uom_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $confirmed
 * @property string $confirmed_at
 * @property integer $confirmed_by
 * @property string $note
 *
 * @property \app\models\Inv $inv
 * @property \app\models\Pickline $pickline
 * @property \app\models\ProdUom $prodUom
 * @property \app\models\User $createdBy
 * @property \app\models\User $updatedBy
 * @property \app\models\User $confirmedBy
 */
class Pickdet extends ActiveRecordSc
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pickline_id', 'inv_id', 'prod_uom_id'], 'required'],
            [['pickline_id', 'seq', 'lot_id', 'ser_id', 'cont_id', 'inv_id', 'prod_uom_id', 'created_by', 'updated_by', 'confirmed_by'], 'integer'],
            [['qty'], 'number'],
            [['created_at', 'updated_at', 'confirmed_at'], 'safe'],
            [['confirmed'], 'string', 'max' => 1],
            [['note'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pickdet';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pickline_id' => 'Pickline ID',
            'seq' => 'Seq',
            'lot_id' => 'Lot ID',
            'ser_id' => 'Serial ID',
            'cont_id' => 'Cont ID',
            'inv_id' => 'Inv ID',
            'qty' => 'Qty',
            'prod_uom_id' => 'Prod UOM ID',
            'confirmed' => 'Confirmed',
            'confirmed_at' => 'Confirmed At',
            'confirmed_by' => 'Confirmed By',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInv()
    {
        return $this->hasOne(\app\models\Inv::className(), ['id' => 'inv_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPickline()
    {
        return $this->hasOne(\app\models\Pickline::className(), ['id' => 'pickline_id'])->inverseOf('pickdets');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdUom()
    {
        return $this->hasOne(\app\models\ProdUom::className(), ['id' => 'prod_uom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy()
    {
        return $this->hasOne(User::class, ['id' => 'confirmed_by']);
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
