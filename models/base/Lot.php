<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "lot".
 *
 * @property integer $id
 * @property integer $prod_id
 * @property string $num
 * @property integer $comp_id
 * @property string $exp_date
 * @property string $mfg_date
 * @property integer $hold
 * @property integer $active
 * @property string $rotation_date
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Comp $comp
 * @property \app\models\Prod $prod
 * @property \app\models\User $createdBy
 * @property \app\models\User $updatedBy
 */
class Lot extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_id', 'num', 'comp_id'], 'required'],
            [['prod_id', 'comp_id', 'hold', 'active', 'created_by', 'updated_by'], 'integer'],
            [['exp_date', 'mfg_date', 'rotation_date', 'created_at', 'updated_at'], 'safe'],
            [['num'], 'string', 'max' => 60]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lot';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Prod ID',
            'num' => 'Num',
            'comp_id' => 'Comp ID',
            'exp_date' => 'Exp Date',
            'mfg_date' => 'Mfg Date',
            'hold' => 'Hold',
            'active' => 'Active',
            'rotation_date' => 'Rotation Date',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id'])->inverseOf('lots');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(\app\models\Prod::className(), ['id' => 'prod_id'])->inverseOf('lots');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by'])->inverseOf('lots');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'updated_by'])->inverseOf('lots');
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
