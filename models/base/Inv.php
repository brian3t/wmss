<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "inv".
 *
 * @property integer $id
 * @property integer $prod_variant_id
 * @property integer $uom_id
 * @property integer $qty
 * @property integer $bin_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $note
 * @property integer $noted_by
 *
 * @property \app\models\Bin $bin
 * @property \app\models\ProdVariant $prodVariant
 * @property \app\models\Uom $uom
 */
class Inv extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_variant_id', 'uom_id', 'bin_id'], 'required'],
            [['prod_variant_id', 'uom_id', 'qty', 'bin_id', 'created_by', 'updated_by', 'noted_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_variant_id' => 'Product Variant ID',
            'uom_id' => 'Uom ID',
            'qty' => 'Qty',
            'bin_id' => 'Bin ID',
            'note' => 'Note',
            'noted_by' => 'Noted By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBin()
    {
        return $this->hasOne(\app\models\Bin::className(), ['id' => 'bin_id'])->inverseOf('invs');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdVariant()
    {
        return $this->hasOne(\app\models\ProdVariant::className(), ['id' => 'prod_variant_id'])->inverseOf('invs');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUom()
    {
        return $this->hasOne(\app\models\Uom::className(), ['id' => 'uom_id']);
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
