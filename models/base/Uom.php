<?php

namespace app\models\base;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "uom".
 *
 * @property integer $id
 * @property string $name
 * @property integer $comp_id
 * @property integer $uom_type
 * @property integer $is_base
 * @property string $created_at
 * @property string $updated_at
 * @property string $cnv_to_base
 * @property integer $erp_key
 * @property string $erp_uom
 * @property string $desc
 *
 * @property \app\models\ProdUom[] $prodUoms
 * @property \app\models\Comp $company
 */
class Uom extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'comp_id'], 'required'],
            [['comp_id', 'erp_key'], 'integer'],
            [['uom_type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['cnv_to_base'], 'number'],
            [['name', 'erp_uom', 'desc'], 'string', 'max' => 64],
            [['is_base'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uom';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'UOM',
            'desc' => 'Description',
            'comp_id' => 'Company ID',
            'uom_type' => 'Uom Type',
            'is_base' => 'Is Base',
            'cnv_to_base' => 'Convert To Base',
            'erp_key' => 'Erp Key',
            'erp_uom' => 'Erp Uom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdUoms()
    {
        return $this->hasMany(\app\models\ProdUom::className(), ['uom_id' => 'id'])->inverseOf('uom');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(\app\models\Comp::className(), ['id' => 'comp_id'])->inverseOf('uoms');
    }
}
