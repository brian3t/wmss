<?php

namespace app\models;

use \app\models\base\Inv as BaseInv;
use \soc\yii2helper\models\ModelB3tTrait;

/**
 * This is the model class for table "inv".
 */
class Inv extends BaseInv
{
  use ModelB3tTrait;
  /**
   * @throws \yii\db\Exception
   */
  public function adj($post_payload): bool {
    $old_model = $this->attributes;
    if ($this->loadAll($post_payload) && $this->save()) {
      $inv_jour = new InvJour();
      $inv_jour->inv_old = json_encode($old_model);
      $inv_jour->inv_new = json_encode($this->attributes);
      $inv_jour->note = $post_payload['jour-note'];
      $inv_jour_save_res = $inv_jour->saveAndLogError();
      return $inv_jour_save_res;
    } else {
      return false;
    }
  }
}
