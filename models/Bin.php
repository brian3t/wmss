<?php

namespace app\models;

use \app\models\base\Bin as BaseBin;

/**
 * This is the model class for table "bin".
 * @property int $comp_id [int]
 * @property string $description [varchar(255)]
 * @property bool $active [tinyint(1)]
 * @property bool $excl_fr_avail [tinyint(1)]  Exclude From Available
 * @property bool $excl_fr_fcast [tinyint(1)]  Exclude From Forecast
 * @property bool $allow_neg_inv [tinyint(1)]
 * @property string $seq [mediumint unsigned]
 * @property int $erp_int [int]
 * @property bool $excl_rec [tinyint(1)]  Exclude From Receipt
 * @property bool $excl_ship [tinyint(1)]  Exclude From Ship
 * @property string $erp_loc [varchar(64)]
 * @property string $pallet_spaces [decimal(12,10)]
 * @property int $primary_zone_id [int]
 * @property bool $excl_from_recon [tinyint(1)]  Exclude From Recon
 * @property bool $reverse_recon [tinyint(1)]  Reverse Reconcile From ERP
 * @property int $reverse_recon_rsnkey [int]  Reverse Reconcile ERP Reason
 * @property bool $excl_cont [tinyint(1)]  Exclude Container
 * @property bool $allow_ship_stage [tinyint(1)]  Allow Shipment Staging
 * @property string $perm_move_in [varchar(64)]
 * @property string $perm_move_out [varchar(64)]
 * @property bool $disallow_hold_inv [tinyint(1)]
 * @property string $height_level [decimal(12,10)]
 * @property int $row [smallint]
 * @property int $col [smallint]
 * @property bool $is_item_restricted [tinyint(1)]
 * @property string $loc_positions [decimal(12,10)]
 * @property bool $is_shipping_lane [tinyint(1)]
 * @property bool $is_consolidation_area [tinyint(1)]
 * @property bool $is_pack_area [tinyint(1)]
 * @property bool $auto_unload_cont [tinyint(1)]
 * @property bool $excl_count [tinyint(1)]  Exclude From Count
 * @property bool $require_cont [tinyint(1)]
 * @property bool $is_single_item_only [tinyint(1)]
 * @property bool $is_pick_area [tinyint(1)]
 * @property int $pickgroup_id [int]
 * @property string $length [decimal(12,10)]
 * @property string $width [decimal(12,10)]
 * @property string $height [decimal(12,10)]
 * @property bool $flagged_for_count [tinyint(1)]
 * @property int $pickroute_id [int]
 * @property string $rank [varchar(3)]
 * @property string $max_wt [decimal(12,10)]
 * @property bool $flagged_for_putaway [tinyint(1)]
 * @property bool $is_putaway_hold [tinyint(1)]
 * @property string $putaway_hold_comment [varchar(255)]
 * @property bool $is_pick_hold [tinyint(1)]
 * @property string $pick_hold_comment [varchar(255)]
 * @property string $latitude [decimal(12,10)]
 * @property string $longitude [decimal(12,10)]
 * @property string $last_count_date [datetime]
 * @property string $floor [tinyint unsigned]
 */
class Bin extends BaseBin
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['comp_id', 'name'], 'required'],
            [['comp_id', 'zone_id', 'seq', 'erp_int', 'primary_zone_id', 'reverse_recon_rsnkey', 'row', 'col', 'pickgroup_id', 'pickroute_id', 'created_by', 'updated_by'], 'integer'],
            [['pallet_spaces', 'height_level', 'loc_positions', 'length', 'width', 'height', 'max_wt', 'latitude', 'longitude'], 'number'],
            [['last_count_date', 'created_at', 'updated_at'], 'safe'],
            [['floor'], 'string'],
            [['name', 'erp_loc', 'perm_move_in', 'perm_move_out'], 'string', 'max' => 64],
            [['description', 'putaway_hold_comment', 'pick_hold_comment'], 'string', 'max' => 255],
            [['active', 'excl_fr_avail', 'excl_fr_fcast', 'allow_neg_inv', 'excl_rec', 'excl_ship', 'excl_from_recon', 'reverse_recon', 'excl_cont', 'allow_ship_stage', 'disallow_hold_inv', 'is_item_restricted', 'is_shipping_lane', 'is_consolidation_area', 'is_pack_area', 'auto_unload_cont', 'excl_count', 'require_cont', 'is_single_item_only', 'is_pick_area', 'flagged_for_count', 'flagged_for_putaway', 'is_putaway_hold', 'is_pick_hold'], 'string', 'max' => 1],
            [['rank'], 'string', 'max' => 3]
        ]);
    }

}
