<?php

namespace app\models;

use \app\models\base\Prod as BaseProd;

/**
 * This is the model class for table "prod".
 */
class Prod extends BaseProd
{
//  protected $_rt_softdelete;

  function __construct() {
    /*$this->_rt_softdelete = [
      // multiple row marker column example
//      'deleted_by' => \Yii::$app->user->id,
//      'deleted_at' => date('Y-m-d H:i:s')
    ];*/
    return parent::__construct();
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return parent::rules();
  }

}
