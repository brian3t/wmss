<?php

namespace app\models;

use \app\models\base\Variant as BaseVariant;

/**
 * This is the model class for table "variant".
 */
class Variant extends BaseVariant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['long_desc'], 'string', 'max' => 512]
        ]);
    }
	
}
