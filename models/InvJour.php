<?php

namespace app\models;

use \app\models\base\InvJour as BaseInvJour;
use \soc\yii2helper\models\ModelB3tTrait;

/**
 * This is the model class for table "inv_jour".
 */
class InvJour extends BaseInvJour
{
  use ModelB3tTrait;
}
