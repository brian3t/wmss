<?php

namespace app\models;

use \app\models\base\ProdUom as BaseProdUom;
use \soc\yii2helper\models\ModelB3tTrait;

/**
 * This is the model class for table "inv".
 */
class ProdUomV extends BaseProdUom
{
  use ModelB3tTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'vprod_uom';
  }
}
