<?php

namespace app\models;

use \app\models\base\Pickdet as BasePickdet;

/**
 * This is the model class for table "pickdet".
 */
class Pickdet extends BasePickdet
{
  public function beforeValidate() {
    return parent::beforeValidate();
  }

  public function afterSave($insert, $changedAttributes) {
    parent::afterSave($insert, $changedAttributes);
  }

}
