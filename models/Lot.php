<?php

namespace app\models;

use \app\models\base\Lot as BaseLot;

/**
 * This is the model class for table "lot".
 */
class Lot extends BaseLot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['prod_id', 'num', 'comp_id'], 'required'],
            [['prod_id', 'comp_id', 'hold', 'active', 'created_by', 'updated_by'], 'integer'],
            [['exp_date', 'mfg_date', 'rotation_date', 'created_at', 'updated_at'], 'safe'],
            [['num'], 'string', 'max' => 60]
        ]);
    }
	
}
