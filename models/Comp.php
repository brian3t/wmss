<?php

namespace app\models;

use \app\models\base\Comp as BaseComp;

/**
 * This is the model class for table "comp".
 */
class Comp extends BaseComp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['updated_at'], 'safe'],
            [['updated_by'], 'integer'],
            [['name'], 'string', 'max' => 12],
            [['descr'], 'string', 'max' => 128]
        ]);
    }

}
