<?php

namespace app\models;

use \app\models\base\Uom as BaseUom;

/**
 * This is the model class for table "uom".
 */
class Uom extends BaseUom
{
    /**
     * @inheritdoc
     */
    public function rules() {
      return parent::rules();
    }
}
