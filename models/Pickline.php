<?php

namespace app\models;

use \app\models\base\Pickline as BasePickline;
use soc\yii2helper\models\ModelB3tTrait;

/**
 * This is the model class for table "pickline".
 */
class Pickline extends BasePickline
{
  use ModelB3tTrait;
  public function beforeValidate()
  {
    return parent::beforeValidate();
  }

  public function beforeSave($insert)
  {
    if (empty($this->description)) {
      if (empty($this->soline_id)) $this->description = 'Waiting for SO Line to get linked';
      else $this->description = "Pick " . $this->soline_id;
    }
    if (empty($this->orig_qty_to_pick)) $this->orig_qty_to_pick = $this->qty_to_pick;
    if (empty($this->decimals)) $this->decimals = 4;
    return parent::beforeSave($insert);
  }

  public function afterSave($insert, $changedAttributes)
  {
    if (empty($this->description)) {
      $soline = $this->soline;
      $this->description = $soline->so0->name . ' ' . $soline->lineid . ' ' . $this->prodVar->name;
    }
    if (empty($this->qty_to_pick)) {
      $v_soline_pickline = $this->getVSolinePickline();
      if (!($v_soline_pickline->exists())) {
        parent::afterSave($insert, $changedAttributes);
        return;
      }
      $v_soline_pickline = $v_soline_pickline->asArray()->one();
      $remain_to_pick_stock = number_format($v_soline_pickline['qty_ord_stock']) - number_format($v_soline_pickline['sum_qty_picked_stock']);
      if ($remain_to_pick_stock < 0) $remain_to_pick_stock = 0; //future this must be prevented during validation
      $this->qty_to_pick = $remain_to_pick_stock / ($this->prodUom?->uom?->cnv_to_base ?? 1);//pick remaining
      $save_res = $this->save();
      $err = $this->errors;
      $a = 1;
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function getDesc(){
    $soline = $this->soline;
    return "PL for SO " . $soline->so . " line " . $soline->lineid;
  }
}
