<?php

namespace app\models;

use \app\models\base\ProdDetail as BaseProdDetail;

/**
 * This is the model class for table "prod_detail".
 */
class ProdDetail extends BaseProdDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['prod_variant_id', 'variant_val_id'], 'required'],
            [['prod_variant_id', 'variant_val_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note'], 'string', 'max' => 255]
        ]);
    }
	
}
