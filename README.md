### Todo:  
install cebe/markdown-latex, 2amigos/yii2-usuario
Then re-configure mail 


INSTALL
-------------
Config files: `config/db.php` `config/params.php` `api/params.php` `models/constant.php`

`chmod -R 777 web/assets`  
`chmod -R 777 runtime`  
`cp -aR vendor/bower-asset ./vendor/bower`  
`cp -aR vendor/npm-asset ./vendor/npm`  


Install SOCAL modules:  

[//]: # (`git submodule add https://github.com/brian3t/yii2helper.git`)
git submodule add git@github.com:brian3t/yii2helper.git

```
mkdir ./soc
git submodule add git@gitlab.com:brian3t/yii-user.git soc/yiiuser
```

Copy folder `override` to root dir.
Copy folder ./web/js, which contains highcharts, bootstrap-notify.

#### Code overrides
+ 4/21/23: mootensai/RelationTrait:302:
                            
        if (!$isNewRecord && $relData[$relName]['ismultiple']) {//b3t when creating new record; do NOT do this. Because afterSave might have added child records

+ 4/22/23 dev only: vendor/phpspec/php-diff/lib/Diff.php :

    	private $defaultOptions = array(
		'ignoreWhitespace' => true,

So that yii-gii ignore whitespaces

#### API
###### API rules:
For rest API endpoints; index grabs data from view. 
For example: v1/inv/ calls modules/v1/controllers/InvController.php
Then inside InvController.php; it overrides indexAction to use class app\api\modules\v1\controllers\InvvIndexAction.
That class grabs data from vinv instead of table.
