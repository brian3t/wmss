<?php

namespace app\api\modules\v1\controllers;

use app\models\ProdUomV;
use yii\data\ActiveDataProvider;
use yii\rest\IndexAction;

class ProdUomvIndexAction extends IndexAction{
  public function prepareDataProvider()
  {
    $params = \Yii::$app->getRequest()->getQueryParams();
    unset($params['page']);
    $page_size = $params['page_size'] ?? false;
    unset($params['page_size']);
    $sql = '
        select *
FROM
  vprod_uom
  ';

    $dp = new ActiveDataProvider(
      [
        'query' => ProdUomV::findBySql($sql),
//                'query' => \app\models\Event::find()->where(['>=', 'date_utc',$event_date_start])
//                    ->andWhere(['<=', 'date_utc', $event_date_end])->joinWith('bands'),
        'pagination' => [
          'pageSize' => $page_size ?? 20,
        ],
      ]
    );
    if (YII_DEBUG) {
      $dp->pagination = false;
    }
    return $dp;

  }
}
