<?php

namespace app\api\modules\v1\controllers;

use app\models\InvV;
use yii\data\ActiveDataProvider;
use yii\rest\IndexAction;

class InvvIndexAction extends IndexAction{
  public function prepareDataProvider()
  {
    $params = \Yii::$app->getRequest()->getQueryParams();
    unset($params['page']);
    $page_size = $params['page_size'] ?? false;
    unset($params['page_size']);
    $sql = '
        select *
FROM
  vinv
  ';

    $dp = new ActiveDataProvider(
      [
        'query' => InvV::findBySql($sql),
//                'query' => \app\models\Event::find()->where(['>=', 'date_utc',$event_date_start])
//                    ->andWhere(['<=', 'date_utc', $event_date_end])->joinWith('bands'),
        'pagination' => [
          'pageSize' => $page_size ?? 20,
        ],
      ]
    );
    if (YII_DEBUG) {
      $dp->pagination = false;
    }
    return $dp;

  }
}
