<?php

namespace app\api\modules\v1\controllers;

use app\api\base\controllers\BaseActiveController;
use app\models\So;
use yii\data\ActiveDataProvider;
use yii\rest\IndexAction;


require_once realpath(dirname(__DIR__, 4)). "/models/constants.php";
class SoController extends BaseActiveController
{
    // We are using the regular web app modules:
    public $modelClass = 'app\models\So';

    public function actions(): array {
        $actions = parent::actions();

        // disable the default REST actions
//        unset($actions['index']);

        return $actions;
    }

}
