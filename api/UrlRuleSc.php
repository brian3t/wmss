<?php

namespace app\api;

use Yii;
use yii\base\InvalidConfigException;
use yii\rest\UrlRule;
use yii\web\UrlRuleInterface;

class UrlRuleSc extends UrlRule implements UrlRuleInterface
{
  protected string $path = '@app/controllers';

  /**
   * @throws InvalidConfigException
   */
  public function init()
  {
    $d = dir(Yii::getAlias($this->path));
    $arr = [];
    while (false !== ($entry = $d->read())) {
      if (str_contains($entry, 'Controller.php')) {
        $arr[] = lcfirst(str_replace(['Controller.php'], '', $entry));
      }
    }
    $this->controller = $arr;
    parent::init();
  }

  /**
   * @throws InvalidConfigException
   */
  public function parseRequest($manager, $request)
  {
    $pathInfo = $request->getPathInfo();
    if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
      // check $matches[1] and $matches[3] to see
      // if they match a manufacturer and a model in the database.
      // If so, set $params['manufacturer'] and/or $params['model']
      // and return ['car/index', $params]
    }
    return false; // this rule does not apply
  }

}
