<?php

use yii\db\Migration;
use soc\yiiuser\User\Model\User;

/**
 * Class m221205_011725_yiiuser_11_add_admin
 */
class m221205_011725_yiiuser_11_add_admin extends Migration
{
  /**
   * @throws \yii\base\Exception
   * @throws Exception
   */
  public function safeUp()
    {
        /* 8/11/23 b3t commented out b/c Exception: SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'user_id' cannot be null
The SQL being executed was: INSERT INTO `auth_assignment` (`user_id`, `item_name`, `created_at`) VALUES (NULL, 'admin', 1691759943) (/var/www/wmss/vendor/yiisoft/yii2/db/Schema.php:676)

        $auth = Yii::$app->authManager;

        // create a role named "administrator"
        $administratorRole = $auth->createRole('admin');
        $administratorRole->description = 'Administrator';
        $auth->add($administratorRole);

        // create permission for certain tasks
        $permission = $auth->createPermission('user-management');
        $permission->description = 'User Management';
        $auth->add($permission);

        // let administrators do user management
        $auth->addChild($administratorRole, $auth->getPermission('user-management'));

        // create user "admin" with password "verysecret"
        $user = new User([
            'scenario' => 'create',
            'email' => "ngxtri@gmail.com",
            'username' => "admin",
            'password' => "Trapok)1"  // >6 characters!
        ]);
        $user->confirmed_at = time();
        $user->save();

        // assign role to our admin-user
        $auth->assign($administratorRole, $user->id);*/
    }

  /**
   * @throws Throwable
   * @throws \yii\db\StaleObjectException
   */
  public function safeDown()
    {
        /*$auth = Yii::$app->authManager;

        // delete permission
        $auth->remove($auth->getPermission('user-management'));

        // delete admin-user and administrator role
        $administratorRole = $auth->getRole("administrator");
        $user = User::findOne(['username'=>"admin"]);
        $auth->revoke($administratorRole, $user->id);
        $user->delete();
        $auth->remove($administratorRole);*/
    }
}
